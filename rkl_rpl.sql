/*
Navicat MySQL Data Transfer

Source Server         : root@127.0.0.1 ( Localhost )
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : rkl_rpl

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2016-01-01 07:25:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `hasil_uji`
-- ----------------------------
DROP TABLE IF EXISTS `hasil_uji`;
CREATE TABLE `hasil_uji` (
  `hasil_uji_id` varchar(100) NOT NULL,
  `tgl_uji` date NOT NULL,
  `m_company_id` varchar(100) DEFAULT NULL,
  `m_aturan_id` int(11) NOT NULL,
  `jenis_kegiatan` int(11) NOT NULL COMMENT 'fk dari kategori dengan tipe kategori = 2',
  `jenis_sample` int(11) NOT NULL COMMENT 'fk dari kategori dengan tipe kategori = 1',
  `tipe_lokasi` int(11) NOT NULL COMMENT 'fk dari kategori dengan tipe kategori = 3',
  PRIMARY KEY (`hasil_uji_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hasil_uji
-- ----------------------------

-- ----------------------------
-- Table structure for `hasil_uji_detil`
-- ----------------------------
DROP TABLE IF EXISTS `hasil_uji_detil`;
CREATE TABLE `hasil_uji_detil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hasil_uji_id` varchar(100) NOT NULL,
  `m_parameter_id` date NOT NULL,
  `hasil_uji` varchar(100) DEFAULT NULL,
  `nilai_ref1` int(11) NOT NULL,
  `nilai_ref2` int(11) NOT NULL COMMENT 'fk dari kategori dengan tipe kategori = 2',
  `desc` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hasil_uji_detil
-- ----------------------------

-- ----------------------------
-- Table structure for `huji_dummy`
-- ----------------------------
DROP TABLE IF EXISTS `huji_dummy`;
CREATE TABLE `huji_dummy` (
  `huji_dummy_id` int(11) NOT NULL AUTO_INCREMENT,
  `param_dummy_id` varchar(50) DEFAULT NULL,
  `huji_dummy_val` int(11) DEFAULT NULL,
  PRIMARY KEY (`huji_dummy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of huji_dummy
-- ----------------------------
INSERT INTO `huji_dummy` VALUES ('7', '1', '70');
INSERT INTO `huji_dummy` VALUES ('8', '2', '40');
INSERT INTO `huji_dummy` VALUES ('9', '3', '40');

-- ----------------------------
-- Table structure for `m_aturan`
-- ----------------------------
DROP TABLE IF EXISTS `m_aturan`;
CREATE TABLE `m_aturan` (
  `m_aturan_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_aturan_name` varchar(100) NOT NULL,
  `m_aturan_desc` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(50) NOT NULL,
  PRIMARY KEY (`m_aturan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_aturan
-- ----------------------------

-- ----------------------------
-- Table structure for `m_baku_mutu`
-- ----------------------------
DROP TABLE IF EXISTS `m_baku_mutu`;
CREATE TABLE `m_baku_mutu` (
  `m_baku_mutu_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_baku_mutu_ref1` double NOT NULL,
  `m_baku_mutu_ref2` double NOT NULL,
  `m_parameter_id` int(11) NOT NULL,
  `m_kategori_id` int(11) NOT NULL COMMENT 'diambil dari m_kategori dengan tipe kategori = 1',
  `m_aturan_id` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`m_baku_mutu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_baku_mutu
-- ----------------------------

-- ----------------------------
-- Table structure for `m_company`
-- ----------------------------
DROP TABLE IF EXISTS `m_company`;
CREATE TABLE `m_company` (
  `m_company_id` varchar(100) NOT NULL,
  `m_company_name` varchar(100) NOT NULL,
  `m_company_address` text,
  `m_kategori` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(50) NOT NULL,
  PRIMARY KEY (`m_company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_company
-- ----------------------------

-- ----------------------------
-- Table structure for `m_kategori`
-- ----------------------------
DROP TABLE IF EXISTS `m_kategori`;
CREATE TABLE `m_kategori` (
  `m_kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_kategori_name` varchar(100) NOT NULL,
  `m_tipe_kategori_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(50) NOT NULL,
  PRIMARY KEY (`m_kategori_id`),
  KEY `m_tipe_kategori_id` (`m_tipe_kategori_id`),
  CONSTRAINT `m_kategori_ibfk_1` FOREIGN KEY (`m_tipe_kategori_id`) REFERENCES `m_kategori_tipe` (`m_kategori_tipe_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_kategori
-- ----------------------------
INSERT INTO `m_kategori` VALUES ('1', 'rumah sakit', '2', '2015-11-10 17:08:51', '0000-00-00 00:00:00', '');
INSERT INTO `m_kategori` VALUES ('2', 'outlet', '3', '2015-11-10 17:09:05', '0000-00-00 00:00:00', '');
INSERT INTO `m_kategori` VALUES ('3', 'air limbah', '1', '2015-11-10 17:09:19', '0000-00-00 00:00:00', '');

-- ----------------------------
-- Table structure for `m_kategori_tipe`
-- ----------------------------
DROP TABLE IF EXISTS `m_kategori_tipe`;
CREATE TABLE `m_kategori_tipe` (
  `m_kategori_tipe_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_kategori_tipe_name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(50) NOT NULL,
  PRIMARY KEY (`m_kategori_tipe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_kategori_tipe
-- ----------------------------
INSERT INTO `m_kategori_tipe` VALUES ('1', 'sample', '2015-11-10 17:03:54', '0000-00-00 00:00:00', '');
INSERT INTO `m_kategori_tipe` VALUES ('2', 'industri / kegiatan', '2015-11-10 17:07:00', '2015-11-10 17:07:00', '');
INSERT INTO `m_kategori_tipe` VALUES ('3', 'lokasi', '2015-11-10 17:07:08', '0000-00-00 00:00:00', '');

-- ----------------------------
-- Table structure for `m_menu`
-- ----------------------------
DROP TABLE IF EXISTS `m_menu`;
CREATE TABLE `m_menu` (
  `m_menu_id` char(3) NOT NULL,
  `m_menu_nama` varchar(255) DEFAULT NULL,
  `m_menu_url` text,
  `m_menu_keterangan` text,
  `m_menu_parent` char(3) DEFAULT NULL,
  `m_menu_level` int(11) DEFAULT NULL,
  `m_menu_icon` varchar(50) DEFAULT NULL,
  `m_menu_status` enum('0','1') DEFAULT NULL,
  `m_menu_position` int(25) DEFAULT NULL,
  `m_menu_created_date` datetime DEFAULT NULL,
  `m_menu_modified_date` datetime DEFAULT NULL,
  `m_menu_created_by` varchar(50) DEFAULT NULL,
  `m_menu_modified_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`m_menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_menu
-- ----------------------------
INSERT INTO `m_menu` VALUES ('004', 'Setting Aplikasi', '#', 'Setting Aplikasi', '0', null, 'fa fa-wrench', null, '1', null, null, null, null);
INSERT INTO `m_menu` VALUES ('005', 'Manajemen Menu', 'pengaturan.menu', 'Manajemen Menu', '004', '2', '', '1', '1', null, '2015-09-22 00:00:00', null, null);
INSERT INTO `m_menu` VALUES ('006', 'Manajemen Akses', 'pengaturan.aturan', 'Manajemen Hak Akses', '004', '2', '', '1', '2', null, '2015-09-22 00:00:00', null, null);
INSERT INTO `m_menu` VALUES ('007', 'Manajemen Pengguna', 'pengaturan.pengguna', 'Manajemen Pengguna', '004', '2', '', '1', '3', null, '2015-09-22 00:00:00', null, null);
INSERT INTO `m_menu` VALUES ('008', 'Master', '#', 'Master Data', '0', null, 'fa fa-book', null, '2', null, null, null, null);
INSERT INTO `m_menu` VALUES ('010', 'Kategori Tipe', 'master_data.kategori_tipe', 'kategori_tipe', '008', null, '', null, '1', null, null, null, null);
INSERT INTO `m_menu` VALUES ('011', 'Kategori', 'master_data.kategori', 'kategori', '008', null, '', null, '2', null, null, null, null);
INSERT INTO `m_menu` VALUES ('012', 'Company', 'master_data.company', 'Company', '008', null, '', null, '3', null, null, null, null);
INSERT INTO `m_menu` VALUES ('013', 'Baku Mutu', 'master_data.baku_mutu', 'Master Baku Mutu', '008', null, '', null, '7', null, null, null, null);
INSERT INTO `m_menu` VALUES ('014', 'Satuan', 'master_data.satuan', 'Master Satuan', '008', null, '', null, '5', null, null, null, null);
INSERT INTO `m_menu` VALUES ('015', 'Aturan', 'master_data.aturan', 'Master Aturan', '008', null, '', null, '4', null, null, null, null);
INSERT INTO `m_menu` VALUES ('016', 'Parameter', 'master_data.parameter', 'Master Parameter', '008', null, '', null, '6', null, null, null, null);
INSERT INTO `m_menu` VALUES ('017', 'Pelaporan', 'pelaporan/pelaporan', 'Pelaporan', '0', null, 'fa fa-book', null, '3', null, null, null, null);
INSERT INTO `m_menu` VALUES ('018', 'Home', 'dashboard/dashboard', 'Home', '0', null, 'fa fa-home', null, '0', null, null, null, null);

-- ----------------------------
-- Table structure for `m_otoritas_menu`
-- ----------------------------
DROP TABLE IF EXISTS `m_otoritas_menu`;
CREATE TABLE `m_otoritas_menu` (
  `m_role_id` int(11) DEFAULT NULL,
  `m_menu_id` char(3) DEFAULT NULL,
  `m_role_view` enum('0','1') DEFAULT '0',
  `m_role_add` enum('0','1') DEFAULT '0',
  `m_role_edit` enum('0','1') DEFAULT '0',
  `m_role_delete` enum('0','1') DEFAULT '0',
  `m_role_export` enum('0','1') DEFAULT '0',
  `m_role_import` enum('0','1') DEFAULT '0',
  KEY `m_role_id` (`m_role_id`),
  KEY `m_menu_id` (`m_menu_id`),
  CONSTRAINT `m_otoritas_menu_ibfk_1` FOREIGN KEY (`m_role_id`) REFERENCES `m_role` (`m_role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_otoritas_menu
-- ----------------------------
INSERT INTO `m_otoritas_menu` VALUES ('17', '004', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '005', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '006', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '007', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '008', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '010', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '011', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '012', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '015', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '014', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '016', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '013', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('18', '018', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('18', '017', '1', '1', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for `m_parameter`
-- ----------------------------
DROP TABLE IF EXISTS `m_parameter`;
CREATE TABLE `m_parameter` (
  `m_parameter_id` int(11) NOT NULL,
  `m_parameter_name` varchar(100) NOT NULL,
  `m_parameter_desc` text,
  `m_satuan_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(50) NOT NULL,
  PRIMARY KEY (`m_parameter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_parameter
-- ----------------------------

-- ----------------------------
-- Table structure for `m_provinsi`
-- ----------------------------
DROP TABLE IF EXISTS `m_provinsi`;
CREATE TABLE `m_provinsi` (
  `m_provinsi_id` varchar(25) NOT NULL,
  `m_provinsi_nama` varchar(35) NOT NULL,
  `m_provinsi_created_date` date DEFAULT NULL,
  `m_provinsi_created_by` varchar(15) DEFAULT NULL,
  `m_provinsi_modified_date` date DEFAULT NULL,
  `m_provinsi_modified_by` varchar(15) DEFAULT NULL,
  `m_provinsi_status` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`m_provinsi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of m_provinsi
-- ----------------------------
INSERT INTO `m_provinsi` VALUES ('1', 'ACEH', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('12920', 'SUMATERA BARAT', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('14086', 'RIAU', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('15885', 'JAMBI', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('17404', 'SUMATERA SELATAN', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('20802', 'BENGKULU', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('22328', 'LAMPUNG', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('24993', 'KEPULAUAN BANGKA BELITUNG', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('25405', 'KEPULAUAN RIAU', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('25823', 'DKI JAKARTA', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('26141', 'JAWA BARAT', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('32676', 'JAWA TENGAH', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('41863', 'DAERAH ISTIMEWA YOGYAKARTA', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('42385', 'JAWA TIMUR', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('51578', 'BANTEN', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('53241', 'BALI', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('54020', 'NUSA TENGGARA BARAT', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('55065', 'NUSA TENGGARA TIMUR', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('58285', 'KALIMANTAN BARAT', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('60371', 'KALIMANTAN TENGAH', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('61965', 'KALIMANTAN SELATAN', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('64111', 'KALIMANTAN TIMUR', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('65702', 'SULAWESI UTARA', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('6728', 'SUMATERA UTARA', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('67393', 'SULAWESI TENGAH', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('69268', 'SULAWESI SELATAN', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('72551', 'SULAWESI TENGGARA', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('74716', 'GORONTALO', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('75425', 'SULAWESI BARAT', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('76096', 'MALUKU', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('77085', 'MALUKU UTARA', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('78203', 'PAPUA', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('81877', 'PAPUA BARAT', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');

-- ----------------------------
-- Table structure for `m_role`
-- ----------------------------
DROP TABLE IF EXISTS `m_role`;
CREATE TABLE `m_role` (
  `m_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_role_nama` varchar(50) DEFAULT NULL,
  `m_role_ket` text,
  `m_role_created_date` datetime DEFAULT NULL,
  `m_role_modified_date` datetime DEFAULT NULL,
  `m_role_created_by` varchar(50) DEFAULT NULL,
  `m_role_modified_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`m_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_role
-- ----------------------------
INSERT INTO `m_role` VALUES ('17', 'Admin', 'Super Admin', '2015-02-05 09:51:21', '2015-02-05 09:51:25', '', '');
INSERT INTO `m_role` VALUES ('18', 'User', 'User', null, null, null, null);

-- ----------------------------
-- Table structure for `m_satuan`
-- ----------------------------
DROP TABLE IF EXISTS `m_satuan`;
CREATE TABLE `m_satuan` (
  `m_satuan_id` int(11) NOT NULL,
  `m_satuan_name` varchar(100) NOT NULL,
  `m_satuan_printout` varchar(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(50) NOT NULL,
  PRIMARY KEY (`m_satuan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_satuan
-- ----------------------------

-- ----------------------------
-- Table structure for `m_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `m_sessions`;
CREATE TABLE `m_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_sessions
-- ----------------------------

-- ----------------------------
-- Table structure for `m_user`
-- ----------------------------
DROP TABLE IF EXISTS `m_user`;
CREATE TABLE `m_user` (
  `m_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_user_nama` varchar(50) DEFAULT NULL,
  `m_user_username` varchar(50) DEFAULT NULL,
  `m_user_password` varchar(32) DEFAULT NULL,
  `m_user_jk` enum('P','L') DEFAULT NULL,
  `m_user_tgl_lhr` date DEFAULT NULL,
  `m_user_alamat` text,
  `m_user_email` varchar(255) DEFAULT NULL,
  `m_user_tlp` varchar(15) DEFAULT NULL,
  `m_user_wrong_pass` int(11) DEFAULT '0',
  `m_user_ph` varchar(255) NOT NULL,
  `m_user_status` enum('1','0') DEFAULT NULL,
  `m_user_lastlogin` date DEFAULT NULL,
  `m_user_last_password_update` date DEFAULT NULL,
  `m_user_created_date` datetime DEFAULT NULL,
  `m_user_modified_date` datetime DEFAULT NULL,
  `m_user_created_by` varchar(50) DEFAULT NULL,
  `m_user_modified_by` varchar(50) DEFAULT NULL,
  `m_toko_id` int(11) DEFAULT NULL,
  `m_role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`m_user_id`),
  KEY `m_role_id` (`m_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=151207002 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_user
-- ----------------------------
INSERT INTO `m_user` VALUES ('21', 'Luthfi Aziz Nugraha', 'admin', '21232f297a57a5a743894a0e4a801fc3', null, null, null, 'geje@gmail.com', '089928111232', '0', '51599948d10Z2VqZQ==65d2ea03425', '1', null, null, '2015-10-10 00:00:00', null, 'Administrator', null, null, '17');
INSERT INTO `m_user` VALUES ('151207001', 'Ruli Achmad Sobari', 'ruliachmad', 'a6173c3aced9c9dfeee358b97eb6381e', null, null, null, null, null, '0', '', null, null, null, null, null, null, null, null, '18');

-- ----------------------------
-- Table structure for `param_dummy`
-- ----------------------------
DROP TABLE IF EXISTS `param_dummy`;
CREATE TABLE `param_dummy` (
  `param_dummy_id` int(11) DEFAULT NULL,
  `param_dummy_name` varchar(50) DEFAULT NULL,
  `param_dummy_val` int(11) DEFAULT NULL,
  `m_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of param_dummy
-- ----------------------------
INSERT INTO `param_dummy` VALUES ('1', 'NO', '60', '151207001');
INSERT INTO `param_dummy` VALUES ('2', 'SO', '50', '151207001');
INSERT INTO `param_dummy` VALUES ('3', 'Dll', '70', '151207001');
