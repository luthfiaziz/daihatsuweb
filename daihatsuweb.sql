/*
Navicat MySQL Data Transfer

Source Server         : root@localhost
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : daihatsuweb

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2016-09-25 17:20:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dclient
-- ----------------------------
DROP TABLE IF EXISTS `dclient`;
CREATE TABLE `dclient` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `picture` text,
  `desc` text,
  `status` enum('on','off') DEFAULT 'off',
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dclient
-- ----------------------------

-- ----------------------------
-- Table structure for dcontactus
-- ----------------------------
DROP TABLE IF EXISTS `dcontactus`;
CREATE TABLE `dcontactus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `jk` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `notelp` int(11) DEFAULT NULL,
  `pesan` text,
  `crtd_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dcontactus
-- ----------------------------
INSERT INTO `dcontactus` VALUES ('1', 'Tes', 'l', 'tes@gmail.com', 'tes', '123456', 'tes', null);
INSERT INTO `dcontactus` VALUES ('2', 'tes', 'l', 'tes@gmail.com', 'tes', '123', 'tes', '2016-09-25');
INSERT INTO `dcontactus` VALUES ('3', 'tes', 'l', 'tes@gmail.com', 'tes', '123', 'tes', '2016-09-25');

-- ----------------------------
-- Table structure for devents
-- ----------------------------
DROP TABLE IF EXISTS `devents`;
CREATE TABLE `devents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `desc` text,
  `vdesc` text,
  `picture` text,
  `status` enum('on','off') DEFAULT 'on',
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of devents
-- ----------------------------
INSERT INTO `devents` VALUES ('1', 'Ayo cari mimin di #DaihatsuGIIAS2016', '<p>Ayo cari mimin di <em><strong>#DaihatsuGIIAS2016</strong></em>, kamu bakal dapatin voucher belanja setiap hari dari <strong>11-21&nbsp; Agustus 2016</strong> untuk 3 Sahabat tercepat<br />\r\n<br />\r\nSyarat dan Ketentuan<br />\r\n<br />\r\n- Follow semua akun Daihatsu Indonesia</p>\r\n\r\n<p>Facebook: Daihatsu Indonesia<br />\r\nTwitter &amp; Instagram: @DaihatsuInd</p>\r\n\r\n<p><br />\r\n- Cari admin Daihatsu di area <strong><em>#DaihatsuGIIAS2016</em></strong> pada <strong>11-21 Agustus 2016</strong> selama acara berlangsung<br />\r\n- 3 Sahabat yang tercepat menemukan Admin Daihatsu harus menepuk pundak Admin dengan kata kunci &quot;Kamu mimin<br />\r\n<br />\r\nmedia sosialnya Daihatsu Indonesia?&quot; Jika Admin menjawab &quot;Iya saya miminnya media sosial Daihatsu&quot;.</p>\r\n\r\n<p><br />\r\n- Tunjukan akun Facebook, Twitter &amp; Instagram Daihatsu yang sudah di follow untuk mendapatkan voucher</p>\r\n\r\n<p><br />\r\nbelanja senilai 100K.<br />\r\n- Kamu yang menemukan admin DILARANG mengambil foto dan unggah di media sosial.</p>', 'Ayo cari mimin di #DaihatsuGIIAS2016, kamu bakal dapatin voucher belanja setiap hari dari 11-21  Agustus 2016 untuk 3 Sahabat tercepat. Syarat dan Ketentuan; Follow semua akun Daihatsu Indonesia; Facebook: Daihatsu Indonesia; Twitter & Instagram: @DaihatsuInd; Cari admin Daihatsu di area #DaihatsuGIIAS2016 pada 11-21 Agustus 2016 selama acara berlangsung 3 Sahabat yang tercepat menemukan Admin Daihatsu harus menepuk pundak Admin dengan kata kunci \"Kamu mimin media sosialnya Daihatsu Indonesia?\" Jika Admin menjawab \"Iya saya miminnya media sosial Daihatsu\". Tunjukan akun Facebook, Twitter & Instagram Daihatsu yang sudah di follow untuk mendapatkan voucher belanja senilai 100K. Kamu yang menemukan admin DILARANG mengambil foto dan unggah di media sosial.', 'image_20160921072340.png', 'on', '2016-09-21', '2016-09-22', 'Luthfi Aziz Nugraha');
INSERT INTO `devents` VALUES ('2', 'HARI BERSAMA SHEILA ON 7', '<p>Ikuti kompetisi Foto atau Video yang nunjukin kecintaanmu pada Sheila On 7. Segera Upload di Instagram-mu dan raih kesempatan untuk meet &amp; greet bersama personil Sheila On 7.<br />\r\n<br />\r\nSyarat dan Ketentuan<br />\r\n<br />\r\n1. Peserta wajib untuk follow Instagram @DaihatsuInd.<br />\r\n2. Peserta wajib upload foto &ldquo;Kecintaannya pada SO7&rdquo; di Instagram dengan hashtag #HariBersamaSO7 dan tag ke @DaihatsuInd.<br />\r\n3. Peserta wajib menyertakan caption menarik untuk bertemu Sheila On 7 di GIIAS 2016.<br />\r\n4. Peserta dapat mengupload maksimal 5 foto kecintaannya pada SO7.<br />\r\n5. Periode kuis 26 Juli &ndash; 4 Agustus 2016.<br />\r\n6. Panitia akan mendiskualifikasi peserta, jika ditemukan kecurangan.<br />\r\n7. Panitia akan menghapus segala materi yang mengandung unsur pornografi, SARA dan tidak sesuai dengan ketentuan kuis.<br />\r\n8. Keputusan juri tidak dapat diganggu gugat.<br />\r\n9. Hati-hati penipuan! Daihatsu dan agensi penyelenggara tidak memungut biaya apapun untuk promosi ini.<br />\r\n10. Info lebih lanjut, hubungi melalui facebook <a href=\"http://facebook.com/daihatsuindonesia\" target=\"_blank\">Daihatsu Indonesia</a><br />\r\n11. 10 peserta dengan foto dan video terbaik pilihan juri, akan mendapatkan tiket GIIAS 2016 dan berkesempatan untuk meet &amp; greet bersama personil Sheila On 7 di GIIAS Daihatsu 2016.<br />\r\n12. Untuk pemenang, Daihatsu tidak menanggung biaya transportasi menuju GIIAS 2016<br />\r\n13. Syarat dan ketentuan di atas dapat berubah sewaktu-waktu tanpa pemberitahuan terlebih dahulu.</p>', 'Ikuti kompetisi Foto atau Video yang nunjukin kecintaanmu pada Sheila On 7. Segera Upload di Instagram-mu dan raih kesempatan untuk meet & greet bersama personil Sheila On 7. Syarat dan Ketentuan 1. Peserta wajib untuk follow Instagram @DaihatsuInd. 2. Peserta wajib upload foto “Kecintaannya pada SO7” di Instagram dengan hashtag #HariBersamaSO7 dan tag ke @DaihatsuInd. 3. Peserta wajib menyertakan caption menarik untuk bertemu Sheila On 7 di GIIAS 2016. 4. Peserta dapat mengupload maksimal 5 foto kecintaannya pada SO7. 5. Periode kuis 26 Juli – 4 Agustus 2016. 6. Panitia akan mendiskualifikasi peserta, jika ditemukan kecurangan. 7. Panitia akan menghapus segala materi yang mengandung unsur pornografi, SARA dan tidak sesuai dengan ketentuan kuis. 8. Keputusan juri tidak dapat diganggu gugat.', 'image_20160921072628.png', 'on', '2016-09-21', '2016-09-22', 'Luthfi Aziz Nugraha');
INSERT INTO `devents` VALUES ('3', 'Daihatsu Show Choir Competition', '<p>Daihatsu Show Choir Competition 2016 merupakan kompetisi paduan suara yang diperuntukkan bagi siswa/i tingkat Sekolah Dasar (SD) dengan total hadiah Rp 30juta. Lagu wajib yang akan dibawakan oleh peserta ditentukan oleh pihak Daihatsu. Lagu yang dibawakan harus merupakan karya asli untuk paduan suara dan bukan reduksi dari orchestra serta harus dibawakan sesuai dengan partitur.</p>\r\n\r\n<p>Setiap sekolah masing-masing diwakilkan oleh maksimal 1 kelompok paduan suara dengan anggota dari tiap kelompok peserta lomba merupakan siswa/i tingkat SD atau sederajat yang masih resmi dan terbukti menjadi siswa di sekolah yang bersangkutan. Pendaftaran peserta dimulai dari tanggal 12 Mei - 15 Juli 2016. Untuk pendaftaran, silahkan download dan isi <a href=\"http://daihatsu.co.id/formulir/kids-choir-2016.docx\">formulir</a> kemudian email ke daihatsushowchoir2016@gmail.com</p>', null, 'image_20160921072722.jpg', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');
INSERT INTO `devents` VALUES ('4', 'Berkah Ramadan Daihatsu', '<p>Berikan Keceriaan Sahabat Ramadan Daihatsu!! Follow twitter <a href=\"http://twitter.com/daihatsuind\" target=\"_blank\">@daihatsuind</a>, fanpage Daihatsu Indonesia dan instagram <a href=\"http://instagram.com/daihatsuind\" target=\"_blank\">@daihatsuind</a> untuk mengikuti rangkaian kegiatan Ramadan Daihatsu</p>\r\n\r\n<p>#KurmaDaihatsu<br />\r\nIkuti Kuis Ramadan Daihatsu setiap pukul 03.00 - 05.00 WIB di twitter <a href=\"http://twitter.com/daihatsuind\" target=\"_blank\">@DaihatsuInd</a> dan ikuti semua tantangannya selama Ramadan. Akan ada pulsa senilai 25K masing-masing untuk 5 pemenang.</p>\r\n\r\n<p>#NgabuburitDaihatsu<br />\r\nMenangkan voucher belanja senilai 200K masing-masing untuk 10 pemenang setiap minggunya di <a href=\"http://facebook.com/DaihatsuIndonesia\" target=\"_blank\">facebook.com/DaihatsuIndonesia</a> selama Ramadan.</p>\r\n\r\n<p>#SahabatBerbagi<br />\r\nIkutan berdonasi bersama Daihatsu di instagram <a href=\"http://instagram.com/daihatsuind\" target=\"_blank\">@daihatsuind</a> dengan mengirimkan foto kegiatan positifmu selama Ramadan. 1 foto yang dikirimkan sama dengan 1 kotak nasi untuk saudara kita. 20 foto terbaik akan mendapatkan voucher belanja senilai 200K.</p>\r\n\r\n<p>#TradisiAsik<br />\r\nKompetisi foto yang menantang para fans untuk mengirimkan foto tentang Tradisi Ramadan dan Idul Fitri di Instagram <a href=\"http://instagram.com/daihatsuind\" target=\"_blank\">@daihatsuind</a>. 10 foto terbaik pilihan juri akan mendapatkan masing-masing voucher belanja senilai 200K.</p>\r\n\r\n<p>&nbsp;</p>', null, 'image_20160921072837.png', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');
INSERT INTO `devents` VALUES ('5', 'Daihatsu Robot Competition', '<p>Daihatsu Robot Competition adalah kompetisi membuat sebuah robot yang terbuka untuk umum. Peserta kompetisi diharuskan untuk membuat sebuah robot yang dapat bergerak mandiri dan dapat mendorong robot lawan ke luar arena yang sudah dibuat sesuai peraturan kompetisi. Robot harus dapat beroperasi secara mandiri atau robot autonomous.<br />\r\n<br />\r\nSetiap peserta hanya boleh membawa dan menggunakan 1 (satu) unit robot pada kompetisi ini dan tidak diperkenankan memiliki atau menggunakan benda tajam, api, air dan atau cairan lain yang dapat merusak arena kompetisi, membahayakan peserta kompetisi yang lain atau membahayakan robot yang lain. Pendaftaran Daihatsu Robot Competition&nbsp; mulai dibuka sejak 12 Mei - 25 Juli 2016. Untuk pendaftaran, silahkan download dan isi <a href=\"http://daihatsu.co.id/formulir/robot-competition-2016.docx\">formulir</a> kemudian email ke daihatsurobotcompetition2016@gmail.com</p>', null, 'image_20160921072922.jpg', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');
INSERT INTO `devents` VALUES ('6', 'Daihatsu Lipsync Competition', '<p>Melihat antusiasme masyarakat Indonesia terhadap dunia musik, Daihatsu ingin memberikan pengalaman berbeda kepada penikmat musik tanah air untuk mengekspresikan jiwa musiknya dengan mengadakan Daihatsu Lipsync Competition yakni sebuah ajang lomba lipsync yang diperuntukkan bagi masyarakat umum, karyawan DaihatSu dan Value Chain Daihatsu dengan total hadiah sebesar Rp 10juta.<br />\r\n<br />\r\nPeserta yang berhak mengikuti kompetisi ini harus berada direntang usia 15-30 tahun. Setiap peserta boleh menentukan lagu pilihannya sendiri dan diberikan kebebasan untuk berkreasi membawakan lagu bebas tersebut secara berbeda dengan durasi lagu wajib dan lagu pilihan yang dinyanyikan peserta maksimal 5 (lima) menit. Pendaftaran mulai dibuka sejak 12 Mei-22 Juli 2016. Untuk pendaftaran, silahkan download dan isi formulir kemudian email ke daihatsulypsinc2016@gmail.com</p>', null, 'image_20160921073013.jpg', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');
INSERT INTO `devents` VALUES ('7', 'Pestaria Ayla', '<p style=\"text-align:justify\">PT Astra Daihatsu Motor (ADM) kembali menggelar Pesta Sahabat Daihatsu 2015 yang digelar di Gedung Balai Kartini Jakarta, Sabtu tanggal 5 Desember. Acara tahunan yang sudah keempat kalinya digelar, ini merupakan penutup seluruh rangkaian aktifitas Daihatsu sepanjang tahun 2015.</p>\r\n\r\n<p style=\"text-align:justify\">Mengusung tema &quot;Terima Kasih Sahabat&quot; yang dikemas dalam konsep Fun &amp; Elegant dengan nuansa Broadway sebagai apresiasi kepada Sahabat Daihatsu, Pelanggan dan Relasi, serta para Pemenang berbagai kompetisi yang diselenggarakan tahun ini.</p>\r\n\r\n<p style=\"text-align:justify\">Acara yang dipandu oleh MC kondang Boy William dan Karina, membuat suasana Balai Kartini menjadi meriah. Diawali dengan penampilan dari Kids Choir yaitu pemenang kids Choir Competition di GIIAS 2015 lalu, yang membuat para penonton tercengang dengan penampilan lincah dan suara yang memukau mereka.</p>\r\n\r\n<p style=\"text-align:justify\">Melalui Pesta Sahabat 2015 ini, Daihatsu Indonesia berusaha menyampaikan misinya untuk memberikan kepuasan 100% kepada pelanggan dengan tagline Fun, Friendly dan Reliable. Uniknya misi ini disampaikan dengan cara berbeda yakni melalui drama musikal yang diperankan oleh Boy William bersama para Sahabat Daihatsu.</p>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"height:214px; width:30%\">\r\n	<tbody>\r\n		<tr>\r\n			<td><img alt=\"\" src=\"/assets/js/kcfinder/upload/images/pestasahabat1.jpg\" style=\"height:200px; margin:0px auto; position:relative; width:356px\" /></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Acara dilanjutkan dengan mengumumkan pemenang di tiga program yang diadakan Daihatsu Indonesia selama penghujung 2015 ini yaitu Xenia Setia, SMK Skill Competition dan Untukmu Sahabat. Pemenang Xenia Setia mendapat hadiah rekondisi mobil, pemenang SMK Skill Competition mendapatkan hadiah jalan-jalan ke Malaysia sementara Pemenang Untukmu Sahabat mendapat kesempatan mewujudkan mimpi-mimpinya bersama Raditya Dika yang juga hadir memeriahkan Pesta Sahabat Daihatsu.</p>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"height:214px; width:30%\">\r\n	<tbody>\r\n		<tr>\r\n			<td><img alt=\"\" src=\"/assets/js/kcfinder/upload/images/pestasahabat2.jpg\" style=\"height:200px; width:356px\" /></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Acara dilanjutkan dengan penampilan dari Kahitna yang berhasil mengguncang Balai Kartini siang itu. Kemeriahan Pesta Sahabat 2015 pun diakhiri dengan pembagian door prize dan ucapan terima kasih dari Daihatsu Indonesia atas kesetiaan para Sahabat Daihatsu selama ini. Terima Kasih Sahabat, sampai jumpa di Pesta Sahabat tahun depan :)</p>', '', 'image_20160921073847.jpg', 'on', '2016-09-21', '2016-09-22', 'Luthfi Aziz Nugraha');
INSERT INTO `devents` VALUES ('8', 'Pesta Sahabat 2015', '<p>PestaSahabatChallenge mengajak Sahabat Daihatsu untuk hadir memeriahkan Pesta Sahabat 2015. Ada 50 golden tiket dan hadiah puluhan juta rupiah. Yuk ikuti kuis-kuisnya di sosial media Daihatsu! Tunggu apalagi, rebut segera tiket dan hadiahnya.</p>\r\n\r\n<p>SYARAT DAN KETENTUAN</p>\r\n\r\n<ul>\r\n	<li>Wajib untuk like facebook Daihatsu dan follow twitter Instagram @DaihatsuInd</li>\r\n	<li>Mengikuti kuis dan syarat kuis di facebook dan instagram Daihatsu</li>\r\n	<li>Pemenang dipilih berdasarkan keputusan juri dan bersifat mutlak</li>\r\n	<li>Pemenang yang berasal dari luar JADETABEK, transport dan hotel akan disediakan oleh pihak Daihatsu</li>\r\n	<li>50 pemenang berkesempatan untuk menghadiri acara eksklusif Pesta Sahabat 2015 di Jakarta dan masing-masing pemenang mendapatkan 3 buat tiket Pesta Sahabat 2015</li>\r\n	<li>Review acara Pesta Sahabat 2015 dikirim via email ke dai.indonesia@gmail.com</li>\r\n	<li>Peserta yang tidak mengkonfirmasi data lebih dari batas waktu yang ditentukan akan dinyatakan gugur</li>\r\n	<li>Panitia akan mendiskualifikasi peserta dari Pesta Sahabat Challenge, jika ditemukan kecurangan</li>\r\n	<li>Panitia akan menghapus segala materi yang mengandung unsur pornografi, SARA, dan tidak sesuai dengan ketentuan lomba</li>\r\n	<li>Semua hal yang sudah dikirimkan menjadi hak milik Daihatsu, Daihatsu berhak mempublikasikannya di semua media Daihatsu</li>\r\n	<li>Hati-hati penipuan! Daihatsu tidak memungut biaya apapun untuk promosi ini</li>\r\n	<li>Info lebih lanjut mengenai lomba langsung ditanyakan di facebook Daihatsu Indonesia</li>\r\n	<li>Syarat dan ketentuan diatas dapat berubah sewaktu-waktu tanpa pemberitahuan terlebih dahulu</li>\r\n</ul>', null, 'image_20160921073945.png', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');
INSERT INTO `devents` VALUES ('9', 'Terios Blog Competition', '<p>Nikmati berpetualang seru bersama Daihatsu di Terios 7 Wonder Borneo Wild Adventure. Kamu akan diajak untuk menjelajah keindahan alam liar Kalimantan dengan Mobil New Daihatsu Terios.</p>\r\n\r\n<p>Yuk, tuliskan artikel tentang keinginan atau pengalamanmu mengunjungi salah satu atau lebih destinasi Borneo Wild Adventure menggunakan New Daihatsu Terios sekarang! Bakal ada tiga blogger terbaik yang berkesempatan menjelajah alam liar Kalimantan bersama New Terios.</p>\r\n\r\n<p>Daftar di <strong>sini</strong></p>', null, 'image_20160921074025.jpg', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');
INSERT INTO `devents` VALUES ('10', 'Ngabuburit Daihatsu', '<h3>Syarat &amp; Ketentuan</h3>\r\n\r\n<ul>\r\n	<li>Voucher hanya berlaku di cabang yang tertera di dalam voucher.</li>\r\n	<li>Voucher berlaku hingga tanggal 12 Juli 2015.</li>\r\n	<li>Sertakan Fotocopy KTP saat penukaran voucher</li>\r\n	<li>Kelebihan tagihan dibayar langsung oleh pemegang voucher.</li>\r\n	<li>Bukti Voucher harap diprint dan ditunjukan kepada petugas Gubuk makan mang engking saat ingin menggunakannya.</li>\r\n	<li>Dapatkan hadiah tambahan dengan upload foto selfie mu ke media social daihatsu dengan voucher buka puasa di Gubuk Makan Mang Engking</li>\r\n</ul>\r\n\r\n<p><br />\r\nYuk, ikutan #NgabuburitDaihatsu! Ngabuburit bareng Daihatsu kamu nggak perlu antri dan ikutan macet di jalan lho! Karena ngabuburit di sini kamu bakal berkesempatan untuk menangkan voucher berbuka bersama dan pulsa total Jutaan Rupiah lho :D</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tunggu apalagi! Yuk ikutan #NgabuburitDaihatsu sekarang</p>', null, 'image_20160921074121.jpg', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');
INSERT INTO `devents` VALUES ('11', 'Kids Choir Competition', '<p>DAIHATSU GIIAS KIDS CHOIR COMPETITION 2015 adalah sebuah kompetisi paduan suara siswa/i tingkat SD. Adapun yang dapat mengikuti program ini adalah siswa/i maksimal berusia 13 tahun dengan tim sebanyak minimal 15 orang dan maksimal 25 orang, termasuk conductor dan pemusik.</p>\r\n\r\n<p>Pada kompetisi DAIHATSU GIIAS KIDS CHOIR COMPETITION 2015 ini, setiap peserta akan membawakan 2 (dua) lagu, yaitu lagu wajib dan bebas. Lagu wajib akan ditentukan oleh panitia, sementara lagu bebas yang dibawakan adalah bernuansa pop, daerah, dan lagu yang dinyanyikan secara medley atau acapella.</p>\r\n\r\n<p>Dalam penampilan Peserta lomba DAIHATSU GIIAS KIDS CHOIR COMPETITION 2015 wajib menggunakan kostum bertemakan &quot;EXPERIENCE WITH YOUR BESTFRIEND&quot; sesuai dengan kreasi masing-masing tim.</p>\r\n\r\n<p>Untuk pendaftaran silahkan download &amp; isi formulir dan di email ke DAIHATSUKIDSCHOIR2015@gmail.com</p>', null, 'image_20160921074235.jpg', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');
INSERT INTO `devents` VALUES ('12', 'Miss Daihatsu Competition', '<p>MISS DAIHATSU COMPETITION adalah sebuah kompetisi untuk wanita berusia diatas 17 tahun dan maximal 27 tahun, dengan tinggi minimal 168 CM, berat badan proposional, Warga Negara Indonesia (WNI) , good looking dan berwawasan luas dimana peserta diharuskan untuk mempersiapkan sebuah penampilan berupa menyanyi, bermain alat musik, dan bakat lainnya saat babak penyisihan.</p>\r\n\r\n<p>Pemenang MISS DAIHATSU pada kompetisi ini dinilai berdasarkan kriteria seperti Catwalk, Foto Wajah, Kepercayaan Diri, Wawasan tentang daihatsu, Bakat, dan Wawasan lainnya.</p>\r\n\r\n<p>Untuk pendaftaran silahkan download &amp; isi formulir dan di email ke missdaihatsu2015@gmail.com</p>', null, 'image_20160921074632.jpg', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');

-- ----------------------------
-- Table structure for dgallery
-- ----------------------------
DROP TABLE IF EXISTS `dgallery`;
CREATE TABLE `dgallery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `picture` text,
  `status` enum('on','off') DEFAULT 'on',
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dgallery
-- ----------------------------
INSERT INTO `dgallery` VALUES ('1', 'Banner 1', 'image_20160921054246.jpg', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');
INSERT INTO `dgallery` VALUES ('2', 'Banner 2', 'image_20160921054303.jpg', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');
INSERT INTO `dgallery` VALUES ('3', 'Banner 3', 'image_20160921054329.png', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');
INSERT INTO `dgallery` VALUES ('4', 'Banner 4', 'image_20160921054346.png', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');

-- ----------------------------
-- Table structure for dkinerja
-- ----------------------------
DROP TABLE IF EXISTS `dkinerja`;
CREATE TABLE `dkinerja` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `desc` text,
  `status` enum('on','off') DEFAULT 'off',
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dkinerja
-- ----------------------------

-- ----------------------------
-- Table structure for dnews
-- ----------------------------
DROP TABLE IF EXISTS `dnews`;
CREATE TABLE `dnews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `desc` text,
  `vdesc` text,
  `picture` text,
  `status` enum('on','off') DEFAULT 'on',
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dnews
-- ----------------------------
INSERT INTO `dnews` VALUES ('1', 'Wholesales Daihatsu hingga Agustus 2016 Capai 116.', '<p>JAKARTA: PT Astra Daihatsu Motr (ADM) melewati bulan kedelapan di tahun 2016 dengan penjualan yang cukup baik. Selama periode Januari - Agustus 2016 wholesales Daihatsu berhasil mencapai 116.999 unit, atau pangsa pasar 16.9%. Sementara retailsales mencapai 117.838 unit, atau pangsa pasar 17,2%. Pencapaian ini menunjukkan ada peningkatan 3% untuk wholesales dan 8% untuk retailsales dibandingkan tahun lalu.</p>\r\n\r\n<p>Wholesales Daihatsu Januari-Agustus 2016 mencapai 116.999 unit, disumbang oleh tiga kontributor utama yakni Ayla, Xenia, dan Gran Max. Astra Daihatsu Ayla mencapai 29.975 unit atau 25.6%. Kontributor terbesar kedua adalah Great New Xenia sebanyak 29.383 unit atau 25.1%. Kontributor ketiga adalah Gran Max Pick Up (PU) mencapai 24.894 unit atau 21.3%. Selanjutnya adalah SUV Daihatsu Terios menyumbang 10.152 unit atau 8.7%, disusul Daihatsu Gran Max Mini Bus (MB) sebanyak 9.804 unit atau 8.4%, Astra Daihatsu Sigra sebesar 7.748 unit atau 6.6%, Daihatsu Luxio sebanyak 2.971 unit atau 2.5%, Daihatsu Sirion sebanyak 2.070 atau 1,8%.</p>\r\n\r\n<p>Retailsales Daihatsu Januari-Agustus 2016 mencapai 117.838 unit. Perincian retailsales tidak jauh berbeda dengan wholesales, dengan penopang utama Xenia, Ayla, dan Gran Max Pick Up (PU). MPV Great New Xenia berkontribusi terbanyak yaitu 32.004 unit atau 27.2%. Kontributor terbesar kedua adalah Astra Daihatsu Ayla yang mencapai 30.415 unit atau 25.8%. Gran Max Pick Up (PU) terjual 27.461 unit atau 23.3%. Kemudian Daihatsu Terios menyumbang 9.757 unit atau 8.3%, Gran Max Mini Bus 9.333 unit atau 7.9%, Astra Daihatsu Sigra sebesar 3.803 unit atau 3.2%, Daihatsu Luxio sebanyak 2.961 unit atau 2.5%, dan Sirion sebanyak 2.103 unit atau 1.8%.</p>\r\n\r\n<p>&nbsp;</p>', null, 'image_20160921080344.png', 'on', '2016-09-21', '2016-09-21', 'Luthfi Aziz Nugraha');
INSERT INTO `dnews` VALUES ('2', 'Daihatsu Memukau Sahabat di GIIAS 2016', '<p><strong>JAKARTA (22/8): </strong> Selama 11 hari pelaksanaan <strong>Gaikindo Indonesia International Auto Show (GIIAS) 2016</strong>, <em>booth</em> Daihatsu memikat banyak Sahabat Daihatsu dari penjuru Jabodetabek bahkan dari berbagi kota di Tanah Air untuk datang dan menyaksikan langsung <strong>Astra Daihatsu Sigra &#39;Sahabat Impian Keluarga&#39;</strong>. Selain itu, Daihatsu berhasil menjadikan ajang <strong>GIIAS 2016</strong> sebagai ajang edukasi untuk memperkenalkan inovasi-inovasi Daihatsu dalam mengembangkan mobil kompak (<em>compact car</em>).</p>\r\n\r\n<p>Hadir dengan tema <strong><em>&quot;FUNTASTIC EXPERIENCE&quot;</em></strong> yang selaras dengan tagline &#39;<strong>Sahabat Daihatsu</strong>&#39;, menjadikan booth Daihatsu sebagai arena yang menyenangkan dengan nuansa ceria, edukatif, dan interaktif bagi pengunjung. Selama kegiatan GIIAS 2016 berlangsung, <em>booth</em> Daihatsu menggelar berbagai acara seperti <em>interactive games, </em> serta berbagai kompetisi seperti <em>Show Choir, Lipsync Battle</em>, Robotica, dan penampilan spesial dari Sheila on 7.</p>\r\n\r\n<p>Meskipun pada ajang ini, Daihatsu lebih menitikberatkan pada pengenalan teknologi-teknologi terkini Daihatsu, namun Daihatsu juga memberikan kemudahan kepada Sahabat Daihatsu untuk memiliki Daihatsu idamannya melalui program penjualan menarik yaitu angsuran mulai 1 jutaan, gratis asuransi, special <em>credit package</em>, dan <em>direct gift</em>. Berbagai program penjualan ini menarik minat Sahabat Daihatsu untuk melakukan transaksi pemesanan kendaraan (SPK), tercatat sampai dengan hari terakhir GIIAS 2016 terjadi <strong>1.148 SPK</strong> di <em>booth</em> Daihatsu. <strong>Astra Daihatsu Sigra</strong> yang baru saja resmi diluncurkan pada GIIAS 2016 ini menjadi tipe kendaraan yang paling banyak dipesan, yaitu sebesar <strong>594 unit (51.74%).</strong> Pemesanan tipe kendaraan lain berturut-turut dari terbanyak adalah <strong>Daihatsu Xenia Sahabat Keluarga</strong>, mendapatkan jumlah pemesanan <strong>206 unit (17.94%), Astra Daihatsu Ayla Sahabat Seru</strong> mendapatkan jumlah pemesanan sebesar <strong>151 unit (13.15%)</strong>, diikuti oleh <strong>Daihatsu Terios</strong> sang <strong>Sahabat Petualang</strong> yang dipesan sebanyak <strong>80 unit (6.97%).</strong> Sementara itu, Daihatsu Sirion mendapatkan SPK sebanyak <strong>34 unit (2.96%)</strong>, Daihatsu Luxio sebanyak <strong>15 unit (1.31%), dan Copen sebanyak 1 unit (0.09%)</strong>. Selain kendaraan untuk keluarga, kendaraan komersial unggulan Daihatsu yaitu GranMax Minibus dan Pick Up juga menjadi incaran pebisnis yang mengunjungi GIIAS 2016 kali ini, terbukti <strong>GranMax MB</strong> sudah dipesan <strong>36 unit (3.14%)</strong> dan <strong>GranMax PU</strong> sebesar <strong>31 unit (2.70%)</strong>.</p>\r\n\r\n<p>&quot;Kami bersyukur respon pengunjung terhadap produk-produk Daihatsu sangat menggembirakan dan positif selama <strong>GIIAS 2016</strong> ini. Sampai dengan berakhirnya acara, Daihatsu telah membukukan hasil SPK melebihi target yang telah kami tetapkan. Kami juga berharap agar pameran ini semakin mendekatkan Daihatsu dengan para Sahabat dan memberikan kontribusi bagi pertumbuhan pasar dan industri otomotif nasional,&quot; ujar <strong>Hendrayadi Lastiyoso, Marketing &amp; CR Division Head, PT Astra International Tbk - Daihatsu Sales Operation.</strong></p>', null, 'image_20160921080424.jpg', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');
INSERT INTO `dnews` VALUES ('3', 'Daihatsu Tampilkan 2 Unit Japan Domestic Model di ', '<p>JAKARTA (19/8) : Sebagai spesialis pembuat mobil kompak (<em>compact car</em>) Daihatsu selalu mengembangkan kendaraan hemat energi dan ramah lingkungan sesuai <em>trend</em> otomotif masa depan. Pada ajang bergengsi Gaikindo Indonesia International Auto Show (GIIAS) 2016, Daihatsu konsisten menampilkan unit CBU Jepang yang bertujuan untuk memperkenalkan inisiatif Daihatsu dalam mendukung gaya hidup yang sesuai dengan perkembangan zaman dan teknologi. Daihatsu membawa dua unit <em>Japan Domestic Model yaitu Cast Sport, dan Copen Cero.</em></p>\r\n\r\n<p><strong><em>Cast Sport</em></strong> hadir panjang 3.395 mm dengan mesin 660 cc turbo tiga silinder, dan <em>CVT transmission</em>. Dengan bagian aero yang dibuat secara khusus untuk menampilkan kesan roda dan interior yang <em>sporty</em>. Suspensi yang <em>sporty</em> menghasilkan kestabilan saat bermanuver di jalan raya. Dilengkapi dengan stir MOMO &amp; <em>paddle shift</em> akan memudahkan pengemudi saat berkendara di jalan raya. Mengusung mesin 3 silinder 600 cc turbo, serta menggunakan platform <em>Front Engine Front Drive (FF)</em> ini lincah untuk digunakan sehari-hari.</p>\r\n\r\n<p><strong><em>Copen Cero</em></strong> hadir dengan panjang 3.395 mm dengan mesin 660 cc turbo tiga silinder, dan <em>manual transmission</em>. <em>Copen Cero</em> hadir dengan performa dan stabilitas yang baik dengan D Frame, panel body menggunakan material resin dan kesenyapan suara yang baik. Copen Cero hadir dengan gaya seperti tetesan air yang bergerak sangat cepat dengan mesin 3 silinder 600 cc turbo.</p>\r\n\r\n<p>&quot;Sebagai produsen mobil kompak terkemuka, Daihatsu ingin menunjukkan banyak model-model menarik yang dibuat Daihatsu. Walaupun dijual di Jepang, kami ingin masyarakat Indonesia bisa melihat keunikan model Daihatsu tersebut di ajang GIIAS 2016 ini,&quot; ujar Pradipto Sugondo, <em>Executive Officer Research and Development</em> PT Astra Daihatsu Motor.</p>\r\n\r\n<p>&nbsp;</p>', null, 'image_20160921080504.jpg', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');
INSERT INTO `dnews` VALUES ('4', 'Daihatsu Terus Memberikan yang Terbaik', '<p>JAKARTA (18/8): Kepercayaan Sahabat Daihatsu kepada produk-produk Daihatsu tentu bertambah dengan hadirnya layanan terbaik yang diberikan oleh Daihatsu mulai dari saat pembelian unit Daihatsu baru, sampai dengan layanan purna jual yang ada di outlet-outlet resmi Daihatsu di seluruh Indonesia.</p>\r\n\r\n<h3>PERKEMBANGAN OUTLET DAIHATSU</h3>\r\n\r\n<p>Daihatsu terus melakukan perluasan daerah jangkauannya melalui menambahan outlet baru, maupun menstandardisasikan outlet yang sudah ada. Outlet-outlet Daihatsu yang distandardisasi telah telah menerapkan standar global Daihatsu, baik eksterior maupun interior, dan dilengkapi sarana yang lengkap, nyaman, serta modern. Outlet Daihatsu mengimplementasikan konsep standarisasi yang sesuai dengan <em>brand campaign</em> &quot;Daihatsu Sahabatku&quot; seperti terlihat di atribut outlet dan seragam frontliners.</p>\r\n\r\n<p>Outlet Daihatsu terbagi menjadi dua jenis, yaitu tipe V (Vehichle) dan VSP (Vehicle, Service, Part). Tipe oulet V hanya melayani penjualan unit-unit baru Daihatsu, sementara outlet VSP melayani penjualan unit baru, layanan purna jual, dan penjualan spare part resmi Daihatsu. YTD Juli 2016, Outlet resmi Daihatsu di seluruh Indonesia sudah berjumlah 221 outlet, yang terdiri dari 137 outlet VSP (Vehicle, Service, Part) dan 84 outlet V (Vehicle). Dari 221 outlet Daihatsu yang ada saat ini, outlet yang sudah distandardisasikan sebanyak 168 outlet. Pada tahun 2016, Daihatsu berencana untuk menambah 7 outlet, yaitu terdiri dari 1 outlet VSP dan 6 outlet V sehingga di akhir tahun jumlah outlet Daihatsu adalah 228 outlet, terdiri dari 139 VSP dan 89 V.</p>\r\n\r\n<h3>DAIHATSU INDONESIA SERVICE MANAGEMENT (DISM)</h3>\r\n\r\n<p>Saat ini layanan purnajual (after sales service) juga menjadi faktor penting bagi calon pelanggan sebelum memutuskan pembelian mobil. Melalui slogan <strong><em>&quot;Daihatsu Cares for You Better&quot;</em></strong>, Daihatsu terus mengembangkan layanan purnajual untuk meningkatkan kepuasan pelanggan. Dengan tujuan untuk memberikan kualitas kerja bengkel yang lebih baik dan waktu pengerjaan yang lebih cepat kepada pelanggan saat melakukan service kendaraan, Daihatsu mengembangkan standar baru operasional bengkel, yang disebut <strong>Daihatsu Indonesia Service Management</strong>. Melalui penerapan DISM, bengkel-bengkel resmi Daihatsu memiliki standar operasional baru yang lebih bersahabat sesuai dengan kebutuhan pelanggan. Untuk mencapai tujuan, DISM dijalankan dengan langkah,yaitu <strong>reminder Service, Penerimaan (Reception), Proses Servis (Production), Pemeriksaan Akhir (Final Inspection), Penyerahan Kendaraan (Delivery), Konfirmasi Hasil Perkerjaan (Follow Up H+3)</strong></p>\r\n\r\n<p>Saat ini, DISM dipioritaskan di 12 bengkel resmi di kota-kota besar yang melayani permintaan yang tinggi, antara lain Astra Daihatsu BSD, Astra Daihatsu Cibubur, Astra Daihatsu Cibeureum-Bandung, Astra Daihatsu Jl. Majapahit-Semarang, Astra Daihatsu Malang, Astra Daihatsu Jl. Sisingamangaraja-Medan, Astra Daihatsu Samarinda, Astra Daihatsu Makassar, Tunas Matraman, ASCO Bekasi, Astrido Otista, Armada Auto Tara Depok, dan akan terus diperluas hingga ke seluruh wilayah di Indonesia.</p>\r\n\r\n<p>Dengan semua fasilitas dan pelayanan yang ada, outlet Daihatsu menjadi <strong>One Stop Solution</strong> bagi para Sahabat Daihatsu yang datang ke outlet resmi Daihatsu. Sahabat Daihatsu akan mendapatkan pelayanan penjualan, seperti perhitungan cash/kredit dan asuransi yang berkerja sama dengan leasing pembayaran, maupun pelayanan purna jual seperti perawatan berkala, perbaikan umum dan penyediaan suku cadang asli secara mudah dan cepat. Hal ini menjadi tujuan Daihatsu agar semakin dekat dengan Sahabat Daihatsu di manapun.</p>\r\n\r\n<h3>PENCAPAIAN PENJUALAN</h3>\r\n\r\n<p>Salah satu bukti bahwa Daihatsu senantiasa terus memberikan kepuasan kepada Sahabat Daihatsu adalah hasil penjualan Daihatsu yang meningkatkat dibandingkan tahun lalu (YTD Juni) di tengah kondisi kompetisi pasar yang semakin berat. Daihatsu berhasil mencatatkan pertumbuhan penjualan yang positif. Penjualan Daihatsu sesuai retail sales Gaikindo YTD Juni 2016 tercatat sebanyak 89.110 unit, angka penjualan Daihatsu ini menunjukan peningkatan jika dibandingkan dengan penjualan pada periode yang sama pada tahun sebelumnya yang tercatat sebesar 85.116 unit atau naik sebesar 2.06 %. Hal ini menunjukkan bahwa merek Daihatsu masih tetap menjadi pilihan terbaik bagi Sahabat Daihatsu di Indonesia.</p>\r\n\r\n<p>Astra Daihatsu Ayla menjadi kontributor terbesar, yaitu terjual sebanyak 25.004 unit (28.06%), disusul oleh MPV kebanggaan Indonesia Daihatsu Xenia &#39;Sahabat Keluarga&#39; memberikan kontribusi sebesar 27.84% atau 24.810 unit, Daihatsu Gran Max PU sebesar 21.038 (23.61%), Daihatsu Terios sebesar 7.339 unit (8.24%), Daihatsu Gran Max MB sebesar 6.958 unit (7.81%) , Daihatsu Luxio 2.300 (2.58 %), dan Daihatsu Sirion 1.660 (1.86%)</p>\r\n\r\n<p>Dari segi persebaran penjualan, DKI Jakarta dan sekitar masih menjadi kontributor terbesar yaitu sebesar 35.70%, kemudian disusul oleh Jawa Timur (17.25%) dan Sumatera (15.22%). Wilayah Jawa Tengah memberi kontribusi penjualan sebesar 9.81%, Indonesia Bagian Timur sebesar 7.64%, sedangkan Kalimantan memberi kontribusi sebesar 6.34%.</p>\r\n\r\n<p>&quot;Daihatsu Indonesia senantiasa ingin mendekatkan diri dan memberikan pelayanan yang terbaik untuk seluruh Sahabat Daihatsu di Indonesia. Kami senantiasa berkomitmen untuk terus melakukan improvement baik dari segi penjualan maupun purna jual. Pencapaian Daihatsu selama ini menunjukkan menunjukkan bahwa merek Daihatsu masih tetap menjadi pilihan terbaik bagi Sahabat Daihatsu di Indonesia,&quot; ungkap <strong>Hendrayadi Lastiyoso, Marketing &amp; CR Division Head PT Astra International Tbk. - Daihatsu Sales Operation.</strong></p>', null, 'image_20160921080544.jpg', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');
INSERT INTO `dnews` VALUES ('5', 'Daihatsu Hadirkan 3 Unit Special Edition pada GIIA', '<p>JAKARTA (15/8) : Sebagai produsen mobil kompak terbesar di Indonesia, PT Astra Daihatsu Motor (ADM) konsisten melakukan inovasi untuk mengembangkan varian yang sesuai dengan kebutuhan masyarakat Indonesia. Pada ajang Gaikindo Indonesia International Autoshow (GIIAS) 2016 Daihatsu menampilkan 3 model special edition yaitu <strong><em>Xenia Custom (25 unit), Ayla Custom (20 unit)</em></strong>, dan <strong><em>Terios Black Edition (10 unit).</em></strong></p>\r\n\r\n<p><strong>Xenia Custom</strong> hadir dengan konsep <em>More Tough and Comfort.</em> Sahabat keluarga Indonesia ini hadir dengan tambahan eksterior berupa <em>Black Roof; Roof Rail; Body sticker &amp; emblem; serta R15 Polished Alloy</em> yang hadir dalam 2 <em>tone colour, white and black</em>. Di bagian interior, Xenia Custom dilengkapi dengan <em>Center Cluster, Door Arm Rest,</em> dan <em>Dashboard List Ornament with Carbon Ornament; 2Din Audio with 2 Way Miracast; Steering with Leather Warp &amp; Audio Switch; Shift Lever Knob</em> dan <em>Door Trim Ornament with Leather Warp</em>; serta <em>seat with Leather Material</em>, hanya tersedia di GIIAS ini dapat dibawa pulang dengan harga Rp. 211.600.000,- untuk tipe manual dan Rp. 222.600.000,- untuk tipe <em>automatic.</em></p>\r\n\r\n<p><strong>Ayla Custom</strong> hadir dengan konsep <em>Young and Sporty</em>. Unit mobil yang menjadi sahabat seru ini menarik perhatian pengunjung dengan warna merah, hadir dengan tambahan pada eksterior berupa <em>Black Outer Mirror; Body Sticker &amp; Emblem; Side Skirt;</em> serta <em>R14 Polished Alloy</em>. Pada bagian interior, mobil ini dilengkapi dengan <em>Upper Center Cluster with Carbon Ornament, 2Din Audio with 2 Way Miracast; serta Seat with Leather Material</em>. Mobil yang dilengkapi dengan <em>side impact beam </em>dan <em>dual SRS airbag</em> ini dapat dibawa pulang dengan harga Rp. 124.300.000,- untuk tipe manual dan Rp. 133.350.000,- untuk tipe <em>automatic</em>.</p>\r\n\r\n<p><strong>Terios Black Edition</strong> hadir sebagai sahabat petualang keluarga Indonesia, Terios memiliki berbagai sisi potensial untuk dimodifikasi. Tim Daihatsu memodifikasi dengan perubahan di interior dan eksterior. Eksterior <em>Terios Black Edition</em> semakin gagah dengan <em>Body Color Outer Mirror with Red Ornament; Window List</em> dan <em>Fender Garnish with Smoke Painting; Side Stone Guard, Aloy Wheel with Black Gloss; Grille Accent with Red Painting, Body Sticker; Rear Parking Camera; Smoke Rear Lamp;</em> serta <em>Spare Wheel Cover with Red List</em>. Di bagian interior, Terios Black Edition juga hadir dengan tambahan <em>Dashboard Garnish &amp; Door Assist Grip with Hydro Printing; Meter Cluster with Black Color, Leather Warp Steering Wheel &amp; Shift Knob Lever with Red Stitch;</em> serta <em>Seat &amp; Door Trim Ornament with Leather Material</em>. <em>Terios Black Edition</em> ini khusus disediakan bagi sahabat petualang dengan harga Rp. 238.000.000,- untuk tipe <em>manual</em> dan Rp. 252.100.000,- untuk tipe <em>automatic</em>.</p>\r\n\r\n<p>&quot;Edisi <em>special edition</em> ini dibuat khusus untuk GIIAS 2016 dengan unit yang sangat terbatas. Jika sudah terjual sesuai unit <em>Special edition</em> yang dibuat tersebut, kami tidak akan mengeluarkan tambahan unit lainnya. Siapa cepat dia dapat,&quot; ujar Pradipto Sugondo, <em>Executive Officer Research and Development Division PT Astra Daihatsu Motor</em>.</p>', null, 'image_20160921080630.jpg', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');
INSERT INTO `dnews` VALUES ('6', 'Tiga Mobil Konsep Daihatsu Tampil di GIIAS 2016', '<p>JAKARTA (15/8) : Sebagai spesialis pembuat mobil kompak (compact car) Daihatsu selalu mengembangkan kendaraan hemat energi dan ramah lingkungan sesuai trend otomotif masa depan. Pada ajang bergengsi Gaikindo Indonesia International Auto Show (GIIAS) 2016, Daihatsu konsisten menampilkan mobil konsep yang bertujuan untuk memperkenalkan inisiatif Daihatsu dalam mendukung gaya hidup yang sesuai dengan perkembangan zaman dan teknologi. Kali ini Daihatsu memperkenalkan mobil konsep yang sebelumnya pernah ditampilkan pada perhelatan akbar Tokyo Motor Show 2015 silam, yaitu Tempo, Hinata dan Cast Activa</p>\r\n\r\n<p><strong>Tempo</strong> dirancang sebagai mobil toko yang praktis, memudahkan untuk menghampiri pelanggan dan membuka toko secara langsung. Dengan ide dasar a new genre space commercial vehicle, yang di desain menggunakan platform Front Engine Front Drive (FF). Sumber tenaga dari mobil ini berasal dari mesin tiga silinder dengan kapasitas 660cc, dengan menggunakan transmisi sistem continuous variable transmission (CVT). Dengan ruang yang lebih luas, dapat menjadi pilihan untuk jasa catering, dengan berbagai kelebihan, ruang yang fleksibel, bodi samping yang dilengkapi lampu LED yang dapat dibuka dan difungsikan sebagai kanopi, serta dilengkapi dengan fasilitas digital signage di kedua sisi, untuk berpromosi menjadikan mobil ini layak dijadikan mobil toko yang fungsional dan stylish.</p>\r\n\r\n<p><strong>Hinata</strong> hadir sebagai mobil konsep yang nyaman dan menarik. Pada bagian interior, bagian dashboard tampil unik dengan aksen motif kayu serta layar sentuh. Mobil ini juga memiliki daya tarik tersendiri, dengan kabin yang lebih luas karena pintu dapat terbuka berhadapan, tanpa memiliki pilar B. Sumber tenaga dari mobil ini berasal dari mesin tiga silinder dengan kapasitas 660cc, dengan menggunakan transmisi sistem continuous variable transmission (CVT). Bepergian juga menjadi lebih nyaman, karena pintu gatefold ganda yang memungkinkan variasi tempat duduk yang beragam, dari terrace bench seat hingga face to face seat.</p>\r\n\r\n<div class=\"pd-second-container\">\r\n<p>Cast Activa hadir dengan mudah menyesuaikan kondisi jalan sehingga menghasilkan akselerasi yang nyaman dan halus. Down hill assist &amp; Tire grip control mendukung untuk berkendara off road. Dengan ban 15&quot; dan lift up 80mm, Daihatsu Cast Activa cocok untuk berpetualan di medan off road</p>\r\n\r\n<p>&quot;Sebagai produsen global mobil kompak terkemuka, Daihatsu konsisten menampilkan mobil konsep. Kali ini, di Pameran Otomotif Gaikindo, GIIAS 2015, Kami membawa tiga mobil konsep yang sebelumnya mendapat respon yang cukup positif di Jepang. Model-model ini merupakan hasil karya tim Research and Development Daihatsu di Jepang,&quot; ujar Pradipto Sugondo, Executive Officer Research and Development PT Astra Daihatsu Motor.</p>\r\n\r\n<p>&nbsp;</p>\r\n</div>\r\n\r\n<div class=\"clear\" style=\"height:20px;\">&nbsp;</div>', null, 'image_20160921080920.jpg', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');
INSERT INTO `dnews` VALUES ('7', 'R&D Daihatsu Tampilkan Keunggulan Teknologi Sigra ', '<p>JAKARTA (13/8) : PT Astra Daihatsu Motor sebagai spesialis produsen mobil kompak di Indonesia, kembali menghadirkan berbagai fasilitas menarik untuk Sahabat Daihatsu pada acara Gaikindo Indonesia International Auto Show (GIIAS) 2016. Hal ini tercermin pada booth Daihatsu dengan fasilitas Research &amp; Development (R&amp;D) Corner selalu memberikan berbagai program menarik, informatif dan juga edukatif.</p>\r\n\r\n<p>R&amp;D Corner di booth Daihatsu memamerkan kendaraan terbarunya, yaitu Astra Daihatsu Sigra yang hadir dengan tampilan Astra Daihatsu Sigra Cutting Body yang disebut Sigra Superior Points, dimana setengah bagian mobil ini memperlihatkan komponen dalam secara utuh dan menarik, setengahnya lagi tetap dalam kondisi mobil normal. Selain ingin berbagi pengetahuan, Daihatsu juga ingin memperkenalkan kepada masyarakat tentang sederet keunggulan Astra Daihatsu Sigra.</p>\r\n\r\n<p>Pada bagian mesin, Sigra menggunakan sistem FF (Front Engine Front Wheel Drive) yang dikembangkan dengan mengusung teknologi mesin terbaru Dual VVT-i dengan Drive by Wire System. Mesin ini menghasilkan tenaga dan torsi yang lebih besar, namun tetap halus, konsumsi bahan bakar yang lebih irit, efisien, dan ramah lingkungan. Dengan turning radius yang kecil, semakin memudahkan kendaraan ini saat berputar balik di jalan sempit sekalipun.</p>\r\n\r\n<p>Pada sisi eksterior, kendaraan ini mengadopsi konsep Sophisticated &amp; Dynamic Design. Selain tampil dengan desain yang stylish dan modern, bodi mobil ini juga memiliki desain struktur rangka berlapis yang kuat untuk melindungi penggunanya apabila terjadi benturan.</p>\r\n\r\n<p>Pada bagian Interior, kendaraan ini memiliki kabin luas, nyaman, dan fleksibel dengan 3 (tiga) baris kursi yang dapat dilipat untuk memudahkan akses keluar masuk penumpang. Sigra dilengkapi dengan fitur keamanan yang lengkap, seperti Collision Safety Body, dual SRS Airbag sebagai pengaman untuk pengemudi dan penumpang, Anti-locked Brake System (ABS) pada varian tertinggi, dan Immobilizer System, menjadikan kendaraan ini sebagai satu-satunya mobil LCGC dengan harga terjangkau, namun fitur berlimpah di kelasnya.</p>\r\n\r\n<p>Pada fasilitas yang terdapat di R&amp;D Corner pengunjung juga dapat melihat film singkat tentang uji ketangguhan Astra Daihatsu Sigra dalam melewati serangkaian tes kualitas dalam menghadapi berbagai kondisi jalan di Indonesia.</p>\r\n\r\n<p>Selain itu, tersedia juga sebuah diorama yang menunjukkan penerapan teknologi Daihatsu Hydrazine Fuel Cell City pada sebuah kota dimana bahan bakar tersebut digunakan untuk mendukung semua aktifitas kota tersebut agar lebih irit dan ramah lingkungan.</p>\r\n\r\n<p>�Kami menampilkan R&amp;D Corner sebagai sarana informasi dan edukasi bagi pengunjung agar dapat melihat berbagai keunggulan mobil Daihatsu. Kami berharap, selain memberikan hiburan, juga menjadi pengalaman yang tak terlupakan�, ujar Pradipto Sugondo, Executive Officer Research and Development PT Astra Daihatsu Motor.</p>', null, 'image_20160921081004.JPG', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');
INSERT INTO `dnews` VALUES ('8', 'Beragam Hadiah Menarik dari Daihatsu di GIIAS 2016', '<p>JAKARTA (12/8) : Selaras dengan tagline &quot;Daihatsu Sahabatku&quot; booth Daihatsu di GIIAS 2016 mengusung tema &quot;Funtastic Experience&quot; yang menyajikan booth yang ceria, program yang bersahabat dan menampilkan mobil-mobil yang dapat diandalkan.</p>\r\n\r\n<p>Tema ini menjadikan booth Daihatsu di GIIAS 2016 sebagai arena yang menyenangkan dengan nuansa ceria dan bersahabat, edukatif dan interaktif, menjadi pengalaman yang menyenangkan selama di GIIAS 2016.</p>\r\n\r\n<p>Selama pelaksanaan GIIAS 2016, Booth Daihatsu menggelar berbagai programbaik di panggung utama (on stage), maupun pertunjukan di luar panggung (non stage). Program on stage antara lain penampilan artis Sheila On 7, drama musical &quot;Dreams Comes True&quot; serta Hoverboard dance, pertunjukkan Puppetee, Costume Parade Carnival, serta ragam kompetisi berupa Show Choir, Lip Sync, dan robotica. Sedangkan untuk program non stage berupa permainan interaktif.</p>\r\n\r\n<p>Dengan konsep mengedukasi pengunjung, booth Daihatsu menyediakan lima permainan interaktif seperti Daihatsu Hidden Words, Eco Energy, Friendship Keychain, Sigra Spot The Differences, dan Dance Sahabat, dengan beragam hadiah langsung yang menarik. Program edukatif merupakan program yang sengaja diberikan kepada pengunjung, seperti pada tahun-tahun sebelumnya.</p>\r\n\r\n<p>Untuk mendapatkan hadiah, pengunjung hanya mengumpulkan poin dengan mengikuti berbagai permainan. Poin tersebut dapat ditukar langsung di counter Daihatsu dengan beragam hadiah menarik dari pencil; pencil box; mini fan; photo frame; toiletrees dan pengumpul poin terbanyak bisa mendapatkan boneka cantik.</p>\r\n\r\n<p>&quot;Sebagai Sahabat, Daihatsu menghadirkan program yang bukan hanya menarik tetapi juga mempunyai nilai edukasi. Kami mengajak para pengunjung untuk mencoba berbagai permainan yang telah disediakan, memahami pengetahuannya dan mendapat hadiah menarik,&quot; ujar Amelia Tjandra, Marketing Director PT Astra Daihatsu Motor.</p>', null, 'image_20160921081049.jpg', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');
INSERT INTO `dnews` VALUES ('9', 'Funtastic Experience Bersama Daihatsu di GIIAS 201', '<p>JAKARTA (11/8) : Sebagai spesialis produsen mobil kompak (compact car) di Indonesia, Daihatsu ikut serta dalam ajang Gaikindo Indonesia International Auto Show (GIIAS) 2016 yang berlangsung pada 11-21 Agustus 2016 di Indonesia Convention Exhibition (ICE), BSD City, Tangerang Selatan. Pembukaan Booth Daihatsu secara resmi dilakukan oleh Presiden Direktur PT Astra Daihatsu Motor (ADM) Sudirman MR dan Mr. Shigeharu Toda, Direktur Daihatsu Motor Company, disaksikan oleh Chief Executive PT Astra International Tbk - Daihatsu Sales Operation (AI-DSO) Djony Bunarto Tjondro beserta tamu undangan dan wartawan baik nasional maupun regional.</p>\r\n\r\n<p>Selaras dengan tagline &quot;Daihatsu Sahabatku&quot; booth Daihatsu tahun ini mengusung tema &quot;Funtastic Experience&quot;, menjadikan booth Daihatsu sebagai arena yang menyenangkan dengan nuansa ceria, edukatif dan interaktif bagi pengunjung. Menempati area Hall 8, Booth Daihatsu dibangun dengan konsep nyaman, fun dan bersahabat, yang tercermin pada pilihan warna dan tata ruang dengan konsep wooden block dengan LED yang atraktif.</p>\r\n\r\n<p>Untuk menunjukkan inisiatif dan inovasi Daihatsu dalam mengembangkan teknologi otomotif yang efisien dan ramah lingkungan, pada GIIAS 2016 ini Booth Daihatsu menampilkan total 15 unit yang dipamerkan, terdiri dari 3 unit mobil konsep, yaitu: Daihatsu Tempo, Daihatsu Hinata, dan Daihatsu Cast Activa ; 2 unit mobil model Jepang, yaitu Copen Cero, dan Cast Sport. Tidak hanya itu, booth Daihatsu juga menampilkan 3 unit Special Edition yaitu Ayla Custom, Xenia Custom, Terios Black Edition, serta Current Model, meliputi 6 Unit Astra Daihatsu Sigra, dan 1 unit Sirion Sport.</p>\r\n\r\n<p>Selama pelaksanaan GIIAS 2016, Booth Daihatsu menggelar berbagai program baik di panggung utama (on stage), maupun pertunjukan di luar panggung (non stage) yang berupa permainan interaktif. Booth Daihatsu dilengkapi dengan Research &amp; Development (R&amp;D) Corner, yang menampilkan Sigra Superior Point, dimana setengah bagian mobil diperlihatkan bagian dalamnya secara utuh dan menarik, setengahnya lagi tetap dalam kondisi mobil normal. Tersedia juga sebuah diorama yang menunjukkan penerapan teknologi fuel cell, pada sebuah kota dimana fuel cell tersebut dipakai untuk mendukung semua aktifitas kota tersebut. Tampil sebagai sahabat keluarga, booth Daihatsu menyediakan fasilitas Kids Corner &amp; Dealing Area. Tidak hanya itu, Daihatsu juga memberikan ragam informasi sepanjang GIIAS 2016 dengan memberikan daily press release membahas berbagai topik menarik terkait Daihatsu.</p>\r\n\r\n<p>&quot;Daihatsu mengusung konsep Funtastic Experience dalam pameran GIIAS kali ini. Kamiberharap para pengunjung dapat menikmati pengalaman yang mengesankan dalam berbagai program dan fasilitas yang telah disediakan oleh Daihatsu. Pada pameran ini kami juga memperkenalkan kepada publik salah satu produk andalan kami, Astra Daihatsu Sigra, kepada masyarakat Indonesia.&quot; ujar Sudirman MR, Presiden Direktur PT Astra Daihatsu Motor.</p>', null, 'image_20160921081145.jpg', 'on', '2016-09-21', null, 'Luthfi Aziz Nugraha');

-- ----------------------------
-- Table structure for dprofile
-- ----------------------------
DROP TABLE IF EXISTS `dprofile`;
CREATE TABLE `dprofile` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `desc` text,
  `status` enum('on','off') DEFAULT 'off',
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dprofile
-- ----------------------------

-- ----------------------------
-- Table structure for dsejarah
-- ----------------------------
DROP TABLE IF EXISTS `dsejarah`;
CREATE TABLE `dsejarah` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `desc` text,
  `status` enum('on','off') DEFAULT 'off',
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dsejarah
-- ----------------------------
INSERT INTO `dsejarah` VALUES ('1', 'Tes 1', '<p>tes</p>', 'off', null, '2016-09-20', 'Admin');
INSERT INTO `dsejarah` VALUES ('2', 'tes 2', '<p>tes</p>', 'off', null, '2016-09-20', 'Admin');
INSERT INTO `dsejarah` VALUES ('3', 'tes 3 ', '<p>tes</p>', 'off', null, '2016-09-20', 'Admin');
INSERT INTO `dsejarah` VALUES ('4', 'tes 4', 'tes', 'off', null, null, null);
INSERT INTO `dsejarah` VALUES ('5', ' tes 5', 'tes', 'off', null, null, null);
INSERT INTO `dsejarah` VALUES ('6', 'teste 6', '<p>tes</p>', 'off', null, '2016-09-20', 'Admin');
INSERT INTO `dsejarah` VALUES ('7', 'tes 7', 'tes', 'off', null, null, null);

-- ----------------------------
-- Table structure for dservice
-- ----------------------------
DROP TABLE IF EXISTS `dservice`;
CREATE TABLE `dservice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `desc` text,
  `status` enum('on','off') DEFAULT 'off',
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dservice
-- ----------------------------

-- ----------------------------
-- Table structure for dsorg
-- ----------------------------
DROP TABLE IF EXISTS `dsorg`;
CREATE TABLE `dsorg` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `grade` varchar(100) DEFAULT NULL,
  `parent` int(10) DEFAULT NULL,
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dsorg
-- ----------------------------
INSERT INTO `dsorg` VALUES ('1', 'Agus SM', 'Branch Manager', '0', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('2', 'Theo Ekandarista', 'Sales Supervisor', '1', '2016-09-24', '2016-09-24', 'Admin');
INSERT INTO `dsorg` VALUES ('3', 'Dedi Irawan', 'Administration Head', '1', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('4', 'Muhammad Hatta', 'Head Part Indirect', '1', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('5', 'Muhammad Surofi', 'Workshop Head', '1', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('6', 'Ary Prasetyo', 'General Affair', '1', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('7', 'Vera Ramayanti', 'Sales Counter', '2', '2016-09-24', '2016-09-24', 'Admin');
INSERT INTO `dsorg` VALUES ('8', 'Ahkamudin', 'Sales Supervisor', '1', '2016-09-24', '2016-09-24', 'Admin');
INSERT INTO `dsorg` VALUES ('9', 'Puji Astuti', 'Sales Counter', '7', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('10', 'Nurlina Muhammad', 'Sales Counter', '9', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('11', 'Katmadi', 'Samarinda Outlet', '2', '2016-09-24', '2016-09-24', 'Admin');
INSERT INTO `dsorg` VALUES ('12', 'Robiul Tri Kahupati', 'Samarinda Outlet', '11', '2016-09-24', '2016-09-24', 'Admin');
INSERT INTO `dsorg` VALUES ('13', 'Hartono', 'Samarinda Outlet', '12', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('14', 'Hendra Ananta', 'Samarinda Outlet', '13', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('15', 'Aknes Puspita Dewi', 'Samarinda Outlet', '14', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('16', 'Riffa Nutty Malibu', 'Samarinda Outlet', '15', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('17', 'Tri Wahyuning Tias', 'Samarinda Outlet', '16', '2016-09-24', '2016-09-24', 'Admin');
INSERT INTO `dsorg` VALUES ('18', 'Deny Eko wahyudi', 'Samarinda Outlet', '17', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('19', 'Iwan Siswanto', 'Samarinda Outlet', '18', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('20', 'Yantika Amelia', 'Samarinda Outlet', '19', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('21', 'Sy Rizki Amelia', 'Samarinda Outlet', '20', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('22', 'M.Refani Arfan', 'Samarinda Outlet', '21', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('23', 'Albet Gunadi', 'Samarinda Outlet', '22', '2016-09-24', '2016-09-24', 'Admin');
INSERT INTO `dsorg` VALUES ('24', 'Rachmatussaban', 'Samarinda Outlet', '23', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('25', 'Irwan Permana Jaya', 'Samarinda Outlet', '24', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('26', 'Rizki Akbar', 'Samarinda Outlet', '25', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('27', 'Sigit Indra Wahyudi', 'Samarinda Outlet', '26', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('28', 'Tri Noviani', 'Samarinda Outlet', '27', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('29', 'Sapta Negara', 'Samarinda Outlet', '28', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('30', 'I.G Agung Suwadistana', 'Samarinda Outlet', '29', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('31', 'Pipit Novi', 'Bontang Outlet', '8', '2016-09-24', '2016-09-24', 'Admin');
INSERT INTO `dsorg` VALUES ('32', 'Triana Asmara', 'Bontang Outlet', '31', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('33', 'Teguh Wahyudi', 'Bontang Outlet', '32', '2016-09-24', null, 'Admin');
INSERT INTO `dsorg` VALUES ('34', 'Edwin Pohan', 'Sanggata Outlet', '33', null, null, null);
INSERT INTO `dsorg` VALUES ('35', 'Duki Roli', 'Sanggata Outlet', '34', null, null, null);
INSERT INTO `dsorg` VALUES ('36', 'Salman alfarisi', 'Sanggata Outlet', '35', null, null, null);
INSERT INTO `dsorg` VALUES ('37', 'Rahmat Hidayat', 'Tenggarong Outlet', '8', null, null, null);
INSERT INTO `dsorg` VALUES ('38', 'Rachmad Khirnadi', 'Tenggarong Outlet', '37', null, null, null);
INSERT INTO `dsorg` VALUES ('39', 'Hari Risdiyana', 'Tenggarong Outlet', '38', null, null, null);
INSERT INTO `dsorg` VALUES ('40', 'Wa Ode Adventurera', 'Tenggarong Outlet', '39', null, null, null);
INSERT INTO `dsorg` VALUES ('41', 'Marthin', 'Tenggarong Outlet', '40', null, null, null);
INSERT INTO `dsorg` VALUES ('42', 'Arie Susianto', 'Tenggarong Outlet', '41', null, null, null);
INSERT INTO `dsorg` VALUES ('43', 'Roni', 'Tenggarong Outlet', '42', null, null, null);
INSERT INTO `dsorg` VALUES ('44', 'Lydia Langen', null, '3', null, null, null);
INSERT INTO `dsorg` VALUES ('45', 'Hartanto', null, '44', null, null, null);
INSERT INTO `dsorg` VALUES ('46', 'Kartika Annisa', null, '45', null, null, null);
INSERT INTO `dsorg` VALUES ('47', 'Erwin Dwi Saputra', null, '46', null, null, null);
INSERT INTO `dsorg` VALUES ('48', 'Ariyani', null, '47', null, null, null);
INSERT INTO `dsorg` VALUES ('49', 'Dini Muktisari', null, '48', null, null, null);
INSERT INTO `dsorg` VALUES ('50', 'Rusdi', null, '49', null, null, null);
INSERT INTO `dsorg` VALUES ('51', 'Rahyudi', null, '50', null, null, null);
INSERT INTO `dsorg` VALUES ('52', 'Anton', null, '51', null, null, null);
INSERT INTO `dsorg` VALUES ('53', 'Rofiq', null, '52', null, null, null);
INSERT INTO `dsorg` VALUES ('54', 'Agus Syamsuddin', null, '53', null, null, null);
INSERT INTO `dsorg` VALUES ('55', 'Ferdiyannur', null, '54', null, null, null);
INSERT INTO `dsorg` VALUES ('56', 'Ranty', null, '55', null, null, null);
INSERT INTO `dsorg` VALUES ('57', 'Subhan', null, '56', null, null, null);
INSERT INTO `dsorg` VALUES ('58', 'Sujatmoko', 'Service Advisor', '5', null, null, null);
INSERT INTO `dsorg` VALUES ('59', 'Hermawan Tri Asmoro', 'Service Advisor', '58', null, null, null);
INSERT INTO `dsorg` VALUES ('60', 'Ahmad Arifin', 'Service Advisor', '59', null, null, null);
INSERT INTO `dsorg` VALUES ('61', 'Hendri Heryatno', 'Service Advisor', '60', null, null, null);
INSERT INTO `dsorg` VALUES ('62', 'Muhammad', 'Kepala Regu', '61', null, null, null);
INSERT INTO `dsorg` VALUES ('63', 'Saadduddin Azizi', 'Kepala Regu', '62', null, null, null);
INSERT INTO `dsorg` VALUES ('64', 'Mimhaji Siswanto', 'Mekanik', '5', null, null, null);
INSERT INTO `dsorg` VALUES ('65', 'Santoso', 'Mekanik', '64', null, null, null);
INSERT INTO `dsorg` VALUES ('66', 'Rahmad', 'Mekanik', '65', null, null, null);
INSERT INTO `dsorg` VALUES ('67', 'Rudi', 'Mekanik', '66', null, null, null);
INSERT INTO `dsorg` VALUES ('68', 'Raharjo', 'Mekanik', '67', null, null, null);
INSERT INTO `dsorg` VALUES ('69', 'Umay', 'Mekanik', '68', null, null, null);
INSERT INTO `dsorg` VALUES ('70', 'M. Suyanto', 'Mekanik', '69', null, null, null);
INSERT INTO `dsorg` VALUES ('71', 'Rijal', 'Mekanik', '70', null, null, null);
INSERT INTO `dsorg` VALUES ('72', 'Sobirin', 'Mekanik', '71', null, null, null);
INSERT INTO `dsorg` VALUES ('73', 'Eko', 'Mekanik', '72', null, null, null);
INSERT INTO `dsorg` VALUES ('74', 'Sulis', 'Mekanik', '73', null, null, null);
INSERT INTO `dsorg` VALUES ('75', 'Saifudin', 'Mekanik', '74', null, null, null);
INSERT INTO `dsorg` VALUES ('76', 'Ghazali', 'Mekanik', '75', null, null, null);
INSERT INTO `dsorg` VALUES ('77', 'Mustofa', 'Mekanik', '76', null, null, null);
INSERT INTO `dsorg` VALUES ('78', 'Joko', 'Mekanik', '77', null, null, null);
INSERT INTO `dsorg` VALUES ('79', 'Ali Mujahid', 'Mekanik', '78', null, null, null);
INSERT INTO `dsorg` VALUES ('80', 'Imam', 'Mekanik', '79', null, null, null);
INSERT INTO `dsorg` VALUES ('81', 'Setyono', 'Mekanik', '80', null, null, null);
INSERT INTO `dsorg` VALUES ('82', 'Candra', 'Mekanik', '81', null, null, null);
INSERT INTO `dsorg` VALUES ('83', 'Ahmad', 'SA Body Repair', '5', null, null, null);
INSERT INTO `dsorg` VALUES ('84', 'Ahmad Mujahid', 'SA Body Repair', '83', null, null, null);
INSERT INTO `dsorg` VALUES ('85', 'Sudy Wibowo', 'Body Repair', '5', null, null, null);
INSERT INTO `dsorg` VALUES ('86', 'Djito', 'Body Repair', '85', null, null, null);
INSERT INTO `dsorg` VALUES ('87', 'M. Basrir', 'Body Repair', '86', null, null, null);
INSERT INTO `dsorg` VALUES ('88', 'Budiman', 'Body Repair', '87', null, null, null);
INSERT INTO `dsorg` VALUES ('89', 'Ajis Mulyadi', 'Body Repair', '88', null, null, null);
INSERT INTO `dsorg` VALUES ('90', 'Hendra Setiawan', 'Body Repair', '89', null, null, null);
INSERT INTO `dsorg` VALUES ('91', 'Akhmad Robi\'u', 'Body Repair', '90', null, null, null);
INSERT INTO `dsorg` VALUES ('92', 'Slamet Fatoni', 'Body Repair', '91', null, null, null);
INSERT INTO `dsorg` VALUES ('93', 'Dimaz PP', 'Body Repair', '92', null, null, null);
INSERT INTO `dsorg` VALUES ('94', 'Mardiyanto', 'Body Repair', '93', null, null, null);
INSERT INTO `dsorg` VALUES ('95', 'Dedy Eko Prasetyo', 'Body Repair', '94', null, null, null);
INSERT INTO `dsorg` VALUES ('96', 'Micheal Antomy', 'Body Repair', '95', null, null, null);
INSERT INTO `dsorg` VALUES ('97', 'Khosim', 'Security', '6', null, null, null);
INSERT INTO `dsorg` VALUES ('98', 'Ilham', 'Security', '97', null, null, null);
INSERT INTO `dsorg` VALUES ('99', 'Jeffrey', 'Security', '98', null, null, null);
INSERT INTO `dsorg` VALUES ('100', 'Mualim', 'Security', '99', null, null, null);
INSERT INTO `dsorg` VALUES ('101', 'Ali Setia Budi', 'Security', '100', null, null, null);
INSERT INTO `dsorg` VALUES ('102', 'Hasnul Hadi', 'Security', '101', null, null, null);
INSERT INTO `dsorg` VALUES ('103', 'Solehadi', 'Security', '102', null, null, null);
INSERT INTO `dsorg` VALUES ('104', 'Amirudin', 'Security', '103', null, null, null);
INSERT INTO `dsorg` VALUES ('105', 'Jaino', 'Cleaning Service', '6', null, null, null);
INSERT INTO `dsorg` VALUES ('106', 'Anton', 'Cleaning Service', '105', null, null, null);
INSERT INTO `dsorg` VALUES ('107', 'Benny', 'Cleaning Service', '106', null, null, null);
INSERT INTO `dsorg` VALUES ('108', 'Eko', 'Cleaning Service', '107', null, null, null);
INSERT INTO `dsorg` VALUES ('109', 'Yudi', 'Cleaning Service', '108', null, null, null);
INSERT INTO `dsorg` VALUES ('110', 'Bagus', 'Cleaning Service', '109', null, null, null);
INSERT INTO `dsorg` VALUES ('111', 'Misfu', 'Cleaning Service', '110', null, null, null);
INSERT INTO `dsorg` VALUES ('112', 'Odi', 'Cleaning Service', '111', null, null, null);
INSERT INTO `dsorg` VALUES ('113', 'Ridwan', 'Office Boy', '6', null, null, null);
INSERT INTO `dsorg` VALUES ('114', 'Iin', 'Office Boy', '113', null, null, null);
INSERT INTO `dsorg` VALUES ('115', 'Rizki', 'Office Boy', '114', null, null, null);
INSERT INTO `dsorg` VALUES ('116', 'Amir', 'Car Washer', '115', null, null, null);
INSERT INTO `dsorg` VALUES ('117', 'Wahyudi', 'Car Washer', '116', null, null, null);
INSERT INTO `dsorg` VALUES ('118', 'Juli', 'Car Washer', '117', null, null, null);
INSERT INTO `dsorg` VALUES ('119', 'Latif', 'Car Washer', '118', null, null, null);
INSERT INTO `dsorg` VALUES ('120', 'Erwin', 'Driver', '6', null, null, null);
INSERT INTO `dsorg` VALUES ('121', 'Yudha', 'Driver', '120', null, null, null);

-- ----------------------------
-- Table structure for dvismis
-- ----------------------------
DROP TABLE IF EXISTS `dvismis`;
CREATE TABLE `dvismis` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `vision` text,
  `mision` text,
  `status` enum('on','off') DEFAULT 'off',
  `crtd_at` date DEFAULT NULL,
  `edtd_at` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dvismis
-- ----------------------------
INSERT INTO `dvismis` VALUES ('1', 'Visi Misi', '<p><span style=\"font-size:28px\">Menjadi No. 1 di pasar mobil compact di Indonesia dan sebagai basis produksi global utama untuk Grup Daihatsu/Toyota yang sama dengan standar kualitas pabrik Jepang </span></p>\r\n', '<ol>\r\n	<li><span style=\"font-size:28px\">Kami memproduksi mobil compact bernilai terbaik dan menyediakan layanan terkait yang penting bagi peningkatan nilai stakeholder dan ramah lingkungan</span></li>\r\n	<li><span style=\"font-size:28px\">Kami mengembangkan dan memberikan inspirasi kepada karyawan untuk mencapai kinerja tingkat dunia</span></li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n', 'on', '2016-09-20', '2016-09-22', 'Admin');
INSERT INTO `dvismis` VALUES ('2', 'tes 2', '<p>tes</p>\r\n', '<p>tes</p>\r\n', 'off', '2016-09-20', null, 'Admin');

-- ----------------------------
-- Table structure for m_aturan
-- ----------------------------
DROP TABLE IF EXISTS `m_aturan`;
CREATE TABLE `m_aturan` (
  `m_aturan_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_aturan_name` varchar(100) NOT NULL,
  `m_aturan_desc` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(50) NOT NULL,
  PRIMARY KEY (`m_aturan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_aturan
-- ----------------------------

-- ----------------------------
-- Table structure for m_menu
-- ----------------------------
DROP TABLE IF EXISTS `m_menu`;
CREATE TABLE `m_menu` (
  `m_menu_id` char(3) NOT NULL,
  `m_menu_nama` varchar(255) DEFAULT NULL,
  `m_menu_url` text,
  `m_menu_keterangan` text,
  `m_menu_parent` char(3) DEFAULT NULL,
  `m_menu_level` int(11) DEFAULT NULL,
  `m_menu_icon` varchar(50) DEFAULT NULL,
  `m_menu_status` enum('0','1') DEFAULT NULL,
  `m_menu_position` int(25) DEFAULT NULL,
  `m_menu_created_date` datetime DEFAULT NULL,
  `m_menu_modified_date` datetime DEFAULT NULL,
  `m_menu_created_by` varchar(50) DEFAULT NULL,
  `m_menu_modified_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`m_menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_menu
-- ----------------------------
INSERT INTO `m_menu` VALUES ('004', 'Setting Aplikasi', '#', 'Setting Aplikasi', '0', null, 'fa fa-wrench', null, '1', null, null, null, null);
INSERT INTO `m_menu` VALUES ('005', 'Manajemen Menu', 'pengaturan.menu', 'Manajemen Menu', '004', '2', '', '1', '1', null, '2015-09-22 00:00:00', null, null);
INSERT INTO `m_menu` VALUES ('006', 'Manajemen Akses', 'pengaturan.aturan', 'Manajemen Hak Akses', '004', '2', '', '1', '2', null, '2015-09-22 00:00:00', null, null);
INSERT INTO `m_menu` VALUES ('007', 'Manajemen Pengguna', 'pengaturan.pengguna', 'Manajemen Pengguna', '004', '2', '', '1', '3', null, '2015-09-22 00:00:00', null, null);
INSERT INTO `m_menu` VALUES ('018', 'Home', 'dashboard/dashboard', 'Home', '0', null, 'fa fa-home', null, '0', null, null, null, null);
INSERT INTO `m_menu` VALUES ('019', 'Sejarah', 'backendcompany/sejarah', '', '0', null, 'fa fa-camera', null, '3', null, null, null, null);
INSERT INTO `m_menu` VALUES ('020', 'Visi dan Misi', 'backendcompany/visimisi', '', '0', null, 'fa fa-cogs', null, '4', null, null, null, null);
INSERT INTO `m_menu` VALUES ('021', 'Struktur Organisasi', 'backendcompany/strukturorganisasi', '', '0', null, 'fa fa-tasks', null, '5', null, null, null, null);
INSERT INTO `m_menu` VALUES ('022', 'Kinerja Perusahaan', 'backendcompany/kinerja', '', '0', null, 'fa fa-building', null, '6', null, null, null, null);
INSERT INTO `m_menu` VALUES ('023', 'Service', 'backendcompany/service', '', '0', null, 'fa fa-thumbs-o-up', null, '7', null, null, null, null);
INSERT INTO `m_menu` VALUES ('024', 'Client', 'backendcompany/client', '', '0', null, 'fa fa-user', null, '8', null, null, null, null);
INSERT INTO `m_menu` VALUES ('025', 'About Us', 'backendcompany/aboutus', '', '0', null, 'fa fa-meh-o', null, '9', null, null, null, null);
INSERT INTO `m_menu` VALUES ('026', 'Contact Us', 'backendcompany/contactus', '', '0', null, 'fa fa-phone', null, '10', null, null, null, null);
INSERT INTO `m_menu` VALUES ('027', 'Events & News', '#', '', '0', null, 'fa fa-barcode', null, '2', null, null, null, null);
INSERT INTO `m_menu` VALUES ('028', 'Events', 'backendcompany/events', '', '027', null, '', null, '1', null, null, null, null);
INSERT INTO `m_menu` VALUES ('029', 'News', 'backendcompany/news', '', '027', null, '', null, '2', null, null, null, null);
INSERT INTO `m_menu` VALUES ('030', 'Slide', 'backendcompany/gallery', '', '0', null, 'fa fa-picture-o', null, '2', null, null, null, null);

-- ----------------------------
-- Table structure for m_otoritas_menu
-- ----------------------------
DROP TABLE IF EXISTS `m_otoritas_menu`;
CREATE TABLE `m_otoritas_menu` (
  `m_role_id` int(11) DEFAULT NULL,
  `m_menu_id` char(3) DEFAULT NULL,
  `m_role_view` enum('0','1') DEFAULT '0',
  `m_role_add` enum('0','1') DEFAULT '0',
  `m_role_edit` enum('0','1') DEFAULT '0',
  `m_role_delete` enum('0','1') DEFAULT '0',
  `m_role_export` enum('0','1') DEFAULT '0',
  `m_role_import` enum('0','1') DEFAULT '0',
  KEY `m_role_id` (`m_role_id`),
  KEY `m_menu_id` (`m_menu_id`),
  CONSTRAINT `m_otoritas_menu_ibfk_1` FOREIGN KEY (`m_role_id`) REFERENCES `m_role` (`m_role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_otoritas_menu
-- ----------------------------
INSERT INTO `m_otoritas_menu` VALUES ('18', '018', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('18', '017', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '018', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '004', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '005', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '006', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '007', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '027', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '028', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '029', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '030', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '019', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '020', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '021', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '022', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '023', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '024', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '025', '1', '1', '1', '1', '1', '1');
INSERT INTO `m_otoritas_menu` VALUES ('17', '026', '1', '1', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for m_provinsi
-- ----------------------------
DROP TABLE IF EXISTS `m_provinsi`;
CREATE TABLE `m_provinsi` (
  `m_provinsi_id` varchar(25) NOT NULL,
  `m_provinsi_nama` varchar(35) NOT NULL,
  `m_provinsi_created_date` date DEFAULT NULL,
  `m_provinsi_created_by` varchar(15) DEFAULT NULL,
  `m_provinsi_modified_date` date DEFAULT NULL,
  `m_provinsi_modified_by` varchar(15) DEFAULT NULL,
  `m_provinsi_status` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`m_provinsi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of m_provinsi
-- ----------------------------
INSERT INTO `m_provinsi` VALUES ('1', 'ACEH', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('12920', 'SUMATERA BARAT', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('14086', 'RIAU', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('15885', 'JAMBI', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('17404', 'SUMATERA SELATAN', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('20802', 'BENGKULU', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('22328', 'LAMPUNG', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('24993', 'KEPULAUAN BANGKA BELITUNG', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('25405', 'KEPULAUAN RIAU', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('25823', 'DKI JAKARTA', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('26141', 'JAWA BARAT', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('32676', 'JAWA TENGAH', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('41863', 'DAERAH ISTIMEWA YOGYAKARTA', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('42385', 'JAWA TIMUR', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('51578', 'BANTEN', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('53241', 'BALI', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('54020', 'NUSA TENGGARA BARAT', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('55065', 'NUSA TENGGARA TIMUR', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('58285', 'KALIMANTAN BARAT', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('60371', 'KALIMANTAN TENGAH', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('61965', 'KALIMANTAN SELATAN', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('64111', 'KALIMANTAN TIMUR', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('65702', 'SULAWESI UTARA', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('6728', 'SUMATERA UTARA', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('67393', 'SULAWESI TENGAH', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('69268', 'SULAWESI SELATAN', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('72551', 'SULAWESI TENGGARA', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('74716', 'GORONTALO', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('75425', 'SULAWESI BARAT', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('76096', 'MALUKU', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('77085', 'MALUKU UTARA', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('78203', 'PAPUA', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');
INSERT INTO `m_provinsi` VALUES ('81877', 'PAPUA BARAT', '2015-06-04', 'Administrator', '2015-06-04', 'Administrator', '0');

-- ----------------------------
-- Table structure for m_role
-- ----------------------------
DROP TABLE IF EXISTS `m_role`;
CREATE TABLE `m_role` (
  `m_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_role_nama` varchar(50) DEFAULT NULL,
  `m_role_ket` text,
  `m_role_created_date` datetime DEFAULT NULL,
  `m_role_modified_date` datetime DEFAULT NULL,
  `m_role_created_by` varchar(50) DEFAULT NULL,
  `m_role_modified_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`m_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_role
-- ----------------------------
INSERT INTO `m_role` VALUES ('17', 'Admin', 'Super Admin', '2015-02-05 09:51:21', '2015-02-05 09:51:25', '', '');
INSERT INTO `m_role` VALUES ('18', 'User', 'User', null, null, null, null);

-- ----------------------------
-- Table structure for m_sessions
-- ----------------------------
DROP TABLE IF EXISTS `m_sessions`;
CREATE TABLE `m_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_sessions
-- ----------------------------

-- ----------------------------
-- Table structure for m_user
-- ----------------------------
DROP TABLE IF EXISTS `m_user`;
CREATE TABLE `m_user` (
  `m_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_user_nama` varchar(50) DEFAULT NULL,
  `m_user_username` varchar(50) DEFAULT NULL,
  `m_user_password` varchar(32) DEFAULT NULL,
  `m_user_jk` enum('P','L') DEFAULT NULL,
  `m_user_tgl_lhr` date DEFAULT NULL,
  `m_user_alamat` text,
  `m_user_email` varchar(255) DEFAULT NULL,
  `m_user_tlp` varchar(15) DEFAULT NULL,
  `m_user_wrong_pass` int(11) DEFAULT '0',
  `m_user_ph` varchar(255) NOT NULL,
  `m_user_status` enum('1','0') DEFAULT NULL,
  `m_user_lastlogin` date DEFAULT NULL,
  `m_user_last_password_update` date DEFAULT NULL,
  `m_user_created_date` datetime DEFAULT NULL,
  `m_user_modified_date` datetime DEFAULT NULL,
  `m_user_created_by` varchar(50) DEFAULT NULL,
  `m_user_modified_by` varchar(50) DEFAULT NULL,
  `m_toko_id` int(11) DEFAULT NULL,
  `m_role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`m_user_id`),
  KEY `m_role_id` (`m_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=151207002 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_user
-- ----------------------------
INSERT INTO `m_user` VALUES ('21', 'Luthfi Aziz Nugraha', 'admin', '21232f297a57a5a743894a0e4a801fc3', null, null, null, 'geje@gmail.com', '089928111232', '0', '51599948d10Z2VqZQ==65d2ea03425', '1', null, null, '2015-10-10 00:00:00', null, 'Administrator', null, null, '17');
INSERT INTO `m_user` VALUES ('151207001', 'Akhmad Asad', 'akhmadasad', '57e1e56069fc2229ff308ff3aa1c2d6e', null, null, null, null, null, '0', '', null, null, null, null, null, null, null, null, '18');
SET FOREIGN_KEY_CHECKS=1;
