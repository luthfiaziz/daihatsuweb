<?php

/**
 * Description of generator_helper
 *
 * @author Warman Suganda
 */
class hgenerator {
    
    private static $nama_hari = array('Mon' => 'Senin', 'Tue' => 'Selasa', 'Wed' => 'Rabu', 'Thu' => 'Kamis', 'Fri' => 'Jumat', 'Sat' => 'Sabtu', 'Sun' => 'Minggu');
    public static $color_approve = array('T' => 'green', 'F' => 'grey', 'R' => 'red', 'B' => 'yellow', 'C' => '');
    public static $keterangan_shift_detail_lainnya = array('WKM', 'WKS');
    public static $kondisi_jam_absen = array('M' => 'min', 'BO' => 'min', 'BI' => 'min', 'P' => 'max');
    public static $font_awesome = array('icon-gift', 'icon-glass', 'icon-globe', 'icon-group', 'icon-hdd', 'icon-headphones', 'icon-print', 'icon-folder-open', 'icon-user');
    public static $st_active = array('t' => 'Aktif', 'f' => 'Tidak Aktif');
    public static $kelompok_jabatan = array('S' => 'Struktural', 'F' => 'Fungsional');
    public static $otoritas_data = array('D' => 'Diri Sendiri', 'K' => 'Koordinator', 'S' => 'Semua');
    public static $nama_agama = array('001' => 'Islam', '002' => 'Kristen Katolik', '003' => 'Kristen Protestan', '004' => 'Hindu', '005' => 'Budha', '006' => 'Konghuchu');
    public static $nama_bulan = array('Jan' => 'Januari', 'Feb' => 'Februari', 'Mar' => 'Maret', 'Apr' => 'April', 'May' => 'Mei', 'Jun' => 'Juni', 'Jul' => 'Juli', 'Aug' => 'Agustus', 'Sep' => 'September', 'Oct' => 'Oktober', 'Nov' => 'November', 'Dec' => 'Desember');
    public static $nama_bangsa = array('WNI' => 'WNI', 'WNA' => 'WNA');
    public static $nama_gender = array(2 => 'Laki-laki', 1 => 'Perempuan');
    public static $nama_kawin = array('BK' => 'Belum Kawin', 'K' => 'Kawin', 'J' => 'Janda', 'D' => 'Duda');
    public static $kategori_lembur = array('HK' => 'Hari Kerja', 'HL' => 'Hari Libur/Hari Istirahat Mingguan');
    public static $cuti_default = array(4, 5, 6, 7, 8);
    public static $lulus = array('T' => 'Lulus', 'F' => 'Tidak Lulus');
    
    public static function font_awesome($kode = '', $default = '-- Pilih Icon --', $key = '') {
        $font_list = array();
        $font_source = FCPATH . 'assets/css/icon/icon-list.txt';
        
        if (file_exists($font_source)) {
            $font_content = file_get_contents($font_source);
            $font_list = explode(',', $font_content);
        }

        if (empty($kode)) {
            $data = array();
            foreach ($font_list as $value) {
                $value = trim($value);
                $data[$value] = $value;
            }
            if (!empty($default)) {
                return self::merge_array(array($key => $default), $data);
            } else {
                return $data;
            }
        }
    }

    public static function render_button_group($list = array()) {
        $bg = '';
        foreach ($list as $value) {
            $bg .= '<div class="btn-group">';
            $bg .= $value;
            $bg .= '</div>';
        }
        return $bg;
    }

    public static function switch_tanggal($tanggal, $format = '') {
        if (!empty($tanggal)) {
            switch ($format) {
                case 1:
                    list($a, $b, $c) = explode('-', date('d-M-Y', strtotime($tanggal)));
                    $b = self::$nama_bulan[$b];
                    $date = $a . ' ' . $b . ' ' . $c;
                    break;

                case 2:
                    list($a, $b, $c) = explode('-', $tanggal);
                    $date = $c . '/' . $b . '/' . $a;
                    break;

                default:
                    list($a, $b, $c) = explode('-', $tanggal);
                    $date = $c . '-' . $b . '-' . $a;
                    break;
            }

            return $date;
        } else {
            return '';
        }
    }

    public static function array_to_option($array = array(), $selected = '') {
        $option = '';
        foreach ($array as $key => $value) {
            $sel = '';
            if ($key == $selected)
                $sel = 'selected="selected"';

            $option .= '<option value="' . $key . '" ' . $sel . '>' . $value . '</option>';
        }
        return $option;
    }

    public function merge_array($array1 = array(), $array2 = array()) {
        $array = array();
        foreach ($array1 as $key => $value) {
            $array[$key] = $value;
        }
        foreach ($array2 as $key => $value) {
            $array[$key] = $value;
        }
        return $array;
    }

    public static function rupiah($number) {
        return number_format($number, 2, ',', '.');
    }

    public static function datetime_diff($str_interval = '', $start = '', $end = '') {

        $start = date('Y-m-d H:i:s', strtotime($start));
        $end = date('Y-m-d H:i:s', strtotime($end));
        $d_start = new DateTime($start);
        $d_end = new DateTime($end);
        $diff = $d_start->diff($d_end);

        $year = $diff->format('%y');
        $month = $diff->format('%m');
        $day = $diff->format('%d');
        $hour = $diff->format('%h');
        $min = $diff->format('%i');
        $sec = $diff->format('%s');

        switch ($str_interval) {
            case "y":
                $month = $month > 0 ? round($month / 12, 0) : 0;
                $day = $day > 0 ? round($day / 365, 0) : 0;
                $total = $year + $month / 12 + $day / 365;
                break;
            case "m":
                $day = $day > 0 ? round($day / 30, 0) : 0;
                $hour = $hour > 0 ? round($hour / 24, 0) : 0;
                $total = $year * 12 + $month + $day + $hour;
                break;
            case "d":
                $hour = $hour > 0 ? round($hour / 24, 0) : 0;
                $min = $min > 0 ? round($min / 60, 0) : 0;
                $total = ($year * 365) + ($month * 30) + $day + $hour + $min;
                break;
            case "h":
                $min = $min > 0 ? round($min / 60, 0) : 0;
                $total = ((($year * 365) + ($month * 30) + $day) * 24) + $hour + $min;
                break;
            case "i":
                $sec = $sec > 0 ? round($sec / 60, 0) : 0;
                $total = ((((($year * 365) + ($month * 30) + $day) * 24) + $hour) * 60) + $min + $sec;
                break;
            case "s":
                $total = ((((($year * 365) + ($month * 30) + $day) * 24 + $hour) * 60 + $min) * 60) + $sec;
                break;
        }

        if (strtotime($start) < strtotime($end))
            $total = -1 * $total;

        return $total;
    }

    public static function form_dropdown_image($name = '', $options = array(), $selected = array(), $extra = '') {
        $dropdown_print_options = array();
        $dropdown_print_options_icon = array();

        foreach ($options as $key => $value) {
            $dropdown_print_options[$key] = isset($value['title']) ? $value['title'] : '';
            $dropdown_print_options_icon[$key] = isset($value['icon']) ? $value['icon'] : '';
        }

        return form_dropdown($name, $dropdown_print_options, $selected, $extra, 'icon', $dropdown_print_options_icon);
    }

    public static function minute_to_hours($time, $format) {
        settype($time, 'integer');
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

    public static function switch_number($number) {
        $pecah_titik = str_replace('.', '', $number);
        list($uang, $ekor) = explode(',', $pecah_titik);
        return $uang;
    }

}

/* End of file hgenerator_helper.php */
/* Location: ./application/helpers/hgenerator_helper.php */
