<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Luthfi Aziz Nugraha 
 * Create Date : 26/07/2014 - 00:43:05
 * Module Name : Menu Model ( For Template )
 */

class app_menu_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_tmenu1 = "m_menu";
    private $_tmenu2 = "m_menu_role";
    private $_tmenu3 = "m_grup_user";

    private function _kunci($data = array()){
        $result = $data;

        if(!is_array($data))
            $result = array("m_menu_id" => $data);

        return $result;
    }

    public function data_menu($id = '', $m_role_id = ''){
        $sql = "SELECT
                    *
                FROM
                    (
                        SELECT
                            `a`.*, `child`.`have_child`
                        FROM
                            (`m_menu` a)
                        LEFT OUTER JOIN(
                            SELECT
                                m_menu_parent,
                                COUNT(*)AS have_child
                            FROM
                                `m_menu`
                            GROUP BY
                                m_menu_parent
                        )child ON `a`.m_menu_id = `child`.m_menu_parent
                    )my_menu
                JOIN `m_otoritas_menu` b ON `b`.`m_menu_id` = `my_menu`.`m_menu_id`
                JOIN `m_role` c ON `c`.`m_role_id` = `b`.`m_role_id` 
                WHERE 
                    `my_menu`.`m_menu_parent` = '{$id}' 
                AND
                    `b`.`m_role_id`   = '{$m_role_id}'
                ORDER BY `my_menu`.`m_menu_position` ASC
                ";

        return $this->db->query($sql);
    }

    public function clickcount($menuid = "",$roleid = "", $userid = ""){
        return $this->db->query("UPDATE m_menu_click_count SET m_click_count = m_click_count + 1 WHERE m_menu_id = '{$menuid}' AND m_role_id ='{$roleid}' AND m_user_id = '{$userid}'");
    }

    

}

/* End of file tm_gelar.php */
/* Location: ./application/modules/crud/models/crud.php */