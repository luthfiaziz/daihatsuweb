<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Developed By : Luthfi Aziz Nugraha
 * Module Name  : App Menu ( For Template )
 */

class App_menu extends MX_Controller {
    // VARIABEL SESSION TEMP
    private $SESSION;

    public function __construct() {
        parent::__construct();
        $this->load->model("app_menu_model");

        // GET SESSION DATA
        // $this->SESSION  = $this->session->userdata(APPKEY);
    }

    public function get_top($grup_id = ''){        
        $html   = '<ul class="nav navbar-nav">';      

        $html  .= $this->get_menu_top(0,$grup_id);

        $html  .= '</ul>'; 

        return $html;
    }

	public function get_menu_top($id = '', $grup_id = '') {
            $m_module=$this->app_menu_model->data_menu($id, $grup_id);
            
            $html = "";

            if($m_module->num_rows()>0){
                foreach($m_module->result() as $rs) {
                    if($rs->have_child > 0){
                        // if($this->SESSION["m_otoritas_menu"][$rs->m_menu_id]["m_role_view"] == 1){     
                            $html .= "<li>";

                                $html .= "<a href='javascript:void(0)' class='dropdown-toggle acctive changepage' data-close-others='true' data-hover='dropdown' data-toggle='dropdown' data-page='" . $rs->m_menu_url . "'>";
                                            $html .= $rs->m_menu_nama; 
                                $html .= "&nbsp; <i class='fa fa-angle-down'></i>";
                                $html .= "</a>";  

                                $html .= "<ul class='dropdown-menu'>"; 
                                            $html .= $this->get_menu_top($rs->m_menu_id,$rs->m_role_id);     
                                $html .= "</ul>";         
                            $html .= "</li>";
                        // }
                    }else if($rs->have_child == null || $rs->have_child == "" ){
                        // if($this->SESSION["m_otoritas_menu"][$rs->m_menu_id]["m_role_view"] == 1){
                            $html .= "<li>";    
                                    $html .= "<a href='javascript:void(0)' class='changepage' data-page='" . $rs->m_menu_url . "'>";  
                                                $html .= $rs->m_menu_nama;  
                                    $html .= "</a>";                         
                            $html .= "</li>";
                        // }
                    }else{

                    }
                }		    
            }

            return $html;
	}
}