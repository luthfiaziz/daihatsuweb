<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Developed By : Luthfi Aziz Nugraha
 * Module Name  : Setting Environtment ( For Template )
 */

class App_setting extends MX_Controller {
    private $favicon;
    private $plugin_list = array();
    private $temp_plugin = array();
    private $list_plugin_js = array();
    private $list_plugin_css = array();
    private $list_js = array();
    private $list_css = array();
    private $list_layout = array();

    public function __construct() {
        parent::__construct();
        $this->config();
    }

    private function config() {
        $this->path_asset = base_url() . "assets/";

        /*
         * Daftar Plugin
         * Format untuk mendaftarkan plugin
         * array(
         * 	  'nama_plugin' => array(
         * 			'js' => array(
         * 				'nama_file_js1',
         * 				'nama_file_js2'
         * 			),
         * 			'css' => array(
         * 				'nama_file_css1',
         * 				'nama_file_css2'
         * 			)
         * 	   )
         * )
         */
        $this->plugin_list = array(
            'css-main' =>array(
                'css'=>array(
                    'plugins/bootstrap/css/bootstrap.min',
                    'plugins/font-awesome/css/font-awesome.min',
                    'fonts/style',
                    'css/main',
                    'css/main-responsive',
                    'plugins/iCheck/skins/all',
                    'plugins/bootstrap-colorpalette/css/bootstrap-colorpalette',
                    'plugins/perfect-scrollbar/src/perfect-scrollbar',
                    'css/theme_light',                    
                    'plugins/fullcalendar/fullcalendar/fullcalendar',
                    'plugins/bootstrap-social-buttons/social-buttons-3',
                    'css/easyui/themes/default/linkbutton',
                    'css/easyui/themes/default/pagination',
                    'plugins/bootstrap-modal/css/bootstrap-modal-bs3patch',
                    'plugins/bootstrap-modal/css/bootstrap-modal',
                    'plugins/select2/select2',
                    'css/ui'
                )
            ),
            'js-main' =>array(
                'js'  => array(
                    'plugins/jQuery-lib/2.0.3/jquery.min',
                    'plugins/jquery-ui/jquery-ui-1.10.2.custom.min',
                    'plugins/bootstrap/js/bootstrap.min',
                    'plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min',
                    'plugins/blockUI/jquery.blockUI',
                    'plugins/iCheck/jquery.icheck.min',
                    'plugins/perfect-scrollbar/src/jquery.mousewheel',
                    'plugins/perfect-scrollbar/src/perfect-scrollbar',
                    'plugins/less/less-1.5.0.min',
                    'plugins/jquery-cookie/jquery.cookie',
                    'plugins/bootstrap-colorpalette/js/bootstrap-colorpalette',
                    'js/main',
                    'plugins/bootstrap-fileupload/bootstrap-fileupload.min',
                    'plugins/jquery.pulsate/jquery.pulsate.min',
                    'plugins/bootbox/bootbox.min',
                    'js/pages-user-profile',
                    'js/jquery.easyui.pagination',
                    'plugins/bootstrap-modal/js/bootstrap-modal',
                    'plugins/bootstrap-modal/js/bootstrap-modalmanager',
                    'js/ui-modals',
                    'customjs/global',
                    'plugins/select2/select2.min',
                    'js/jquery.form',
                    'plugins/jquery-validation/dist/jquery.validate.min',
                    'js/login',
                    'js/ckeditor/ckeditor',
                ),
            ),
            'cssjs-front' => array(
                'css' => array(
                            'css/bootstrap/css/bootstrap',
                            'css/bootstrap/css/bootstrap-theme',
                            // 'css/slidejs'
                            'js/unslider/css/unslider',
                            'js/unslider/css/unslider-dots',
                            'js/jsorgchart/css/jquery.orgchart'
                        ),
                'js' => array( 
                            'js/jquery-3.1.0.min',
                            'css/bootstrap/js/bootstrap.min',
                            //'js/jquery.slides'
                            'js/unslider/js/unslider-min',
                            'js/jsorgchart/js/jquery.orgchart'
                        )
                ),

        );

        //Daftarkan Layout Yang Akan Dibuat
        $this->layout = array(
                "layout-style-1" => "layout/footin_layout/index",
                "layout-style-2" => "layout/newfootin/index",
            );
    }

    private function reg_plugin($plugin_name) {
        /*
         * Me'looping daftar plugin untuk mencocokan nama plugin yang dipanggil
         */
        $alpha = 0;
        foreach ($this->plugin_list as $key => $value) {
            if (!in_array($key, $this->temp_plugin)) {
                if ($key == $plugin_name) {
                    /*
                     * Mendaftarkan file javascript jika ada yang disertakan kedalam plugin
                     */
                    if (isset($value['js'])) {
                        $idx = 0;
                        foreach ($value['js'] as $js) {
                            if (!empty($js)) {
                                $index = str_pad($alpha, 4, "0", STR_PAD_LEFT) . str_pad($idx, 4, "0", STR_PAD_LEFT);
                                $this->list_plugin_js[$index] = $js . '.js';
                                $idx++;
                            }
                        }
                    }

                    /*
                     * Mendaftarkan file css jika ada yang disertakan kedalam plugin
                     */
                    if (isset($value['css'])) {
                        $idx = 0;
                        foreach ($value['css'] as $css) {
                            if (!empty($css)) {
                                $index = str_pad($alpha, 4, "0", STR_PAD_LEFT) . str_pad($idx, 4, "0", STR_PAD_LEFT);
                                $this->list_plugin_css[$index] = $css . '.css';
                                $idx++;
                            }
                        }
                    }

                    /*
                     * Jika nama plugin yang dipanggil sama dengan index name daftar plugin proses looping dihentikan
                     */
                    break;
                }
                $alpha++;
            } else {
                /*
                 * Jika nama plugin sudah terdaftar
                 */
                break;
            }
        }
    }

    public function set_plugin($plugin = array()) {
        if (is_array($plugin)) {
            foreach ($plugin as $value) {
                $this->reg_plugin($value);
            }
        } else {
            if ($plugin == 'all') {
                foreach ($this->plugin_list as $key => $value) {
                    $this->reg_plugin($key);
                }
            } else {
                $this->reg_plugin($plugin);
            }
        }
    }

    public function get_js() {
        $js_path = $this->path_asset;
        $list = "";

        $list_plugin = $this->list_plugin_js;
        ksort($list_plugin);
        foreach ($list_plugin as $value) {
            $list .= "<script src='" . $js_path . $value . "'></script>";
        }

        $list_js = $this->list_js;
        ksort($list_js);
        foreach ($list_js as $value) {
            $list .= "<script src='" . $js_path . $value . "'></script>";
        }

        return "<!-- Begin : Javacript -->" . $list . "<!-- End : Javacript -->";
    }

    public function get_css() {
        $css_path = $this->path_asset;
        $list = "";

        $list_plugin = $this->list_plugin_css;
        ksort($list_plugin);
        foreach ($list_plugin as $value) {
            $list .= "<link  rel='stylesheet' href='" . $css_path . $value . "'>";
        }

        $list_css = $this->list_css;
        ksort($list_css);
        foreach ($list_css as $value) {
            $list .= "<link  rel='stylesheet' href='" . $css_path . $value . "'>";
        }

        return "<!-- Begin : CSS -->" . $list . "<!-- End : CSS -->";
    }

    public function set_css($css = array()) {
        if (is_array($css)) {
            foreach ($css as $value) {
                $this->list_css[] = $value . '.css';
            }
        } else {
            $this->list_css[] = $css . '.css';
        }
    }

    public function set_js($js = array()) {
        if (is_array($js)) {
            foreach ($js as $value) {
                $this->list_js[] = $value . '.js';
            }
        } else {
            $this->list_js[] = $js . '.js';
        }
    }

    public function get_layout($index = ''){
        return $this->layout[$index];
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
