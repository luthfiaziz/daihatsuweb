<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Luthfi Aziz Nugraha 
 * Create Date : 26/07/2014 - 00:43:05
 * Module Name : Setting App ( For Template )
 */

class App_core extends MX_Controller {
	
	public function __construct() {
            parent::__construct();

            $this->load->module("auth/app_auth");
            $this->load->module("config/app_setting");
            $this->load->module("crud/app_crud");
	}

	public function core($data = array()){
            $setting = $this->app_setting;

            $setting->set_plugin(array(
                'jquery','jquery-form','jquery-ui','tooltip','dhtmlx',
                'custom', 'fullscreen-jquery','select2-jquery',
                'popup-gallery','jquery-scrollbar','jquery-csv-toarray',
                'highchart','css-form',
                'bootbox-js','mouseover','spectrum','autonumeric','alphanum','ckeditor'
            ));

            $data["js_header" ] = $setting->get_js();
            $data["css_header"] = $setting->get_css();
            $this->load->view("core",$data);
	}

}