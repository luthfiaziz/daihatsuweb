<?php

class main_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getBanner(){
        $sql = "SELECT * FROM dgallery";

        $result = $this->db->query($sql);
        $result = $result->result();

        $result = !empty($result) ? $result : array();

        return $result;
    }

    public function getHotEvent(){
        $sql = "SELECT * FROM devents ORDER BY id DESC LIMIT 6";

        $result = $this->db->query($sql);
        $result = $result->result();

        $result = !empty($result) ? $result : array();

        return $result;
    }

    public function getHotNews(){
        $sql = "SELECT * FROM dnews ORDER BY id DESC LIMIT 6";
        
        $result = $this->db->query($sql);
        $result = $result->result();

        $result = !empty($result) ? $result : array();

        return $result;
    }

}

/* End of file user_model.php */
/* Location: ./application/modules/meeting_management/models/user_model.php */ 