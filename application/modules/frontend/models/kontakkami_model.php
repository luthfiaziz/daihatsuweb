<?php

class kontakkami_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function save_as_new($data) {
        $this->db->trans_begin();
        $save_id = $this->db->set_id("dcontactus", 'id', 'no_prefix', 3);
        $this->db->insert("dcontactus", $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }
}

/* End of file user_model.php */
/* Location: ./application/modules/meeting_management/models/user_model.php */ 