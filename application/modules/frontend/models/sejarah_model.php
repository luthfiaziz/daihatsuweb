<?php

class sejarah_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function record_count(){
        return $this->db->count_all("dsejarah");
    }

    // Fetch data according to per_page limit.
    public function fetch_data($key =array()) {
        $this->db->where($key);
        $query = $this->db->get("dsejarah");

        if ($query->num_rows() > 0) {
            return $query->row();
        }
        return false;
    }
}

/* End of file user_model.php */
/* Location: ./application/modules/meeting_management/models/user_model.php */ 