<?php

class strukturorganisasi_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function record_count(){
        return $this->db->count_all("dsorg");
    }

    // Fetch data according to per_page limit.
    public function fetch_data($key =array()) {
        $this->db->where($key);
        $query = $this->db->get("dsorg");

        if ($query->num_rows() > 0) {
            return $query->row();
        }
        return false;
    }



    //------------------------------------------------------------------------------------------------------------
    // LOAD TREEGRID MENU
    //------------------------------------------------------------------------------------------------------------
    public function data_sorg($id = ''){
        $sql = "SELECT
                    *
                FROM
                    (
                        SELECT
                            `a`.*, `child`.`have_child`
                        FROM
                            (`dsorg` a)
                        LEFT OUTER JOIN (
                            SELECT
                                parent,
                                COUNT(*) AS have_child
                            FROM
                                `dsorg`
                            GROUP BY
                                parent
                        ) child ON `a`.id = `child`.parent
                    ) my_menu
                WHERE
                    `my_menu`.`parent` = '{$id}'
                ORDER BY
                    `my_menu`.`id` ASC
                ";

        return $this->db->query($sql);
    }

    //------------------------------------------------------------------------------------------------------------
    // LOAD TREEGRID JSON FORMAT1
    //------------------------------------------------------------------------------------------------------------
    public function loadSORG($id = ''){
        $result = $this->data_sorg($id); 
        $data   = array();

        if($result->num_rows()>0){
            $i = 0;
            foreach($result->result() as $rs) {
                if($rs->have_child > 0){  
                    $data[$i] = array(
                                "name"=> $rs->name,
                                "title"=>$rs->grade,
                                "children"=>$this->loadSORG($rs->id)
                                ); 
                }else if(empty($rs->have_child)){
                    $data[$i] = array(
                                "name"=> $rs->name,
                                "title"=>$rs->grade
                                ); 
                }else{

                }
            $i++;
            }           
        }

        return $data;
    }
}

/* End of file user_model.php */
/* Location: ./application/modules/meeting_management/models/user_model.php */ 