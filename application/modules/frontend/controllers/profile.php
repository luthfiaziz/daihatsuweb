<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class profile extends MX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->module("config/app_setting");

        $this->load->model("profile_model","profile");
        $this->load->library("pagination");
        $this->load->library("breadcrumbs");
    }

    public function index() {
        $setting = $this->app_setting;
        $setting->set_plugin(array('cssjs-front'));

        $data = array(
                    "header"        => $this->blade->render('frontend/header',array(),true),
                    "js"            => $setting->get_js(),
                    "css"           => $setting->get_css(),
                    "footer"        => $this->blade->render('frontend/footer',array(),true)
                );


        $key    = array("status" => "on");

        $data["results"]    = $this->profile->fetch_data($key);

        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('profile', '/section/page');

        $data["breadcrumbs"] = $this->breadcrumbs->show();

        $this->blade->render('frontend/profile/index',$data);
    }
}

/* End of file aboutus.php */
/* Location: ./application/modules/aboutus/controllers/aboutus.php */
