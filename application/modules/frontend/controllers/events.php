<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class events extends MX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->module("config/app_setting");

        $this->load->model("events_model","events");
        $this->load->library("pagination");
        $this->load->library("breadcrumbs");
    }

    public function index() {
        $setting = $this->app_setting;
        $setting->set_plugin(array('cssjs-front'));

        $data = array(
                    "header"        => $this->blade->render('frontend/header',array(),true),
                    "js"            => $setting->get_js(),
                    "css"           => $setting->get_css(),
                    "footer"        => $this->blade->render('frontend/footer',array(),true)
                );

        // Pagination       
        $config = array();
        $config["base_url"] = base_url() . "events";

        $total_row = $this->events->record_count();

        $config['total_rows'] = $total_row; 
        $config['per_page'] = 6;
        $config['uri_segment'] = 2;
        $config['use_page_numbers'] = FALSE;

        $this->pagination->initialize($config);

        if($this->uri->segment(2)){
            $page = ($this->uri->segment(2));
        }else{
            $page = 0;
        }

        $data["results"]    = $this->events->fetch_data($config["per_page"], $page);
        
        $data["links"]      = $this->pagination->create_links();

        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Events', '/section/page');

        $data["breadcrumbs"] = $this->breadcrumbs->show();

        $this->blade->render('frontend/events/index',$data);
    }

    public function articles(){
        $setting = $this->app_setting;
        $setting->set_plugin(array('cssjs-front'));

        $data = array(
                    "header"        => $this->blade->render('frontend/header',array(),true),
                    "js"            => $setting->get_js(),
                    "css"           => $setting->get_css(),
                    "footer"        => $this->blade->render('frontend/footer',array(),true)
                );


        $id     = ($this->uri->segment(3));
        $key    = array("id" => $id);

        $data["results"]    = $this->events->fetch_data_detail($key);
        
        $data["links"]      = $this->pagination->create_links();

        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('Events', '/events');
        $this->breadcrumbs->push('Articles', '/section/page');

        $data["breadcrumbs"] = $this->breadcrumbs->show();

        $this->blade->render('frontend/events/articles',$data);
    }
}

/* End of file aboutus.php */
/* Location: ./application/modules/aboutus/controllers/aboutus.php */
