<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class main extends MX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->module("config/app_setting");

        $this->load->model("main_model","main");
    }

    public function index() {
        $setting = $this->app_setting;
        $setting->set_plugin(array('cssjs-front'));

        $data = array(
                    "header"        => $this->blade->render('frontend/header',array(),true),
                    "js"            => $setting->get_js(),
                    "css"           => $setting->get_css(),
                    "slide"         => $this->main->getBanner(),
                    "hotevents"     => $this->main->getHotEvent(),
                    "hotnews"       => $this->main->getHotNews(),
                    "footer"        => $this->blade->render('frontend/footer',array(),true)
                );

        $this->blade->render('frontend/main/index',$data);
    }
}

/* End of file aboutus.php */
/* Location: ./application/modules/aboutus/controllers/aboutus.php */
