<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class kontakkami extends MX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->module("config/app_setting");

        $this->load->model("kontakkami_model","kontakkami");
        $this->load->library("pagination");
        $this->load->library("breadcrumbs");
    }

    public function index() {
        $setting = $this->app_setting;
        $setting->set_plugin(array('cssjs-front'));

        $data = array(
                    "header"        => $this->blade->render('frontend/header',array(),true),
                    "js"            => $setting->get_js(),
                    "css"           => $setting->get_css(),
                    "footer"        => $this->blade->render('frontend/footer',array(),true)
                );


        $key    = array("status" => "on");

        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('kontakkami', '/section/page');

        $data["breadcrumbs"] = $this->breadcrumbs->show();
        $data["form_action"] = base_url() . "frontend/kontakkami/simpankontak";

        $this->blade->render('frontend/kontakkami/index',$data);
    }

    function simpankontak(){
        $this->form_validation->set_rules('email', 'Status', 'required|valid_email');

        $string  = "<script type='text/javascript'>";
        if ($this->form_validation->run($this)) {
            $data["name"]       = $this->input->post("name");
            $data["jk"]         = $this->input->post("jk");
            $data["email"]      = $this->input->post("email");
            $data["alamat"]     = $this->input->post("alamat");
            $data["notelp"]     = $this->input->post("telpon");
            $data["pesan"]      = $this->input->post("pesan");
            $data["crtd_at"]    = date("Y-m-d");
        
            if ($this->kontakkami->save_as_new($data)) {
                $string .= "alert('data anda telah kami simpan');";
            }else{
                $string .= "alert('Gangguan Server');";
            }
        } else {
            $string .= "alert('data email kosong / format email salah, silahkan ulangi isian anda');";
        }

        $string .= "window.location.href = '" . base_url() . "kontakkami';";
        $string .= "</script>";

        echo $string;
    }
}

/* End of file aboutus.php */
/* Location: ./application/modules/aboutus/controllers/aboutus.php */
