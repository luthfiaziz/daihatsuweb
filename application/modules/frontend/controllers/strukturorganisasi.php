<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class strukturorganisasi extends MX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->module("config/app_setting");

        $this->load->model("strukturorganisasi_model","strukturorganisasi");
        $this->load->library("pagination");
        $this->load->library("breadcrumbs");
    }

    public function index() {
        $setting = $this->app_setting;
        $setting->set_plugin(array('cssjs-front'));

        $data = array(
                    "header"        => $this->blade->render('frontend/header',array(),true),
                    "js"            => $setting->get_js(),
                    "css"           => $setting->get_css(),
                    "footer"        => $this->blade->render('frontend/footer',array(),true)
                );

        $this->breadcrumbs->push('Home', '/');
        $this->breadcrumbs->push('strukturorganisasi', '/section/page');

        $data["breadcrumbs"]    = $this->breadcrumbs->show();
        $data['data_org_chart'] = $this->strukturorganisasi->loadSORG(0);

        $this->blade->render('frontend/strukturorganisasi/index',$data);
    }

    function loadJSORG(){
       $data   = $this->strukturorganisasi->loadSORG(0);
       
       $result = array("rows" => $data);

       echo json_encode($data);
    }
}

/* End of file aboutus.php */
/* Location: ./application/modules/aboutus/controllers/aboutus.php */
