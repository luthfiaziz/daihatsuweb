<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Daihatsu Web</title>
    {{ $css }}

  </head>
  <body>
    {{ $header }}

    <div class="container-fluid" style="padding-top:2%; background:#CCCCCC; min-height:100px">
      <div class="row">
        <div class="container">
          <div class="col-9">
            &nbsp;
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="container" style="margin-top:10px; ">
          {{ $breadcrumbs }}
        </div>
      </div>
      <div class="row">
          <div class="container">
          <div style="width:100%; padding:20px; min-height:500px; border:1px solid #CCCCCC; margin-bottom:10px; overflow:auto">
            <h1 style="margin:0px; padding:0px;">
               <span style="border-left:7px solid #a02f23;">&nbsp;Sejarah Perusahaan</span>
            </h1>
            <div class="col-sm-12" style="border:0px solid black; margin-top:20px; margin-bottom:50px">
            <?php
              if(!empty($results->desc)){
            ?>
              {{ $results->desc }}
            <?php
              }else{
            ?>
              Konten masih kosong
            <?php 
              }
            ?>
            </div>
          </div>
          </div>
      </div>
    </div>

    {{ $footer }}

    {{ $js }}
  </body>
</html>