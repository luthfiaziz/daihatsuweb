<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Daihatsu Web</title>
    {{ $css }}

  </head>
  <body>

    {{ $header }}

	<div class="container-fluid">
		<div class="row">
			<div class="fading-slider col-9 ss">
				<ul>
				<?php foreach($slide as $sld){ ?>
					    <li><img src="{{ base_url() }}assets/images/Slide/{{ $sld->picture }}" style="height:100%"></li>
				<?php } ?>
				</ul>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-9">
				<center><h2 style="color:#216081">Selamat Datang, Sahabat Daihatsu</h2></center>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<img src="{{ base_url() }}assets/images/Logo/hotevents.png" style="width:100%">
			</div>
		</div>

		<div class="row">
			<?php foreach($hotevents as $he) { ?>
				<div class="col-md-4" style="margin-bottom:3%;">
					<div style="border:1px solid #a44c42; padding-bottom:10px;">
						<div style="height:150px; width:100%; overflow:hidden">
							<img src="{{ base_url() }}assets/images/Events/{{ $he->picture }}" style="width:100%">
						</div>
						<div style="overflow:hidden; border:0px solid black; padding:5px; height:50px;">
							<div class="col-xs-9">
								<span style="color:#943126">{{ date("d M Y",strtotime($he->crtd_at)) }}</span><br/>
								<a href="{{ base_url() }}events/articles/{{ $he->id }}" style="font-size:18px; text-decoration:underline; color:#575757">{{ $he->name }}</a>
							</div>
							<div class="col-xs-3" style="text-align:center">
								<div style="display:block; border-radius:5px; background:#cccccc; padding-left:3px; padding-right:3px; padding-top:2px">
									<a href="{{ base_url() }}events/articles/{{ $he->id }}" style="font-size:15px; text-decoration:none; color:#575757;">
									<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <br/>lihat</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
		<div class="row">
			<div class="col-md-5">
				<img src="{{ base_url() }}assets/images/Logo/hotnews.png" style="width:100%">
			</div>
		</div>
		<div class="row">
			<?php foreach($hotnews as $hn) { ?>
				<div class="col-md-4" style="margin-bottom:3%;">
					<div style="border:1px solid #a44c42; padding-bottom:10px;">
						<div style="height:150px; width:100%; overflow:hidden">
							<img src="{{ base_url() }}assets/images/News/{{ $hn->picture }}" style="width:100%">
						</div>
						<div style="overflow:hidden; border:0px solid black; padding:5px; height:50px;">
							<div class="col-xs-9">
								<span style="color:#943126">{{ date("d M Y",strtotime($hn->crtd_at)) }}</span><br/>
								<a href="#" style="font-size:18px; text-decoration:underline; color:#575757">{{ $hn->name }}</a>
							</div>
							<div class="col-xs-3" style="text-align:center">
								<div style="display:block; border-radius:5px; background:#cccccc; padding-left:3px; padding-right:3px; padding-top:2px">
									<a href="#" style="font-size:15px; text-decoration:none; color:#575757;">
									<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <br/>lihat</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>

    {{ $footer }}

    {{ $js }}
    <script type="text/javascript">
		// $(function(){
		// 	$("#slides").slidesjs({
		// 		width: 940,
		// 		height: 528
		// 	});
		// });

		$('.fading-slider').unslider({
			animation: 'fade',
			autoplay: true,
			arrows:false
		});
    </script>
  </body>
</html>