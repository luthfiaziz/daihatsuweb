<div class="container-fluid" style="background:#f2f2f2">
<div class="row">
	<div class="col-9">
		<div class="container" style="padding: 50px 50px 0px 50px">
			<div class="row" style="border-top:0px solid #ffffff; border-bottom:0px solid #888888; padding:10px; margin-bottom:30px">
				<div class="col-xs-12 col-sm-6 col-md-8">
					<p style="font-size:22px; color:#888888; margin-top:10px">MAU HADIAH GRATIS Tiap minggu ? Ikuti akun Social Media kami, </p>
				</div>
					<div class="col-xs-6 col-md-4">
					<img src="{{ base_url() }}assets/images/Icons/facebook.png" style="height:50px" class="pull-right">
					<img src="{{ base_url() }}assets/images/Icons/youtube.png" style="height:50px" class="pull-right">
					<img src="{{ base_url() }}assets/images/Icons/twitter.png" style="height:50px" class="pull-right">
					</div>
			</div>
			<div class="row">
				<div class="col-md-3" style="border-left:1px solid #7c7c7c; height:150px; margin-bottom:10px">
					<p>Informasi Perusahaan</p>
					<p>Sejarah</p>
					<p>Visi & Misi</p>
					<p>Kinerja</p>
				</div>
				<div class="col-md-3" style="border-left:1px solid #7c7c7c; height:150px; margin-bottom:10px">
					<p>Struktur Organisasi</p>
					<p>Event & News</p>
					<p>Service</p>
					<p>Client</p>
				</div>
				<div class="col-md-3" style="border-left:1px solid #7c7c7c; height:150px; margin-bottom:10px">
					<p>Contact Us</p>
					<p>Tentang Kami</p>
				</div>
				<div class="col-md-3" style="margin-bottom:10px">
					<div class="pull-right">
						<p style="font-size:24px; text-align:right">
							PT ASTRA International Tbk - Daihatsu Cabang Samarinda
						</p><br/>
						<p style="font-size:15px; text-align:right">
							JL. IR. H. JUANDA NO. 88
							KALIMANTAN TIMUR, INDONESIA
							TELP : +62 813 4709 4713
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="container-fluid" style="padding-top: 10px; padding-bottom:10px; background:#666666">
<div class="row">
	<div class="col-9" style="color:#AAAAAA">
		<div class="container">
					&copy; 2016 PT. Astra Daihatsu Motor 
		</div>
	</div>
</div>
</div>