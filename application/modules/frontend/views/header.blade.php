<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><img src="{{ base_url() }}assets/images/logo/front.png" height="30px" style="position:relative; top:-6px;"></a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="/"><b>Home</b></a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						<b>Events & News</b> <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="{{ base_url() }}events">Events</a></li>
        				<li role="separator" class="divider"></li>
						<li><a href="{{ base_url() }}news">News</a></li>
					</ul>
				</li>
				<li><a href="{{ base_url() }}visionmision"><b>Visi & Misi</b></a></li>
				<li><a href="{{ base_url() }}kinerja"><b>Kinerja</b></a></li>
				<li><a href="{{ base_url() }}struktur"><b>Struktur Organisasi</b></a></li>
				<li><a href="{{ base_url() }}client"><b>Client</b></a></li>
				<li><a href="{{ base_url() }}service"><b>Service</a></b></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						<b>Tentang Kami</b> <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="{{ base_url() }}profile">Profile</a></li>
        				<li role="separator" class="divider"></li>
						<li><a href="{{ base_url() }}sejarah">Sejarah</a></li>
        				<li role="separator" class="divider"></li>
						<li><a href="{{ base_url() }}kontakkami">Kontak Kami</a></li>
					</ul>
				</li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>