<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Daihatsu Web</title>
    {{ $css }}

  </head>
  <body>
    {{ $header }}

    <div class="container-fluid" style="padding-top:2%; background:#CCCCCC; min-height:100px">
      <div class="row">
        <div class="container">
          <div class="col-9">
            &nbsp;
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="container" style="margin-top:10px; ">
          {{ $breadcrumbs }}
        </div>
      </div>
      <div class="row">
          <div class="container">
          <div style="width:100%; padding:20px; min-height:500px; border:1px solid #CCCCCC; margin-bottom:10px; overflow:auto">
            <h1 style="margin:0px; padding:0px;">
               <span style="border-left:7px solid #a02f23;">&nbsp;Kontak Kami</span>
            </h1>
            <div class="col-sm-12" style="border:0px solid black; margin-top:20px; margin-bottom:50px">
              <div class="col-sm-3">
                  <?php
                    $hidden_form = array('id' => !empty($id) ? $id : '');
                  ?>

                  {{ form_open_multipart($form_action, array('id' => 'fkontak', 'class' => 'form-horizontal'), $hidden_form) }}
                    <div class="form-group">
                      <label for="Name">Nama</label>
                      <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="form-group">
                      <label for="email">Jenis Kelamin</label>
                      <select class="form-control" id="jk" name="jk">
                        <option value="l">Laki-laki</option>
                        <option value="p">Perempuan</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" class="form-control" id="email" name="email">
                    </div>
                    <div class="form-group">
                      <label for="alamat">Alamat</label>
                      <input type="text" class="form-control" id="alamat" name="alamat">
                    </div>
                    <div class="form-group">
                      <label for="telpon">Telpon</label>
                      <input type="text" class="form-control" id="telpon" name="telpon">
                    </div>
                    <div class="form-group">
                      <label for="pesan">Pesan</label>
                      <textarea type="tet" class="form-control" id="pesan" name="pesan"></textarea>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                  {{ form_close(); }}
              </div>
              <div class="col-sm-3">
                &nbsp;
              </div>
              <div class="col-sm-6">
                <div class="panel panel-default" class="pull-right">
                  <div class="panel-body">
                    <div>
                      <p style="font-size:24px; text-align:right">
                        PT ASTRA International Tbk - Daihatsu Cabang Samarinda
                      </p><br/>
                      <p style="font-size:15px; text-align:right">
                        JL. IR. H. JUANDA NO. 88
                        KALIMANTAN TIMUR, INDONESIA
                        TELP : +62 813 4709 4713
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
      </div>
    </div>

    {{ $footer }}

    {{ $js }}
  </body>
</html>