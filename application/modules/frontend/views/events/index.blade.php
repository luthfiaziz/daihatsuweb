<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Daihatsu Web</title>
    {{ $css }}

  </head>
  <body>
    {{ $header }}

  <div class="container-fluid" style="padding-top:2%; background:#CCCCCC">
    <div class="row">
      <div class="container">
        <div class="col-9">
          <img src="{{ base_url() }}assets/images/Logo/events.png" height="150px">
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid" style="margin-top:2%">
    <div  class="container" style="min-height:500px;">
      <div class="row">
        <div class="container" style="margin-top:10px; ">
          {{ $breadcrumbs }}
        </div>
      </div>
      
      <div class="row">
        <?php foreach($results as $hn) { ?>
          <div class="col-md-4" style="margin-bottom:3%;">
            <div style="border:1px solid #a44c42; padding-bottom:10px;">
              <div style="height:150px; width:100%; overflow:hidden">
                <img src="{{ base_url() }}assets/images/Events/{{ $hn->picture }}" style="width:100%">
              </div>
              <div style="overflow:hidden; border:0px solid black; padding:5px; height:200px;">
                <div class="row" style="padding-left:10px; padding-right:10px">
                  <div class="col-xs-10">
                    <span style="color:#943126">{{ date("d M Y",strtotime($hn->crtd_at)) }}</span><br/>
                    <a href="{{ base_url() }}events/articles/{{ $hn->id }}" style="font-size:18px; text-decoration:underline; color:#575757">{{ $hn->name }}</a>
                  </div>
                  <div class="col-xs-2" style="text-align:center">
                    <div style="display:block; border-radius:5px; background:#cccccc; padding-left:3px; padding-right:3px; padding-top:2px">
                      <a href="{{ base_url() }}events/articles/{{ $hn->id }}" style="font-size:15px; text-decoration:none; color:#575757;">
                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> <br/>lihat
                      </a>
                    </div>
                  </div>
                </div>
                <div class="row" style="padding-left:10px; padding-right:10px">
                  <div class="col-sm-12" style="text-align:justify; border:0px solid black; padding-top:5px">
                    {{ $hn->vdesc }}
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
      
      {{ $links }}
    </div> 
  </div>
    {{ $footer }}

    {{ $js }}
  </body>
</html>