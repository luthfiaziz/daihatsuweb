<?php

class contactus_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_table1 = "dcontactus";

    private function _key($key) { //unit ID
        if (!is_array($key)) {
            $key = array('id' => $key);
        }
        return $key;
    }

    public function data($key = '') {
        $this->db->from($this->_table1);

        if (!empty($key) || is_array($key))
            $this->db->where_condition($this->_key($key));

        return $this->db;
    }

    public function save_as_new($data) {
        $this->db->trans_begin();
        $save_id = $this->db->set_id($this->_table1, 'id', 'no_prefix', 3);
        $this->db->insert($this->_table1, $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function save($data, $key) {
        $this->db->trans_begin();
        $this->db->update($this->_table1, $data, $this->_key($key));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function updateall(){
        $sql = "UPDATE " . $this->_table1 . " SET status='off'";

        return $this->db->query($sql);
    }

    public function delete($key) {
        $this->db->trans_begin();
        $this->db->delete($this->_table1, $this->_key($key));
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function data_table($module = '', $limit = 20, $offset = 1) {
        $filter = array();
        
        $kata_kunci = $this->input->post('kata_kunci');
        if (!empty($kata_kunci))
            $filter[$this->_table1 . ".name LIKE '%{$kata_kunci}%' "] = NULL;

        $total = $this->data($filter)->count_all_results();
        $this->db->limit($limit, ($offset * $limit) - $limit);
        $this->db->order_by("crtd_at","DESC");
        $record = $this->data($filter)->get();

        $rows = array();
        foreach ($record->result() as $row) {
            $id = $row->id;

            $aksi           = "<div class='btn btn-xs btn-primary' id='button-edit-{$id}' onclick='load_form(this.id)' data-source='" . base_url() . "$module/edit/$id'><i class='fa fa-eye fa-lg'></i></div>";

            $rows[$id] = array(
                'name' => $row->name,
                'jk'   => ($row->jk == "l") ? "Laki-laki" : "Perempuan",
                'notelp' => $row->notelp,
                'email' => $row->email,
                'alamat' => $row->alamat,
                'crtd_at' => !empty($row->crtd_at) ? date('d-m-Y', strtotime($row->crtd_at)) : '',
                'aksi' => $aksi
            );
        }

        return array('total' => $total, 'rows' => $rows);
    }

    public function options($default = '--Pilih Otoritas Data--') {
        $option = array();
        $list = $this->data()->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->id] = $row->name;
        }

        return $option;
    }

}

/* End of file contactus_model.php */
/* Location: ./application/modules/unit/models/contactus_model.php */