<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo (isset($page_title)) ? $page_title : 'events Management'; ?>
    </div>
    <div class="panel-body">
        <?php
        $hidden_form = array('id' => !empty($id) ? $id : '');
        ?>

        {{ form_open_multipart($form_action, array('id' => 'fevents', 'class' => 'form-horizontal', 'data-ckeditor' => 'true'), $hidden_form) }}
        
        <div class="control-group">
            <label for="events_name" class="control-label"><b>Content Title <span class="symbol required"></span></b></label>
            <div class="controls">
                {{ form_input('name', !empty($default->name) ? $default->name : '', 'class="form-control"'); }}
            </div>
            <br>

            <label for="events_name" class="control-label"><b><span class="title_app">Gambar Cover</span></b></label>
            <div class="controls uibutton">
                <?php 
                    $img = !empty($default->picture) ? $default->picture :'noimage.png'; 
                    $url_image = base_url() . "assets/images/events/" . $img;
                ?>
                <img id="uploadPreview2" src="<?php echo $url_image; ?>" class="upload" height="200px"/><br/> 
                {{ form_hidden('picture', !empty($default->picture) ? $default->picture : '', 'class="form-control"'); }} 
                <label class="myuibutton">
                    <span>Upload Gambar</span>
                    <input id="uploadImage2" type="file" name="images[]" onchange="PreviewImage(2);"/>
                </label>
            </div>

            <label for="events_vketerangan" class="control-label"><b>Deskripsi Tampilan<span class="symbol required"></span></b></label>
            <div class="controls">
                {{ form_textarea('vdesc', !empty($default->vdesc) ? $default->vdesc : '', 'class="form-control"'); }}
            </div>
            <br/>

            <label for="events_keterangan" class="control-label"><b>Deskripsi Detail<span class="symbol required"></span></b></label>
            <div class="controls">
                {{ form_textarea('desc', !empty($default->desc) ? $default->desc : '', 'class="form-control"'); }}
            </div>
            <br/>

            <div class="control-group">
                <label for="events_leterangan" class="control-label"><b>Status <span class="symbol required"></span></b> : </label>
                <div class="controls">
                    {{ form_dropdown('status', array("on" => "Aktif", "off" => "Tidak Aktif"), !empty($default->status) ? $default->status : '', 'class="span8" id="status" style="width:100% !important; height:300px !important!"'); }}
                </div>
            </div>
        </div><br>
        <div class="well-content" id="content_table">
        </div>

        <div class="form-actions">
            <div id="button-save" class="btn btn-default"  onclick="simpan_data(this.id, '#fevents', '#button-back')"><i class="icon-save"></i> Simpan</div>
            <div id="button-back" class="btn btn-bricky" onclick="close_form(this.id)"><i class="icon-circle-arrow-left"></i> Tutup</div>
        </div>
        {{ form_close(); }}
    </div>
</div>
<script type="text/javascript">
    CKEDITOR.replace('desc', settUpload);
    
    $(function() {
        select2_icon('status');
        select2_icon('sico');
    });
</script>
