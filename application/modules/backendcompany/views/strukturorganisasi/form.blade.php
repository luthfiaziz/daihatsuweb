<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo (isset($page_title)) ? $page_title : 'strukturorganisasi Management'; ?>
    </div>
    <div class="panel-body">
        <?php
        $hidden_form = array('id' => !empty($id) ? $id : '');
        ?>

        {{ form_open_multipart($form_action, array('id' => 'fstrukturorganisasi', 'class' => 'form-horizontal', 'data-ckeditor' => 'true'), $hidden_form) }}
        
        <div class="control-group">
            <label for="strukturorganisasi_grade" class="control-label"><b>Jabatan <span class="symbol required"></span></b></label>
            <div class="controls">
                {{ form_input('grade', !empty($default->grade) ? $default->grade : '', 'class="form-control"'); }}
            </div>
            <br>

            <label for="strukturorganisasi_name" class="control-label"><b>Pemegang Jabatan</b></label>
            <div class="controls">
                {{ form_input('name', !empty($default->name) ? $default->name : '', 'class="form-control"'); }}
            </div>
            <br>

            <div class="control-group">
                <label for="strukturorganisasi_leterangan" class="control-label"><b>Induk Jabatan <span class="symbol required"></span></b> : </label>
                <div class="controls">
                    {{ form_dropdown('parent', $strukturOPT, !empty($default->parent) ? $default->parent : '', 'class="span8" id="parent" style="width:100% !important; height:300px !important!"'); }}
                </div>
            </div>
            <br>
        </div><br>
        <div class="well-content" id="content_table">
        </div>

        <div class="form-actions">
            <div id="button-save" class="btn btn-default"  onclick="simpan_data(this.id, '#fstrukturorganisasi', '#button-back')"><i class="icon-save"></i> Simpan</div>
            <div id="button-back" class="btn btn-bricky" onclick="close_form(this.id)"><i class="icon-circle-arrow-left"></i> Tutup</div>
        </div>
        {{ form_close(); }}
    </div>
</div>
<script type="text/javascript">
    $(function() {
        select2_icon('parent');
        select2_icon('status');
        select2_icon('sico');
    });
</script>
