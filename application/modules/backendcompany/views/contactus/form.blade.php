<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo (isset($page_title)) ? $page_title : 'contactus Management'; ?>
    </div>
    <div class="panel-body">
        <?php
        $hidden_form = array('id' => !empty($id) ? $id : '');
        ?>

        {{ form_open_multipart($form_action, array('id' => 'fcontactus', 'class' => 'form-horizontal', 'data-ckeditor' => 'true'), $hidden_form) }}
        
        <div class="control-group">
            <div class="control-group">
                <label for="contactus_leterangan" class="control-label"><b>Nama</b></label>
                <div class="controls">
                    {{ !empty($default->name) ? $default->name : '' }}
                </div>
            </div>

            <div class="control-group">
                <label for="contactus_leterangan" class="control-label"><b>Jenis Kelamin</b></label>
                <div class="controls">
                    {{ !empty($default->jk) ? (($default->jk == 'l')? 'Laki-laki' : 'Perempuan') : '' }}
                </div>
            </div>

            <div class="control-group">
                <label for="contactus_leterangan" class="control-label"><b>No Telpon</b></label>
                <div class="controls">
                    {{ !empty($default->notelp) ? $default->notelp : '' }}
                </div>
            </div>

            <div class="control-group">
                <label for="contactus_leterangan" class="control-label"><b>Email</b></label>
                <div class="controls">
                    {{ !empty($default->email) ? $default->email : '' }}
                </div>
            </div>
          
            <div class="control-group">
                <label for="contactus_leterangan" class="control-label"><b>Pesan</b></label>
                <div class="controls">
                    {{ !empty($default->pesan) ? $default->pesan : '' }}
                </div>
            </div>
        </div><br>
        <div class="form-actions">
            <div id="button-back" class="btn btn-bricky" onclick="close_form(this.id)"><i class="icon-circle-arrow-left"></i> Tutup</div>
        </div>
        {{ form_close(); }}
    </div>
</div>
<script type="text/javascript">
    $(function() {
        select2_icon('status');
        select2_icon('sico');
    });
</script>
