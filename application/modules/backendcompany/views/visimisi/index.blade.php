<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <div class="page-header">
            <h1 class="head-right">Page | <small>Form visimisi</small></h1>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div id="table_menu">
                        <!-- end: PAGE HEADER -->
                    <!-- start: PAGE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- start: DYNAMIC TABLE PANEL -->
                            <div id="index-content" class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i>
                                    DATA :: List Visi Misi
                                    <div class="panel-tools">
                                        <a class="btn btn-xs btn-link panel-refresh" href="#"> <i class="fa fa-refresh"></i> </a>
                                    </div>
                                </div>
                                <div class="panel-body">

                                    <div class="span12" style="margin-bottom:10px">
                                            <div class="btn btn-primary" id="button-add" onclick="load_form(this.id)" data-source="{{ base_url() }}backendcompany/visimisi/add"><i class="fa fa-plus"></i> Tambah Data</div>
                                    </div>

                                    <div id="grid_visimisi" data-source="{{ $data_sources; }}" data-filter="#ffilter" pagination="true"></div>
                                </div>
                            </div>
                            <!-- end: DYNAMIC TABLE PANEL -->
                            <div id="form-content" class="well-content"></div>
                        </div>
                    </div>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<script type="text/javascript">
    jQuery(function($) {

        load_table('#grid_visimisi', 1, '#ffilter');
        
        $('.onchange').change(function(){
            load_table('#grid_visimisi', 1);
        });

        $('#button-filter').click(function() {
            load_table('#grid_visimisi', 1, '#ffilter');
        });
    });
</script>