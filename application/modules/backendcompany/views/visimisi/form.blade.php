<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo (isset($page_title)) ? $page_title : 'visimisi Management'; ?>
    </div>
    <div class="panel-body">
        <?php
        $hidden_form = array('id' => !empty($id) ? $id : '');
        ?>

        {{ form_open_multipart($form_action, array('id' => 'fvisimisi', 'class' => 'form-horizontal', 'data-ckeditor' => 'true'), $hidden_form) }}
        
        <div class="control-group">
            <label for="visimisi_name" class="control-label"><b>Content Title <span class="symbol required"></span></b></label>
            <div class="controls">
                {{ form_input('name', !empty($default->name) ? $default->name : '', 'class="form-control"'); }}
            </div>
            <br>

            <label for="visimisi_keterangan" class="control-label"><b>Visi <span class="symbol required"></span></b></label>
            <div class="controls">
                {{ form_textarea('vision', !empty($default->vision) ? $default->vision : '', 'class="form-control"'); }}
            </div>
            <br/>

          
            <label for="visimisi_keterangan" class="control-label"><b>Misi <span class="symbol required"></span></b></label>
            <div class="controls">
                {{ form_textarea('mision', !empty($default->mision) ? $default->mision : '', 'class="form-control"'); }}
            </div>
            <br/>

            <div class="control-group">
                <label for="visimisi_leterangan" class="control-label"><b>Status <span class="symbol required"></span></b> : </label>
                <div class="controls">
                    {{ form_dropdown('status', array("on" => "Aktif", "off" => "Tidak Aktif"), !empty($default->status) ? $default->status : '', 'class="span8" id="status" style="width:100% !important; height:300px !important!"'); }}
                </div>
            </div>
        </div><br>
        <div class="well-content" id="content_table">
        </div>

        <div class="form-actions">
            <div id="button-save" class="btn btn-default"  onclick="simpan_data(this.id, '#fvisimisi', '#button-back')"><i class="icon-save"></i> Simpan</div>
            <div id="button-back" class="btn btn-bricky" onclick="close_form(this.id)"><i class="icon-circle-arrow-left"></i> Tutup</div>
        </div>
        {{ form_close(); }}
    </div>
</div>
<script type="text/javascript">
    CKEDITOR.replace('vision', { toolbar : basic , height: 160});
    CKEDITOR.replace('mision', { toolbar : basic , height: 160});

    $(function() {
        select2_icon('status');
        select2_icon('sico');
    });
</script>
