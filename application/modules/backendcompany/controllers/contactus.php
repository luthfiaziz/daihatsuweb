<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class contactus extends MX_Controller {

    private $_title = 'Role Management';
    private $_limit = 5;
    private $_module = 'backendcompany/contactus';
    private $_SESSION = array();

    public function __construct() {
        parent::__construct();

        /* Load Global Model */
        $this->load->model('contactus_model');
        $this->_SESSION = $this->session->userdata(APPKEY);
    }

    public function index() {
        // Load Modules
        $this->load->module("template/asset");

        $data['button_group'] = array(
            anchor(null, '<i class="icon-plus"></i> Tambah Data', array('class' => 'btn yellow', 'id' => 'button-add', 'onclick' => 'load_form(this.id)', 'data-source' => base_url($this->_module . '/add')))
        );
        $data['page_title']     = '<i class="icon-laptop"></i> ' . $this->_title;
        $data['page_content']   = $this->_module . '/main';
        $data['data_sources']   = base_url($this->_module . '/load');


        $view = $this->blade->render('backendcompany/contactus/index',$data,true);
        
        echo $view;
    }

    public function add($id = '') {
        $page_title = 'Tambah Data';
        $data['id'] = $id;
        $otoritas = array();
        if ($id != '') {
            $page_title         = 'Edit Data';
            $contactus             = $this->contactus_model->data($id);
            $data['default']    = $contactus->get()->row();
        }
        
        $data['page_title']     = '<i class="icon-laptop"></i> ' . $page_title;
        $data['form_action']    = base_url($this->_module . '/proses');

        $this->blade->render($this->_module . '/form',$data);
    }

    public function edit($id) {
        $this->add($id);
    }

    public function load($page = 1) {
        $data_table = $this->contactus_model->data_table($this->_module, $this->_limit, $page);
        $this->load->library("ltable");
        $table = new stdClass();
        $table->id = 'contactus_id';
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->align = array('name' => 'left', 'jk' => 'left', 'notelp' => 'left', 'email' => 'left', 'alamat' => 'left', 'crtd_at' => 'center' ,'aksi' => 'center');
        $table->width_colom = array(
                                'name' => '30%', 
                                'jk' => '10%' , 
                                'notelp' => '10%' , 
                                'email' => '10%' , 
                                'alamat' => '10%' , 
                                'crtd_at' => '10%' , 
                                'aksi' => '10%'
                            );
        $table->page = $page;
        $table->limit = $this->_limit;
        $table->jumlah_kolom = 6;
        $table->header[] = array(
            "Nama", 1, 1,
            "Jenis Kelamin", 1, 1,
            "Telpon", 1, 1,
            "Email", 1, 1,
            "Alamat", 1, 1,
            "Created At", 1, 1,
            "Aksi", 1, 1
        );

        $table->total = $data_table['total'];
        $table->content = $data_table['rows'];
        $data = $this->ltable->generate($table, 'js', true);
        echo $data;
    }

    public function proses() {
        $this->form_validation->set_rules('name', 'contactus', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('desc', 'Deskripsi', 'trim|max_length[250]');
        $this->form_validation->set_rules('status', 'Status', 'trim|max_length[250]');

        if ($this->form_validation->run($this)) {
            $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');
            $id = $this->input->post('id');
            $data = array();

            $data["name"]       = $this->input->post('name');
            $data["desc"]       = $this->input->post('desc');
            $data["status"]     = $this->input->post('status');
            $data["author"]     = $this->_SESSION["m_role_nama"];
 
            if ($id == '') {
                $this->contactus_model->updateall();

                $data["crtd_at"] = date("Y-m-d");
                if ($this->contactus_model->save_as_new($data)) {
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', '#grid_contactus');
                }
            } else {
                $this->contactus_model->updateall();

                $data["edtd_at"] = date("Y-m-d");
                if ($this->contactus_model->save($data, $id)) {
                    $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', '#grid_contactus');
                }
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors(), '');
        }
        echo json_encode($message, true);
    }

    public function delete($id) {
        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        if ($this->contactus_model->delete($id)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '#grid_contactus');
        }
        echo json_encode($message);
    }

}

/* End of file contactus.php */
/* Location: ./application/modules/contactus/controllers/contactus.php */
