<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class events extends MX_Controller {

    private $_title = 'Role Management';
    private $_limit = 5;
    private $_module = 'backendcompany/events';
    private $_SESSION = array();
    // VARIABEL ATTACHMENT PATH
    private $_attachment_path;

    public function __construct() {
        parent::__construct();

        // SET PATH IMAGE LOCATION
        $this->_attachment_path = FCPATH .'assets/images/events/';

        /* Load Global Model */
        $this->load->model('events_model');
        $this->_SESSION = $this->session->userdata(APPKEY);
    } 

    public function index() {
        // Load Modules
        $this->load->module("template/asset");

        $data['button_group'] = array(
            anchor(null, '<i class="icon-plus"></i> Tambah Data', array('class' => 'btn yellow', 'id' => 'button-add', 'onclick' => 'load_form(this.id)', 'data-source' => base_url($this->_module . '/add')))
        );
        $data['page_title']     = '<i class="icon-laptop"></i> ' . $this->_title;
        $data['page_content']   = $this->_module . '/main';
        $data['data_sources']   = base_url($this->_module . '/load');


        $view = $this->blade->render('backendcompany/events/index',$data,true);
        
        echo $view;
    }

    public function add($id = '') {
        $page_title = 'Tambah Data';
        $data['id'] = $id;
        $otoritas = array();
        if ($id != '') {
            $page_title         = 'Edit Data';
            $events             = $this->events_model->data($id);
            $data['default']    = $events->get()->row();
        }
        
        $data['page_title']     = '<i class="icon-laptop"></i> ' . $page_title;
        $data['form_action']    = base_url($this->_module . '/proses');

        $this->blade->render($this->_module . '/form',$data);
    }

    public function edit($id) {
        $this->add($id);
    }

    public function load($page = 1) {
        $data_table = $this->events_model->data_table($this->_module, $this->_limit, $page);
        $this->load->library("ltable");
        $table = new stdClass();
        $table->id = 'events_id';
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->align = array('name' => 'left', 'image' => 'center', 'status' => 'left', 'crtd_at' => 'center' , 'edtd_at' => 'center' , 'author' => 'center' ,'aksi' => 'center');
        $table->width_colom = array(
                                'name' => '20%', 
                                'image' => '20%',
                                'status' => '10%', 
                                'crtd_at' => '10%' , 
                                'edtd_at' => '10%' , 
                                'author' => '10%' ,
                                'aksi' => '10%'
                            );
        $table->page = $page;
        $table->limit = $this->_limit;
        $table->jumlah_kolom = 6;
        $table->header[] = array(
            "Content Title", 1, 1,
            "Cover",1,1,
            "Status", 1, 1,
            "Created At", 1, 1,
            "Edited At", 1, 1,
            "Author", 1,1,
            "Aksi", 1, 1
        );
        $table->total = $data_table['total'];
        $table->content = $data_table['rows'];
        $data = $this->ltable->generate($table, 'js', true);
        echo $data;
    }

    public function proses() {
        $this->form_validation->set_rules('name', 'events', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('desc', 'Deskripsi', 'trim');
        $this->form_validation->set_rules('status', 'Status', 'trim|max_length[250]');

        if ($this->form_validation->run($this)) {
            $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');
            $id = $this->input->post('id');
            $data = array();

           // SETTING UPLOAD
            $this->upload->initialize(array(
                "upload_path" => $this->_attachment_path,
                "allowed_types" => "png|jpg|jpeg|gif",
                "overwrite" => TRUE,
                "encrypt_name" => TRUE,
                "remove_spaces" => TRUE,
                "max_size" => 50000,
                "xss_clean" => FALSE,
                "file_name" => array("image_" . date('Ymdhis'))
            ));

            $mslide = array();
            $mslide['name']        = $this->input->post("name");
            $mslide['desc']        = $this->input->post("desc");
            $mslide['vdesc']        = $this->input->post("vdesc");
            $mslide['status']      = $this->input->post("status");

            if ($id == '') {
                $mslide['crtd_at']    = date("Y-m-d");
                $mslide['author']      = $this->_SESSION["m_user_nama"];
 
                if(!empty($_FILES)){
                    if ($this->upload->do_multi_upload("images")) {
                        $return = $this->upload->get_multi_upload_data();

                        $mslide['picture']    = $return[0]["orig_name"];

                        if ($this->events_model->save_as_new($mslide)) {
                            $message = array(true, 'Informasi', 'Proses Berhasil', '#grid_events');
                        }
                    }else{
                        $message = array(false, 'Informasi', $this->upload->display_errors(), '');
                    }
                }else{
                    $message = array(false, 'Informasi', 'Gambar belum dipilih!', '');
                }
            } else {
                $mslide['edtd_at']    = date("Y-m-d");
                $mslide['author']     = $this->_SESSION["m_user_nama"];

                if(!empty($_FILES)){
                    if ($this->upload->do_multi_upload("images")) {
                        $return = $this->upload->get_multi_upload_data();

                        $mslide['picture']    = $return[0]["orig_name"];

                        // DELETE IMAGE FROM FOLDER
                        unlink($this->_attachment_path . $this->input->post("picture"));

                        // DELETE DATE IMAGE BY ID
                        $key = array("id"=> $this->input->post("id"));
                        // $this->events_model->delete("m_slide",$key);

                        if ($this->events_model->save($mslide, $key)) {
                            $message = array(true, 'Informasi', 'Proses Berhasil.', '#grid_events');
                        }
                    }else{
                        $message = array(false, 'Informasi', $this->upload->display_errors(), '');
                    }
                }else{
                    $key = array("id"=> $id);
                    if ($this->events_model->save($mslide, $key)) {
                        $message = array(true, 'Informasi', 'Proses Berhasil.', '#grid_events');
                    }
                }
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors(), '');
        }
        echo json_encode($message, true);
    }

    public function delete($id) {
        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        if ($this->events_model->delete($id)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '#grid_events');
        }
        echo json_encode($message);
    }

}

/* End of file events.php */
/* Location: ./application/modules/events/controllers/events.php */
