<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class strukturorganisasi extends MX_Controller {

    private $_title = 'Role Management';
    private $_limit = 5;
    private $_module = 'backendcompany/strukturorganisasi';
    private $_SESSION = array();

    public function __construct() {
        parent::__construct();

        /* Load Global Model */
        $this->load->model('strukturorganisasi_model');
        $this->_SESSION = $this->session->userdata(APPKEY);
    }

    public function index() {
        // Load Modules
        $this->load->module("template/asset");

        $data['button_group'] = array(
            anchor(null, '<i class="icon-plus"></i> Tambah Data', array('class' => 'btn yellow', 'id' => 'button-add', 'onclick' => 'load_form(this.id)', 'data-source' => base_url($this->_module . '/add')))
        );
        $data['page_title']     = '<i class="icon-laptop"></i> ' . $this->_title;
        $data['page_content']   = $this->_module . '/main';
        $data['data_sources']   = base_url($this->_module . '/load');


        $view = $this->blade->render('backendcompany/strukturorganisasi/index',$data,true);
        
        echo $view;
    }

    public function add($id = '') {
        $page_title = 'Tambah Data';
        $data['id'] = $id;
        $otoritas = array();
        if ($id != '') {
            $page_title         = 'Edit Data';
            $strukturorganisasi             = $this->strukturorganisasi_model->data($id);
            $data['default']    = $strukturorganisasi->get()->row();
        }
        
        $data['page_title']     = '<i class="icon-laptop"></i> ' . $page_title;
        $data['strukturOPT']    = $this->strukturorganisasi_model->options();
        $data['form_action']    = base_url($this->_module . '/proses');

        $this->blade->render($this->_module . '/form',$data);
    }

    public function edit($id) {
        $this->add($id);
    }

    public function load($page = 1) {
        $data_table = $this->strukturorganisasi_model->data_table($this->_module, $this->_limit, $page);

        $this->load->library("ltable");
        $table = new stdClass();
        $table->id = 'strukturorganisasi_id';
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->align = array('grade' => 'left', 'name' => 'left', 'parent' => 'left', 'crtd_at' => 'center' , 'edtd_at' => 'center' , 'author' => 'center' ,'aksi' => 'center');
        $table->width_colom = array(
                                'grade' => '20%', 
                                'name' => '15%',  
                                'parent' => '15%',  
                                'crtd_at' => '10%' , 
                                'edtd_at' => '10%' , 
                                'author' => '20%' ,
                                'aksi' => '10%'
                            );
        $table->page = $page;
        $table->limit = $this->_limit;
        $table->jumlah_kolom = 6;
        $table->header[] = array(
            "Jabatan", 1, 1,
            "Pemegang Jabatan", 1, 1,
            "Induk Jabatan", 1, 1,
            "Created At", 1, 1,
            "Edited At", 1, 1,
            "Author", 1,1,
            "Aksi", 1, 1
        );
        $table->total = $data_table['total'];
        $table->content = $data_table['rows'];
        $data = $this->ltable->generate($table, 'js', true);
        echo $data;
    }

    public function proses() {
        $this->form_validation->set_rules('desc', 'Deskripsi', 'trim|max_length[250]');
        $this->form_validation->set_rules('status', 'Status', 'trim|max_length[250]');

        if ($this->form_validation->run($this)) {
            $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');
            $id = $this->input->post('id');
            $data = array();

            $data["name"]       = $this->input->post('name');
            $data["grade"]      = $this->input->post('grade');
            $data["parent"]     = $this->input->post('parent');
            $data["author"]     = $this->_SESSION["m_role_nama"];
 
            if ($id == '') {
                $data["crtd_at"] = date("Y-m-d");
                if ($this->strukturorganisasi_model->save_as_new($data)) {
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', '#grid_strukturorganisasi');
                }
            } else {
                $data["edtd_at"] = date("Y-m-d");
                if ($this->strukturorganisasi_model->save($data, $id)) {
                    $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', '#grid_strukturorganisasi');
                }
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors(), '');
        }
        echo json_encode($message, true);
    }

    public function delete($id) {
        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        if ($this->strukturorganisasi_model->delete($id)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '#grid_strukturorganisasi');
        }
        echo json_encode($message);
    }

}

/* End of file strukturorganisasi.php */
/* Location: ./application/modules/strukturorganisasi/controllers/strukturorganisasi.php */
