<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class dashboard extends MX_Controller {

    private $_title     = 'dashboard';
    private $_module    = 'dashboard/dashboard';
    private $_limit     = 10;

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $data = array();

        $view = $this->blade->render('dashboard/index',$data,true);
        
        echo $view;
    }
}