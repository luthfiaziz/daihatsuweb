<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_management
 *
 * @author Luthfi Aziz Nugraha
 */

class slide extends MX_Controller {
    // VARIABLE MODUL
    private $_module;

    // VARIABEL SESSION TEMP
    private $SESSION;

    // VARIABEL ATTACHMENT PATH
    private $_attachment_path;

    //-------------------------------------------------------------------------------------------------------------------------------
    // [CONSTRUCT] - CONSTRUCT slide
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();
        // SET PATH IMAGE LOCATION
        $this->_attachment_path = FCPATH .'assets/img/slide/';
        
        // Set Module Location
        $this->_module = 'slide/slide';
        // Load Module Core
        $this->load->module('config/app_core');
        // Load Model
        $this->load->model('slide_model','slide');

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [INDEX] - INDEX slide
    //-------------------------------------------------------------------------------------------------------------------------------
    public function index(){
        $menu_id = $_GET["id"];

        $data['load_form']              = base_url() . $this->_module . '/loadform';
        $data['main_toolbar_source']    = base_url() . $this->_module . '/loadMainToolbar/' . $menu_id;
        $data['data_source']            = base_url() . $this->_module . '/listdata';
        $data['aksesEdit']              = ($this->SESSION["m_otoritas_menu"][$menu_id]["m_role_edit"] == 1)? "TRUE" : "FALSE";

    	$this->load->view($this->_module . '/index',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR MAIN slide
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadMainToolbar($id = ''){
        xml::xml_header();
        $xml  = '<toolbar>';

        if($this->SESSION["m_otoritas_menu"][$id]["m_role_add"] == 1){
            $xml .= '<item id="add" text="Tambah" type="button" img="add.png" imgdis="add.png"/>';
        }

        if($this->SESSION["m_otoritas_menu"][$id]["m_role_edit"] == 1){
            $xml .= '<item id="edit" text="Ubah" type="button" img="edit.png" imgdis="edit.png"/>';
        }

        if($this->SESSION["m_otoritas_menu"][$id]["m_role_add"] == 1){
            $xml .= '<item id="delete" text="Hapus" type="button" img="delete.png" imgdis="delete.png"/>';
        }

        $xml .= '<item id="sep" type="separator"/>';
        $xml .= '<item id="refresh" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png"/>';
        $xml .= "</toolbar>";  

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD DATA slide
    //-------------------------------------------------------------------------------------------------------------------------------
    public function listdata(){
       $data   = $this->slide->loadData();
       
       $result = array("rows" => $data);

       echo json_encode($result);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD TOOLBAR FORM slide
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadFormslideToolbar(){
        xml::xml_header();

        $xml  = '<toolbar>';
        $xml .= '<item id="saveslide" text="Simpan" type="button" img="save.png" imgdis="save.png" action="simpan"/>';
        $xml .= '<item id="cancelslide" text="Segarkan" type="button" img="refresh.png" imgdis="refresh.png" action="bersihslide"/>';
        $xml .= "</toolbar>";  

        echo $xml;
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [LOAD] - LOAD FORM slide
    //-------------------------------------------------------------------------------------------------------------------------------
    public function loadform($id = ''){
        $data["readonly"] = false;
        $data['toolbarform_source']    = base_url() . $this->_module . '/loadFormslideToolbar/';
        
        if (!empty($id)) {
            $data["id"]       = $id;
            $data["default"]  = $this->slide->data($id)->get()->row();
            $data["readonly"] = true; 
        }

        $data['form_action'] = base_url() . $this->_module . '/proses';
        $this->load->view($this->_module . '/form',$data);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - SAVE DATA slide
    //-------------------------------------------------------------------------------------------------------------------------------
    public function proses(){
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;
        $id   = $this->input->post('id');
     
        $this->form_validation->set_rules('m_slide_name', 'Judul slide', 'trim|required');

        if ($this->form_validation->run($this)) {
            $message = array(false, 'Informasi', 'Proses Gagal', 'refresh()');

            $id = $this->input->post('id');

            // SETTING UPLOAD
            $this->upload->initialize(array(
                "upload_path" => $this->_attachment_path,
                "allowed_types" => "png|jpg|jpeg|gif",
                "overwrite" => TRUE,
                "encrypt_name" => TRUE,
                "remove_spaces" => TRUE,
                "max_size" => 30000,
                "xss_clean" => FALSE,
                "file_name" => array("image_" . date('Ymdhis'))
            ));

            $mslide = array();
            $mslide['m_slide_name']        = $this->input->post("m_slide_name");

            if ($id == '') {
                $mslide['m_slide_created_date']    = date("Y-m-d");
                $mslide['m_slide_created_by']      = $this->SESSION["m_user_nama"];
 
                if(!empty($_FILES)){
                    if ($this->upload->do_multi_upload("slidegambar")) {
                        $return = $this->upload->get_multi_upload_data();

                        $mslide['m_slide_path']   = $return[0]["orig_name"];
                        $mslide['m_slide_type']    = $return[0]["image_type"];
                        $mslide['m_slide_width']   = $return[0]["image_width"];
                        $mslide['m_slide_height']  = $return[0]["image_height"];

                        if ($crud->save_as_new('m_slide', $mslide)) {
                            $message = array(true, 'Informasi', 'Proses Berhasil', 'refresh()');
                        }
                    }else{
                        $message = array(false, 'Informasi', $this->upload->display_errors(), 'refresh()');
                    }
                }else{
                    $message = array(false, 'Informasi', '<b>Image</b> is empty', 'refresh()');
                }
            } else {
                $mslide['m_slide_modified_date']   = date("Y-m-d");
                $mslide['m_slide_modified_by']     = $this->SESSION["m_user_nama"];

                if(!empty($_FILES)){
                    if ($this->upload->do_multi_upload("slidegambar")) {
                        $return = $this->upload->get_multi_upload_data();

                        $mslide['m_slide_path']   = $return[0]["orig_name"];
                        $mslide['m_slide_type']    = $return[0]["image_type"];
                        $mslide['m_slide_width']   = $return[0]["image_width"];
                        $mslide['m_slide_height']  = $return[0]["image_height"];


                        // DELETE IMAGE FROM FOLDER
                        unlink($this->_attachment_path . $this->input->post("m_slide_path"));

                        // DELETE DATE IMAGE BY ID
                        $key = array("m_slide_id"=> $this->input->post("id"));
                        // $crud->delete("m_slide",$key);


                        if ($crud->save('m_slide', $mslide, $key)) {
                            $message = array(true, 'Informasi', 'Proses Berhasil.', 'refresh()');
                        }
                    }else{
                        $message = array(false, 'Informasi', $this->upload->display_errors(), 'refresh()');
                    }
                }else{
                    $key = array("m_slide_id"=> $id);
                    if ($crud->save('m_slide', $mslide, $key)) {
                        $message = array(true, 'Informasi', 'Proses Berhasil.', 'refresh()');
                    }
                }
            }
        } else {
            $message = array(false, 'Informasi', validation_errors("<span class='error_mess'>","</span>"), "refresh()");
        }

        echo json_encode($message,true);
    }

    /*CUSTOM VALIDATION - cek id jika ada*/
    public function ketersediaan_check()
    {
        $slide_id = $this->input->post("m_slide_id");

        $ada = $this->slide->hitungJikaAda($slide_id);

        if ($ada > 0)
        {
            $this->form_validation->set_message('ketersediaan_check', 'Id <b><u>' . $slide_id . '</u></b> yang anda inputkan sudah ada');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - DELETE DATA slide
    //-------------------------------------------------------------------------------------------------------------------------------
    public function delete() {
        // CALL GLOBAL MODEL
        $crud = $this->app_crud;

        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        $key = array("m_slide_id"=> $this->input->post("id"));
        if ($crud->delete("m_slide",$key)) {

            unlink($this->_attachment_path . $this->input->post("nama"));

            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '');
        }

        echo json_encode($message);
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    // [PROSES] - GET DATA slide options
    //-------------------------------------------------------------------------------------------------------------------------------
    public function getslideOptions(){
        
    }
}

?>