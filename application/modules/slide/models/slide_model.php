<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Create By   : Luthfi Aziz Nugraha 
 * Create Date : 26/07/2014 - 00:43:05
 * Module Name : Menu Model ( For Template )
 */

class slide_model extends CI_Model {
	private $_table1 = "m_slide";
    private $_table2 = "m_kategorislide";

     public function __construct() {
        parent::__construct();
    }

    private function _kunci($key = array()){
        if(!is_array($key))
            $key = array("m_slide_id" => $key);

        return $key;
    }

    public function data($key = ''){
        $this->db->from($this->_table1 . " a");
        
        if (!empty($key) || is_array($key))
            $this->db->where($this->_kunci($key));

        return $this->db;
    }

    public function loadData(){
    	$result = $this->data()->get();	
    	$data   = array();

    	foreach ($result->result() as $value) {
			$data[] = array(
							"id" => $value->m_slide_id, 
							"data"=>array(
                                           $value->m_slide_name,
                                           "<img src='" . base_url() . "assets/img/slide/" . $value->m_slide_path . "' height='70px'/>",
                                           $value->m_slide_type,
                                           $value->m_slide_width,
                                           $value->m_slide_height,
										   $value->m_slide_created_date,
										   $value->m_slide_modified_date,
										   $value->m_slide_created_by,
										   $value->m_slide_modified_by,
                                           $value->m_slide_path,
								         )
						    );
    	}

    	return $data;
    }

    public function hitungJikaAda($value='')
    {
        $this->db->from($this->_table1);
        $this->db->where("m_slide_id",$value);

        $total=$this->db->count_all_results();

        return $total;
    }

    public function options($default = '--Pilih Data--', $key = '') {
        $option = array();
        $this->db->from($this->_table1);

        if(!empty($key))
            $this->db->where($key);

        $list   = $this->db->get();
        
        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->m_slide_id] = $row->m_slide_nama;
        }

        return $option;
    }
}
