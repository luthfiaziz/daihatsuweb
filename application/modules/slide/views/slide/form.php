<div id="slideloading" class="imageloading"></div>
<div class="form-wrapper">
    <?php
    $hidden_form = array('id' => !empty($id) ? $id : '');
    echo form_open_multipart($form_action, array('id' => 'fm_slide'), $hidden_form);
    ?>
    <table class="form" width="100%">
        <tr>
            <td>
                <span><b>Judul slide</b> <i>*</i></span>
                <?php echo form_input('m_slide_name', !empty($default->m_slide_name) ? $default->m_slide_name : '', 'id="m_slide_name" class="form_web" placeholder="wajib diisi"'); ?>
            </td>
        </tr>
        <tr>
            <td> 
                <span><b>Gambar</b> <i>*</i></span>
                <?php $img = !empty($default->m_slide_path) ? $default->m_slide_path :'no_image.png'; ?>
                <div id="wrapperupload">
                    <center><img id="uploadPreview2" src="<?php echo base_url(); ?>assets/img/slide/<?php echo $img; ?>" class="upload"/></center>
                </div>
                <input id="uploadImage2" type="file" name="slidegambar[]" onchange="PreviewImage(2);"  style="position:relative; left:-1px;"/>
                <?php echo form_input('m_slide_path',!empty($default->m_slide_path) ? $default->m_slide_path : '','style="display:none"'); ?>
            </td>
        </tr>
    </table>
    <?php 
        echo form_close(); 
    ?>
    <div style="font-size:12px; font-weight:bold; margin-top:10px;">
        <?php
            echo arrman::getRequired();
        ?>
    </div>
</div>
<script type="text/javascript">
    $("#m_slide_name").focus();

    function simpan(id){
        var toolbar = ["slideWindows","saveslide","cancelslide"];

        SimpanData('#fm_slide','#slideloading','single',toolbar);
    }

    function refresh(){
        close_form_modal(slideWindows,"slideWindows");
        refreshGrid(gridslide,url + "slide/slide/listdata","json");
    }
    
    function bersihslide(){
        var data = [
                        ["m_slide_name","","input"],
                        ["m_slide_name","","image"],
                    ];

        refreshForm(data, "m_slide_name");
    }   

    slideWindows.attachEvent("onClose", function(win){

        return true;
    });
</script>