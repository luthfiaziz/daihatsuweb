<script type="text/javascript">
    var slideAkses    = "<?php echo $aksesEdit; ?>";
    var slideWindows  = new dhtmlXWindows();
    
    //-------------------------------------------------------------------------------------------------------------------------------
	// [OBJECT] TOOLBAR
    //-------------------------------------------------------------------------------------------------------------------------------
	toolbarslide = mainTab.cells("slide").attachToolbar();
    toolbarslide.setSkin("dhx_skyblue");
    //toolbarslide.setSkin("dhx_web");
 	toolbarslide.setIconsPath(url + "assets/img/btn/");
 	toolbarslide.loadXML("<?php echo $main_toolbar_source; ?>");
    toolbarslide.attachEvent("onClick", cslide);

    //-------------------------------------------------------------------------------------------------------------------------------
    // [ACTION] TOOLBAR ACTION
    //-------------------------------------------------------------------------------------------------------------------------------
    function cslide(id){
        var module  = url + "slide/slide";
        var idRow   = gridslide.getSelectedRowId();
        var toolbar = ["add", "edit"];

        if(id == "add"){
            form_modal(slideWindows,"slideWindows",["slide",700,350], module + "/loadFormslideToolbar", module + "/loadform");
        }

        if(id == "edit"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            form_modal(slideWindows,"slideWindows",["slide",700,350], module + "/loadFormslideToolbar", module + "/loadform/" + idRow);
        }

        if(id == "delete"){
            if(idRow == null){
                showMessage("alert-warning","Anda Belum Memilih Data");
                return false;
            }

            var namaImage       = gridslide.cells(idRow,9).getValue();

            dhtmlx.confirm({                
                type:"confirm",
                text:"Apakah anda yakin ingin menghapus data ini?",
                callback: function(result){
                    data = {
                        "id"        : idRow,
                        "nama"      : namaImage,
                    }

                    if(result){
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: module + "/delete",
                            data: data,
                            success: function(e){
                                dhtmlx.message({ 
                                    type: "alert", 
                                    text: e[2],
                                    ok:"Ok",
                                    callback:function(res){
                                        if(res){
                                            if(e[0] == true){
                                                refreshGrid(gridslide, module + "/listdata","json");
                                            }
                                        }
                                    }
                                });
                            },
                        });
                    }
                }
            });
        }

        if(id == "refresh"){
            refreshGrid(gridslide,url + "slide/slide/listdata","json");
        }
    }

     //-------------------------------------------------------------------------------------------------------------------------------
    // [OBJECT] GRID
    //-------------------------------------------------------------------------------------------------------------------------------
    gridslide = mainTab.cells("slide").attachGrid();
    gridslide.setHeader("Nama slide, Gambar, Tipe, Lebar, Tinggi, Tanggal Dibuat, Tanggal Diupdate, Dibuat Oleh, Diedit Oleh, #m_slide_image");
    gridslide.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter"); 
    gridslide.setColAlign("left,center,center,left,left,left,leftt,left,left,left");
    gridslide.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
    gridslide.setInitWidths("150,300,200,300,150,150,100,120,100,100");
    gridslide.enableMultiline(true);
    gridslide.init();   
    gridslide.splitAt(3);
    gridslide.load("<?php echo $data_source; ?>","json");
    gridslide.setColumnHidden(11);

    if(slideAkses == "TRUE"){
        gridslide.attachEvent("onRowDblClicked",gridslideDBLClick);
    }

    // [ACTION] GRID ACTION
	function gridslideDBLClick(){
        var module  = url + "slide/slide";
        form_modal(slideWindows,"slideWindows",["slide",700,350], module + "/loadFormslideToolbar", module + "/loadform/" + gridslide.getSelectedRowId());
    }
</script>