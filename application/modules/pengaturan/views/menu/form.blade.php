<div class="container">
        <div class="page-header">
            <h1 class="head-left"><i class="fa fa-th-large teal"></i> Form <small>| {{ (isset($page_title)) ? $page_title : 'Untitle'; }}</small></h1>
        </div>
</div>
<div class="modal-body">
        <?php
            $hidden_form = array('id' => !empty($id) ? $id : '');
        ?>

        {{ form_open_multipart($form_action, array('id' => 'finput', 'class' => 'form-horizontal'), $hidden_form); }}
        <div class="control-group">
            <label for="menu_nama" class="control-label"><b>Nama Menu <span class="symbol required"></span></b></label>
            <div class="controls">
                {{ form_input('m_menu_nama', !empty($default->m_menu_nama) ? $default->m_menu_nama : '', 'class="form-control"'); }}
            </div>
        </div>
         <div class="control-group">
            <label for="menu_parent" class="control-label"><b>Parent Menu</b></label>
            <div class="controls">
                {{ form_dropdown('m_menu_parent', $parent_options, !empty($default->m_menu_parent) ? $default->m_menu_parent : '', 'class="span12" id="menu_parent" style="width:100% !important;"'); }}
            </div>
        </div>
        <div class="control-group">
            <label for="menu_url" class="control-label"><b>Set URL <span class="symbol required"></span></b></label>
            <div class="controls">
                {{ form_input('m_menu_url', !empty($default->m_menu_url) ? $default->m_menu_url : '', 'class="form-control"'); }}
            </div>
        </div>
        <div class="control-group">
            <label for="menu_keterangan" class="control-label"><b>Keterangan</b></label>
            <div class="controls">
                {{ form_input('m_menu_keterangan', !empty($default->m_menu_keterangan) ? $default->m_menu_keterangan : '', 'class="form-control"'); }}
            </div>
        </div>
        <div class="control-group">
            <label for="menu_urutan" class="control-label"><b>Urutan <span class="symbol required"></span></b></label>
            <div class="controls">
                {{ form_input('m_menu_position', !empty($default->m_menu_position) ? $default->m_menu_position : '', 'class="form-control"'); }}
            </div>
        </div>
        <div class="control-group">
            <label for="menu_icon" class="control-label"><b>Icon {{ !empty($default->m_menu_icon) ? $default->m_menu_icon : ''; }}</b></label>
            <div class="controls">
                {{ form_dropdown('m_menu_icon', hgenerator::font_awesome(), !empty($default->m_menu_icon) ? $default->m_menu_icon : '', 'id="sico" style="width:100% !important;"'); }}
            </div>
        </div>
</div>
<div class="modal-footer">
    <div class="form-actions">
        <div id="button-save" class="btn btn-default"  onclick="simpan_data(this.id, '#finput', '#button-back')"><i class="icon-save"></i> Simpan</div>
        <div id="button-back" class="btn btn-bricky" onclick="close_form_modal(this.id)"><i class="icon-circle-arrow-left"></i> Tutup</div>
    </div>
    {{ form_close(); }}
</div>

<script type="text/javascript">
    $(function() {
        select2_icon('menu_parent');
        select2_icon('sico');
    });
</script>