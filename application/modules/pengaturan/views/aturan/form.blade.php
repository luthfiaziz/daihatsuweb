<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo (isset($page_title)) ? $page_title : 'Role Management'; ?>
    </div>
    <div class="panel-body">
        <?php
        $hidden_form = array('id' => !empty($id) ? $id : '');
        echo form_open_multipart($form_action, array('id' => 'finput', 'class' => 'form-horizontal'), $hidden_form);
        ?>
        <div class="control-group">
            <label for="role_name" class="control-label"><b>Nama Role <span class="symbol required"></span></b></label>
            <div class="controls">
                <?php echo form_input('m_role_nama', !empty($default->m_role_nama) ? $default->m_role_nama : '', 'class="form-control"'); ?>
            </div>
            <br>
            <label for="role_keterangan" class="control-label"><b>Role Keterangan <span class="symbol required"></span></b></label>
            <div class="controls">
                <?php echo form_input('m_role_ket', !empty($default->m_role_ket) ? $default->m_role_ket : '', 'class="form-control"'); ?>
            </div>
        </div><br>
        <div class="well-content" id="content_table">
            <table class="table table-striped table-bordered table-hover datatable dataTable">
                <thead>
                    <tr>
                        <th class="center"><?php echo form_checkbox('cb_select_menu', '', false, 'id="cb_select_menu" target-selected="cb_select" onchange="select_all(this.id)"'); ?></th>
                        <th>Nama Menu</th>
                        <th class="center">View</th>
                        <th class="center">Add</th>
                        <th class="center">Edit</th>
                        <th class="center">Delete</th>
                        <th class="center">Export</th>
                        <th class="center">Import</th>
                    </tr>
                </thead>
                <?php foreach ($list_menu['rows'] as $menu_id => $row_menu): ?>
                    <tr>
                        <td align="center">
                            <?php echo $row_menu['m_menu_flag']; ?>
                        </td>
                        <td><?php echo $row_menu['m_menu_nama']; ?></td>				
                        <td align="center"><?php echo form_checkbox('m_role_view[]', $menu_id, isset($otoritas_menu[$menu_id]['m_role_view']) && $otoritas_menu[$menu_id]['m_role_view'] == '1' ? TRUE : FALSE, 'class="cb_select"'); ?></td>
                        <td align="center"><?php echo form_checkbox('m_role_add[]', $menu_id, isset($otoritas_menu[$menu_id]['m_role_add']) && $otoritas_menu[$menu_id]['m_role_add'] == '1' ? TRUE : FALSE, 'class="cb_select"'); ?></td>
                        <td align="center"><?php echo form_checkbox('m_role_edit[]', $menu_id, isset($otoritas_menu[$menu_id]['m_role_edit']) && $otoritas_menu[$menu_id]['m_role_edit'] == '1' ? TRUE : FALSE, 'class="cb_select"'); ?></td>
                        <td align="center"><?php echo form_checkbox('m_role_delete[]', $menu_id, isset($otoritas_menu[$menu_id]['m_role_delete']) && $otoritas_menu[$menu_id]['m_role_delete'] == '1' ? TRUE : FALSE, 'class="cb_select"'); ?></td>
                        <td align="center"><?php echo form_checkbox('m_role_export[]', $menu_id, isset($otoritas_menu[$menu_id]['m_role_export']) && $otoritas_menu[$menu_id]['m_role_export'] == '1' ? TRUE : FALSE, 'class="cb_select"'); ?></td>
                        <td align="center"><?php echo form_checkbox('m_role_import[]', $menu_id, isset($otoritas_menu[$menu_id]['m_role_import']) && $otoritas_menu[$menu_id]['m_role_import'] == '1' ? TRUE : FALSE, 'class="cb_select"'); ?></td>
                    </tr>
                <?php endforeach; ?>			
            </table>
        </div>

        <div class="form-actions">
            <div id="button-save" class="btn btn-default"  onclick="simpan_data(this.id, '#finput', '#button-back')"><i class="icon-save"></i> Simpan</div>
            <div id="button-back" class="btn btn-bricky" onclick="close_form(this.id)"><i class="icon-circle-arrow-left"></i> Tutup</div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
