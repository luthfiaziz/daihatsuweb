<?php 

if (!defined('BASEPATH')) 
    exit('No direct script access allowed');

class menu extends MX_Controller {
    private $_module = "";
    private $_limit  = 10;

    //-----------------------------------------------------------------------------------------------------------------------
    // CONSTRUCT
    //-----------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();
        $this->load->model("pengaturan/menu_model","mm");

        $this->_module = "pengaturan/menu";
    }

    public function index($data = array()){
        $data = array(
                        "data_source"   => base_url() . $this->_module . "/load_data",
                     );

        $view = $this->blade->render('pengaturan/menu/index',$data,true);
        
        echo $view;
    }

    public function load_data($page = 0){
        $data_table             = $this->mm->data_table($this->_module, $this->_limit, $page);

        $this->load->library("ltable");

        $flag = "<a href='javascript:void(0);'><i class='fa fa-th-large'></i></a>";
        // --------------------------------------------------------------------------------------------
        // SET TABLE
        // --------------------------------------------------------------------------------------------
            $table                  = new stdClass();

            $table->id              = 'menu_table';
            $table->style           = 'table table-striped table-bordered table-hover table-full-width';
            $table->align           = array(
                                                'm_menu_flag'           => 'center',
                                                'm_menu_icon'           => 'center',
                                                'm_menu_nama'           => 'left',
                                                'm_menu_url'            => 'left',
                                                'm_menu_keterangan'     => 'left',
                                                'm_menu_position'       => 'center',
                                                'aksi'                  => 'center',
                                           );

            $table->width_colom     = array(    
                                                'm_menu_flag'           => '5%',
                                                'm_menu_icon'           => '5%',
                                                'm_menu_nama'           => '30%',
                                                'm_menu_url'            => '20%',
                                                'm_menu_keterangan'     => '20%',
                                                'm_menu_position'       => '10%',
                                                'aksi'                  => '10%',
                                            );

            $table->page            = $page;
            $table->jumlah_kolom    = 6;

            $table->header[]        = array(
                                                $flag, 1, 1,
                                                "Icon", 1, 1,
                                                "Nama Menu", 1, 1,
                                                "URL", 1, 1,
                                                "Keterangan", 1, 1,
                                                "Urutan", 1, 1,
                                                "Aksi", 1, 1
                                            );

            $table->total           = $data_table['total'];
            $table->content         = $data_table['rows'];

            $data = $this->ltable->generate($table, 'js', true);
        // --------------------------------------------------------------------------------------------

        echo $data;
        echo $page;
    }

    public function add($id = ''){
        $page_title = 'Tambah Data';
        $data['id'] = $id;
        if ($id != '') {
            $page_title = 'Edit Data';
            $loker = $this->mm->data($id);
            $data['default'] = $loker->get()->row();
        }

        $data['parent_options'] = $this->mm->options('-- Pilih Parent --', array('m_menu.m_menu_parent' => 0));

        $data['page_title']  = '<i class="icon-laptop"></i> ' . $page_title;
        $data['form_action'] = base_url($this->_module . '/proses');

        $this->blade->render($this->_module . '/form',$data);
    }

    public function edit($id) {
        $this->add($id);
    }

    public function proses() {
        $this->form_validation->set_rules('m_menu_nama', 'Nama', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('m_menu_parent', 'Parent');
        $this->form_validation->set_rules('m_menu_url', 'URL', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('m_menu_keterangan', 'Keterangan', 'trim|max_length[250]');
        $this->form_validation->set_rules('m_menu_position', 'Urutan', 'trim|required|integer|max_length[3]');
        if ($this->form_validation->run($this)) {
            $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');
            $id         = $this->input->post('id');
            $parent_id  = $this->input->post('m_menu_parent');

            $menu = array();
            $menu['m_menu_nama']        = $this->input->post('m_menu_nama');
            $menu['m_menu_parent']      = !empty($parent_id) ? $parent_id : 0;
            $menu['m_menu_url']         = $this->input->post('m_menu_url');
            $menu['m_menu_keterangan']  = $this->input->post('m_menu_keterangan');
            $menu['m_menu_position']    = $this->input->post('m_menu_position');
            $menu['m_menu_icon']        = $this->input->post('m_menu_icon');

            if ($id == '') {
                if ($this->mm->save_as_new($menu)) {
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', '#grid_menu');
                }
            } else {

                if ($this->mm->save($menu, $id)) {

                    $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', '#grid_menu');
                }
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors(), '');
        }
        echo json_encode($message, true);
    }

   public function delete($id) {
        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        if ($this->mm->delete($id)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '#grid_menu');
        }
        echo json_encode($message);
    }

}