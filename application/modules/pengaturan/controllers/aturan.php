<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class aturan extends MX_Controller {

    private $_title = 'Role Management';
    private $_limit = 10;
    private $_module = 'pengaturan/aturan';

    public function __construct() {
        parent::__construct();

        /* Load Global Model */
        $this->load->model('aturan_model');
        $this->load->model('menu_model');
        $this->load->model('otoritas_menu_model');
    }

    public function index() {
        // Load Modules
        $this->load->module("template/asset");

        $data['button_group'] = array(
            anchor(null, '<i class="icon-plus"></i> Tambah Data', array('class' => 'btn yellow', 'id' => 'button-add', 'onclick' => 'load_form(this.id)', 'data-source' => base_url($this->_module . '/add')))
        );
        $data['page_title']     = '<i class="icon-laptop"></i> ' . $this->_title;
        $data['page_content']   = $this->_module . '/main';
        $data['data_sources']   = base_url($this->_module . '/load');


        $view = $this->blade->render('pengaturan/aturan/index',$data,true);
        
        echo $view;
    }

    public function add($id = '') {
        $page_title = 'Tambah aturan';
        $data['id'] = $id;
        $otoritas = array();
        if ($id != '') {
            $page_title         = 'Edit aturan';
            $aturan             = $this->aturan_model->data($id);
            $data['default']    = $aturan->get()->row();
            $data_otoritas      = $this->otoritas_menu_model->data(array('m_role_id' => $id))->get();

            foreach ($data_otoritas->result() as $dt_otoritas) {
                $otoritas[$dt_otoritas->m_menu_id] = array
                    (
                        'm_role_view'   => $dt_otoritas->m_role_view,
                        'm_role_add'    => $dt_otoritas->m_role_add,
                        'm_role_edit'   => $dt_otoritas->m_role_edit,
                        'm_role_delete' => $dt_otoritas->m_role_delete,
                        'm_role_export' => $dt_otoritas->m_role_export,
                        'm_role_import' => $dt_otoritas->m_role_import
                    );
            }
        }
        $data['page_title']     = '<i class="icon-laptop"></i> ' . $page_title;
        $data['list_menu']      = $this->menu_model->data_table();
        $data['otoritas_menu']  = $otoritas;
        $data['form_action']    = base_url($this->_module . '/proses');

        $this->blade->render($this->_module . '/form',$data);
    }

    public function edit($id) {
        $this->add($id);
    }

    public function load($page = 1) {
        $data_table = $this->aturan_model->data_table($this->_module, $this->_limit, $page);
        $this->load->library("ltable");
        $table = new stdClass();
        $table->id = 'aturan_id';
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->align = array('m_role_nama' => 'center', 'm_role_ket' => 'left', 'aksi' => 'center');
        $table->page = $page;
        $table->limit = $this->_limit;
        $table->jumlah_kolom = 4;
        $table->header[] = array(
            "Nama aturan", 1, 1,
            "Keterangan aturan", 1, 1,
            "Aksi", 1, 1
        );
        $table->total = $data_table['total'];
        $table->content = $data_table['rows'];
        $data = $this->ltable->generate($table, 'js', true);
        echo $data;
    }

    public function proses() {
        $this->form_validation->set_rules('m_role_nama', 'Nama aturan', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('m_role_ket', 'Keterangan aturan', 'trim|max_length[250]');
        if ($this->form_validation->run($this)) {
            $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');
            $id = $this->input->post('id');
            $data = array();
            $data['m_role_nama'] = $this->input->post('m_role_nama');
            $data['m_role_ket'] = $this->input->post('m_role_ket');
            $temp = array();
            $m_role_view    = $this->input->post('m_role_view');
            $m_role_add     = $this->input->post('m_role_add');
            $m_role_edit    = $this->input->post('m_role_edit');
            $m_role_delete  = $this->input->post('m_role_delete');
            $m_role_export  = $this->input->post('m_role_export');
            $m_role_import  = $this->input->post('m_role_import');

            if (is_array($m_role_view) && count($m_role_view) > 0) {
                foreach ($m_role_view as $val) {
                    $temp[$val]['m_role_view'] = '1';
                }
            }
            if (is_array($m_role_add) && count($m_role_add) > 0) {
                foreach ($m_role_add as $val) {
                    $temp[$val]['m_role_add'] = '1';
                }
            }
            if (is_array($m_role_edit) && count($m_role_edit) > 0) {
                foreach ($m_role_edit as $val) {
                    $temp[$val]['m_role_edit'] = '1';
                }
            }
            if (is_array($m_role_delete) && count($m_role_delete) > 0) {
                foreach ($m_role_delete as $val) {
                    $temp[$val]['m_role_delete'] = '1';
                }
            }
            if (is_array($m_role_export) && count($m_role_export) > 0) {
                foreach ($m_role_export as $val) {
                    $temp[$val]['m_role_export'] = '1';
                }
            }
            if (is_array($m_role_import) && count($m_role_import) > 0) {
                foreach ($m_role_import as $val) {
                    $temp[$val]['m_role_import'] = '1';
                }
            }

            if ($id == '') {
                if ($this->aturan_model->save_as_new($data, $temp)) {
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', '#grid_aturan');
                }
            } else {

                if ($this->aturan_model->save($data, $id, $temp)) {
                    $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', '#grid_aturan');
                }
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors(), '');
        }
        echo json_encode($message, true);
    }

    public function delete($id) {
        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        if ($this->aturan_model->delete($id)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '#grid_aturan');
        }
        echo json_encode($message);
    }

}

/* End of file aturan.php */
/* Location: ./application/modules/aturan/controllers/aturan.php */
