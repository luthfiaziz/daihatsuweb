<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class pengguna extends MX_Controller {

    private $_title     = 'pengguna';
    private $_module    = 'pengaturan/pengguna';
    private $_limit     = 10;

    public function __construct() {
        parent::__construct();

        /* Load Global Model */
        $this->load->model('pengguna_model');
        $this->load->model('aturan_model','role_model');

        // Protection
        // hprotection::login();
    }

    public function index() {
        $data['button_group'] = array(
            anchor(null, '<i class="icon-plus"></i> Tambah Data', array('class' => 'btn yellow', 'id' => 'button-add', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->_module . '/add')))
        );

        $data['print_group'] = array(
            anchor(null, '<i class="icon-plus"></i>', array('id' => 'button-print-pdf', 'class' => 'btn outline pull-right', 'rel' => 'tooltip', 'title' => 'Cetak PDF', 'onclick' => 'do_print(this.id, \'#ffilter\')', 'data-source' => $this->_module . '/pdf')),
            anchor(null, '<i class="icon-plus"></i>', array('id' => 'button-print-excel', 'class' => 'btn outline pull-right', 'rel' => 'tooltip', 'title' => 'Cetak Excel', 'onclick' => 'do_print(this.id, \'#ffilter\')', 'data-source' => $this->_module . '/excel'))
        );

        $data['parent_options']     = $this->role_model->options();
        $data['page_title']         = '<i class="icon-laptop"></i> ' . $this->_title;
        $data['page_content']       = $this->_module . '/main';
        $data['data_source']        = base_url($this->_module . '/load');

        $view = $this->blade->render('pengaturan/pengguna/index',$data,true);
        
        echo $view;
    }

    public function add($id = '') {
        $page_title = 'Tambah Data';
        $data['id'] = $id;

        if ($id != '') {
            $page_title         = 'Edit Data';
            $pengguna           = $this->pengguna_model->data($id);
            $data['default']    = $pengguna->get()->row();
        }

        $data['parent_options'] = $this->role_model->options();
        $data['page_title']     = '<i class="icon-laptop"></i> ' . $page_title;
        $data['form_action']    = base_url($this->_module . '/proses');

        $this->blade->render($this->_module . '/form',$data);
    }

    public function edit($id) {
        $this->add($id);
    }

    public function drildown() {
        $this->load->view('pengguna/ganti_password');
    }

    public function load($page = 1) {
        $data_table = $this->pengguna_model->data_table($this->_module, $this->_limit, $page);

        $this->load->library("ltable");

        $table = new stdClass();

        $table->id              = 'pengguna_id';
        $table->style           = "table table-striped table-bordered table-hover datatable dataTable";
        $table->align           = array( 
                                            'm_user_nama'       => 'left',
                                            'm_user_username'   => 'left',
                                            'm_role_nama'       => 'center',
                                            'm_user_lastlogin'  => 'center',
                                            'm_user_status'     => 'center',
                                            'status_password'   => 'left',
                                            'aksi'              => 'center');
        $table->page            = $page;
        $table->limit           = $this->_limit;
        $table->jumlah_kolom    = 8;
        $table->header[]        = array(
                                        "Nama", 1, 1,
                                        "Username", 1, 1,
                                        "Role", 1, 1,
                                        "Last Login", 1, 1,
                                        "Status", 1, 1,
                                        "Status Password", 1, 1,
                                        "Aksi", 1, 1
                                    );

        $table->total           = $data_table['total'];
        $table->content         = $data_table['rows'];

        $data = $this->ltable->generate($table, 'js', true);

        echo $data;
    }

    public function proses() {

        $this->form_validation->set_rules('m_user_nama', 'Name', 'trim|required|min_length[5]|max_length[30]');
        $this->form_validation->set_rules('m_user_username', 'penggunaname', 'trim|required|min_length[5]|max_length[30]|');
        $this->form_validation->set_rules('m_role_id', 'Role', 'trim|required');
        $this->load->library('encrypt');

        if ($this->form_validation->run($this)) {
            $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');
            $id = $this->input->post('id');

            $pengguna = array();

            if ($id == '') {

                $pengguna['m_user_nama'] = $this->input->post('m_user_nama');
                $pengguna['m_user_username'] = $this->input->post('m_user_username');
                $pengguna['m_user_password'] = md5($this->input->post('m_user_username'));
                $pengguna['m_role_id'] = $this->input->post('m_role_id');

                if ($this->pengguna_model->save_as_new($pengguna)) {
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', '#grid_pengguna');
                }
            } else {

                $pengguna['m_user_nama']        = $this->input->post('m_user_nama');
                $pengguna['m_user_username']    = $this->input->post('m_user_username');
                $pengguna['m_user_password']    = md5($this->input->post('m_user_username'));
                $pengguna['m_role_id']          = $this->input->post('m_role_id');

                if ($this->pengguna_model->save($pengguna, $id)) {
                    $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', '#grid_pengguna');
                }
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors(), '');
        }

        echo json_encode($message, true);
    }

    public function delete($id) {
        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        if ($this->pengguna_model->delete($id)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '#grid_pengguna');
        }

        echo json_encode($message);
    }

    public function reset_password($id) {
        $message = array(false, 'Proses gagal', 'Proses reset password gagal.', '');

        $pengguna = $this->pengguna_model->data($id)->get();
        if ($pengguna->num_rows() > 0) {
            $datapengguna = $pengguna->row();

            $data = array();
            $data['m_user_password'] = md5($datapengguna->m_user_username);

            if ($this->pengguna_model->save($data, $id)) {
                $message = array(true, 'Proses Berhasil', 'Proses reset password berhasil.', '#grid_pengguna');
            }
        }
        echo json_encode($message);
    }

    public function ganti_password() {
        // Load Modules
        $this->load->module("template/asset");

        // Memanggil plugin JS Crud
        $this->asset->set_plugin(array('crud'));

        $page_title             = 'Ubah Password';
        $data['page_title']     = '<i class="icon-laptop"></i> ' . $this->_title;
        $data['form_action']    = base_url($this->_module . '/proses_password');
        $data['page_content']   = $this->_module . '/ganti_password';

        echo Modules::run("template/admin", $data);
    }

    public function proses_password() {
        $this->form_validation->set_rules('password_lama', 'Password Lama', 'trim|required|min_length[5]|max_length[30]|callback_password_check_db');
        $this->form_validation->set_rules('password_baru', 'Password Baru', 'trim|required|min_length[5]|max_length[30]|matches[konf_password]|callback_password_check[password_lama]');
        $this->form_validation->set_rules('konf_password', 'Konfirmasi Password Baru', 'trim|required|min_length[5]|max_length[30]|');
        $this->form_validation->set_message('matches', 'Kedua Password Baru tidak cocok.');

        if ($this->form_validation->run($this)) {
            $message        = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

            $passBaru       = md5($this->input->post('password_baru'));
            $id             = $this->session->penggunadata('pengguna_id');
           
            $data_pengguna  = array();
            $data_pengguna['m_user_password'] = $passBaru;
            $data_pengguna['pengguna_last_password_update'] = date('Y-m-d');
           
            if ($this->pengguna_model->save($data_pengguna, $id)) {
                $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', '');
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors(), '');
        }

        echo json_encode($message, true);
    }

    public function password_check($password_lama, $password_baru) {
        $passLama = md5($password_lama);
        $passBaru = md5($password_baru);

        if ($passLama == $passBaru) {
            $this->form_validation->set_message('password_check', '%s dan Password Lama sama.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function password_check_db($password) {
        $pass       = md5($password);
        $id         = $this->session->penggunadata('pengguna_id');
        $pengguna   = $this->pengguna_model->data($id)->get();

        if ($pengguna->num_rows() > 0) {
            $datapengguna = $pengguna->row();
            if ($datapengguna->m_user_password == $pass) {
                return TRUE;
            } else {
                $this->form_validation->set_message('password_check_db', '%s salah.');
                return FALSE;
            }
        } else {
            $this->form_validation->set_message('password_check_db', 'Data pengguna Tidak ditemukan.');
        }
    }

    public function pdf() {
        $data_table = $this->pengguna_model->data_table($this->_module, $this->_limit, 1);

        $this->load->library("ltable");
        $this->load->library("lpdf");

        $table = new stdClass();
        $table->align = array(
                                'id'                => 'center', 
                                'aksi'              => 'center',
                             );
        $table->jumlah_kolom = 8;
        $table->header[] = array(
            "ID", 1, 1,
            "Nama", 1, 1,
            "penggunaname", 1, 1,
            "Role", 1, 1,
            "Last Login", 1, 1,
            "Status", 1, 1,
            "Status Password", 1, 1,
            "Aksi", 1, 1
        );

        $table->total   = $data_table['total'];
        $table->content = $data_table['rows'];
        $data           = $this->ltable->generate($table, 'html', true);

        $this->lpdf->judul('ABSENSI PEGAWAI');
        $this->lpdf->html($data);
        $this->lpdf->cetak('A4-L');
    }

}

/* End of file pengguna.php */
/* Location: ./application/modules/meeting_management/controllers/pengguna.php */
