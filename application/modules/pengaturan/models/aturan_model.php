<?php

class aturan_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_table1 = "m_role";
    private $_table2 = "m_otoritas_menu";

    private function _key($key) { //unit ID
        if (!is_array($key)) {
            $key = array('m_role_id' => $key);
        }
        return $key;
    }

    public function data($key = '') {
        $this->db->from($this->_table1);
        if (!empty($key) || is_array($key))
            $this->db->where_condition($this->_key($key));
        return $this->db;
    }

    public function save_as_new($data, $temp) {
        $this->db->trans_begin();
        $save_id = $this->db->set_id($this->_table1, 'm_role_id', 'no_prefix', 3);
        $this->db->insert($this->_table1, $data);
        $this->save_otoritas($save_id, $temp);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function save_otoritas($save_id, $temp) {
        $this->db->delete($this->_table2, array('m_role_id' => $save_id));
        
        foreach ($temp as $key => $val) {
            $data_otoritas = array();
            $data_otoritas['m_role_id']     = $save_id;
            $data_otoritas['m_menu_id']     = $key;
            $data_otoritas['m_role_view']   = isset($val['m_role_view']) ? '1' : '0';
            $data_otoritas['m_role_add']    = isset($val['m_role_add']) ? '1' : '0';
            $data_otoritas['m_role_edit']   = isset($val['m_role_edit']) ? '1' : '0';
            $data_otoritas['m_role_delete'] = isset($val['m_role_delete']) ? '1' : '0';
            $data_otoritas['m_role_export'] = isset($val['m_role_export']) ? '1' : '0';
            $data_otoritas['m_role_import'] = isset($val['m_role_import']) ? '1' : '0';

            $this->db->insert($this->_table2, $data_otoritas);
        }
    }

    public function save($data, $key, $temp) {
        $this->db->trans_begin();
        $this->db->update($this->_table1, $data, $this->_key($key));
        $this->save_otoritas($key, $temp);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function delete($key) {
        $this->db->trans_begin();
        $this->db->delete($this->_table1, $this->_key($key));
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function data_table($module = '', $limit = 20, $offset = 1) {
        $filter = array();
        
        $kata_kunci = $this->input->post('kata_kunci');
        if (!empty($kata_kunci))
            $filter[$this->_table1 . ".m_role_nama LIKE '%{$kata_kunci}%' "] = NULL;

        $total = $this->data($filter)->count_all_results();
        $this->db->limit($limit, ($offset * $limit) - $limit);
        $record = $this->data($filter)->get();

        $rows = array();
        foreach ($record->result() as $row) {
            $id = $row->m_role_id;

            $aksi           = "<div class='btn btn-xs btn-primary' id='button-edit-{$id}' onclick='load_form(this.id)' data-source='" . base_url() . "$module/edit/$id'><i class='fa fa-edit fa-lg'></i></div>";
            $aksi          .= "&nbsp;&nbsp;";
            $aksi          .= "<div class='btn btn-xs btn-bricky' id='button-delete-{$id}' onclick='delete_row(this.id)' data-source='" . base_url() . "$module/delete/$id'><i class='fa fa-trash-o'></i></div>";

            $rows[$id] = array(
                'm_role_nama' => $row->m_role_nama,
                'm_role_ket' => $row->m_role_ket,
                'aksi' => $aksi
            );
        }

        return array('total' => $total, 'rows' => $rows);
    }

    public function options($default = '--Pilih Otoritas Data--') {
        $option = array();
        $list = $this->data()->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->m_role_id] = $row->m_role_nama;
        }

        return $option;
    }

}

/* End of file aturan_model.php */
/* Location: ./application/modules/unit/models/aturan_model.php */