<?php

class pengguna_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_table1 = "m_user";
    private $_table2 = "m_role";

    private function _key($key) {
        if (!is_array($key)) {
            $key = array('m_user_id' => $key);
        }
        return $key;
    }

    public function data($key = '') {
        $this->db->from($this->_table1);
        $this->db->join($this->_table2, $this->_table2 . '.m_role_id=' . $this->_table1 . '.m_role_id');

        if (!empty($key) || is_array($key))
            $this->db->where_condition($this->_key($key));

        return $this->db;
    }

    public function save_as_new($data) {
        $this->db->trans_begin();

        $this->db->set_id($this->_table1, 'm_user_id', 'date_prefix', 9);
        $this->db->insert($this->_table1, $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function save($data, $key) {
        $this->db->trans_begin();

        $this->db->update($this->_table1, $data, $this->_key($key));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function delete($key) {
        $this->db->trans_begin();

        $this->db->delete($this->_table1, $this->_key($key));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function reset_password($key) {
        $this->db->trans_begin();

        $this->db->update($this->_table1, $data, $this->_key($key));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function data_table($module = '', $limit = 20, $offset = 1) {
        // data filtering
        $filter = array();
        $kata_kunci = $this->input->post('kata_kunci');

        if (!empty($kata_kunci)) {
            $filter[$this->_table1 . ".m_user_nama LIKE '%{$kata_kunci[0]}%'"] = NULL;
            $filter[$this->_table2 . ".m_role_id LIKE '%{$kata_kunci[1]}%'"] = NULL;
        }
        $total = $this->data($filter)->count_all_results();
        //$this->db->limit($limit, $offset - 1);
        $this->db->limit($limit, ($offset * $limit) - $limit);
        $record = $this->data($filter)->get();

        $rows = array();
        foreach ($record->result() as $row) {
            $id = $row->m_user_id;
            $aksi  = anchor(null, '<i class="fa fa-edit"></i>', array('class' => 'btn btn-xs btn-primary', 'id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($module . '/edit/' . $id)));
            $aksi .= "&nbsp;";
            $aksi .= anchor(null, '<i class="fa fa-trash-o"></i>', array('class' => 'btn btn-xs btn-bricky', 'id' => 'button-delete-' . $id, 'onclick' => 'delete_row(this.id)', 'data-source' => base_url($module . '/delete/' . $id)));
            $aksi .= "&nbsp;";
            $aksi .= anchor(null, '<i class="fa fa-refresh"></i>', array('class' => 'btn btn-xs btn-primary', 'id' => 'button-reset-' . $id, 'onclick' => 'confirm_dialog_ajax(this.id)', 'data-source' => base_url($module . '/reset_password/' . $id), 'data-message' => 'Apakah anda yakin akan mereset password?'));
            // $aksi .= anchor(null, '<i class="icon-zoom-in"></i>', array('id' => 'button-detail-user-' . $id, 'class' => 'btn transparant btn-tooltip btn-small', 'title' => 'Lihat Child', 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 'm_user_id', 'data-source' => base_url($module . '/drildown/' . $id))) . ' ';

            $passUpdate = $row->m_user_last_password_update;
            $expired = date('Y-m-d', strtotime('+30 day', strtotime($passUpdate)));

            $this->load->library('encrypt');
            if (md5($row->m_user_username) == $row->m_user_password) {
                $statusPass = "Default";
            } elseif (md5($row->m_user_username) != $row->m_user_password && date('Y-m-d') < $expired) {
                $statusPass = "Aktif";
            } elseif (date('Y-m-d') > $expired) {
                $statusPass = "Expired";
            }

            $rows[$id] = array(
                                'm_user_nama'       => $row->m_user_nama,
                                'm_user_username'   => $row->m_user_username,
                                'm_role_nama'       => $row->m_role_nama,
                                'm_user_lastlogin'  => $row->m_user_lastlogin,
                                'm_user_status'     => $row->m_user_status,
                                'status_password'   => $statusPass,
                                'aksi'              => $aksi
                            );
        }

        return array('total' => $total, 'rows' => $rows);
    }

}

/* End of file user_model.php */
/* Location: ./application/modules/meeting_management/models/user_model.php */ 