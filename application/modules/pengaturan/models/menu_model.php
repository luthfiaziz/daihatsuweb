<?php

class menu_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_menu = "m_menu";
    private $_rows = array();

    // KEY
    private function _key($key) {

        if (!is_array($key)) {
            $key = array('m_menu_id' => $key);
        }

        return $key;
    }

    // DATA
    public function data($key = '') {
        $this->db->from($this->_menu);

        if (!empty($key) || is_array($key))
            $this->db->where($this->_key($key));

        return $this->db;
    }

    // create data baru
    public function save_as_new($data) {

        $this->db->trans_begin(); //transaksi = ketika semua proses sudah dilakukan di commit

        $this->db->set_id($this->_menu, 'm_menu_id', 'no_prefix', 3); //set id handle auto increment -table -field

        $this->db->insert($this->_menu, $data);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    // untuk update data
    public function save($data, $key) {
        $this->db->trans_begin();
        $this->db->update($this->_menu, $data, $this->_key($key));
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function delete($key) {
        $this->db->trans_begin();

        $this->db->delete($this->_menu, $this->_key($key));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    // untuk menampilkan data ke table 
    public function data_table($module = '') {

        // show parent
        $parent = array();
        // $parent['menu_parent'] = NULL;
        $total = $this->data($parent)->count_all_results();
        $this->db->order_by($this->_menu . '.m_menu_position','ASC');

        $record = $this->data($parent)->get();

        $temp = array();
        foreach ($record->result() as $value) {
            $parent_id = $value->m_menu_parent;
            $parent = !empty($parent_id) ? $parent_id : 0;
            $temp[$parent][] = $value;
        }
        
        $this->_parsing_menu(0, $temp, $module);
        $rows = $this->_rows;

        return array('total' => $total, 'rows' => $rows);
    }

    private function _parsing_menu($parent_id, $temp, $module) {
        if (isset($temp[$parent_id])) {
            foreach ($temp[$parent_id] as $row) {
                $id             = $row->m_menu_id;
                $url            = 
                $aksi           = "<div class='btn btn-xs btn-primary' id='button-edit-{$id}' onclick='load_form_modal(this.id)' data-source='" . base_url() . "$module/edit/$id'><i class='fa fa-edit fa-lg'></i></div>";
                $aksi          .= "&nbsp;&nbsp;";
                $aksi          .= "<div class='btn btn-xs btn-bricky' id='button-delete-{$id}' onclick='delete_row(this.id)' data-source='" . base_url() . "$module/delete/$id'><i class='fa fa-trash-o'></i></div>";

                $flag = '<a href="javascript:void(0);"><i class="fa fa-chevron-circle-down"></i></a>';

                $title = '<a style="color:#0072c6;font-weight:bold;">' . $row->m_menu_nama . '</a>';

                if ($parent_id > 0) {
                    $flag = '<i class="fa fa-caret-right"></i>';
                    $title = $row->m_menu_nama;
                }

                $this->_rows[$id] = array(
                    'm_menu_flag' => $flag,
                    'm_menu_icon' => !empty($row->m_menu_icon) ? "<i class='{$row->m_menu_icon}'></i>" : '-',
                    'm_menu_nama' => $title,
                    'm_menu_url' => $row->m_menu_url,
                    'm_menu_keterangan' => $row->m_menu_keterangan,
                    'm_menu_position' => $row->m_menu_position,
                    'aksi' => $aksi
                );

                $this->_parsing_menu($row->m_menu_id, $temp, $module);
            }
        }
    }

    public function options($default = '--Pilih Parent--', $key = 'all') {
        $option = array();

        if ($key == 'all') {
            $list = $this->data()->get();
        } else {
            $list = $this->data($this->_key($key))->get();
        }

        // array($this->_menu.'.menu_parent' => NULL

        if (!empty($default)) {
            $option[''] = $default;
        }

        foreach ($list->result() as $row) {
            $option[$row->m_menu_id] = $row->m_menu_nama;
        }
        return $option;
    }
}