<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- start: HEAD -->
<head>
	<title>MAIN DASHBOARD</title>
	<!-- start: META -->
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta content="" name="description" />
	<meta content="" name="author" />

	<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
	{{ $css }}

	<link rel="stylesheet" href="{{ base_url() }}assets/css/print.css" type="text/css" media="print"/>
	<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

	<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- end: HEAD -->

<!-- start: BODY -->
<body class="header-default">
	<!-- start: HEADER -->
	<div class="navbar navbar-inverse navbar-fixed-top">
		<!-- start: TOP NAVIGATION CONTAINER -->
		<div class="container">
			<div class="navbar-header">
				<!-- start: RESPONSIVE MENU TOGGLER -->
				<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
					<span class="clip-list-2"></span>
				</button>
				<!-- end: RESPONSIVE MENU TOGGLER -->
				<!-- start: LOGO -->
				<a class="navbar-brand" href="index-2.html">
					DAIHATSU<i class="clip-clip"></i>CPANEL
				</a>
				<!-- end: LOGO -->
			</div>
			<div class="navbar-tools">
				<!-- start: TOP NAVIGATION MENU -->
				<ul class="nav navbar-right">
					<!-- start: USER DROPDOWN -->
					<li class="dropdown current-user">
						<a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
							<img src="{{ base_url() }}assets/images/avatar-1-small.jpg" class="circle-img" alt="">
							<span class="username">{{ $namapengguna }}</span>
							<i class="clip-chevron-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a href="pages_user_profile.html">
									<i class="clip-user-2"></i>
									&nbsp;My Profile
								</a>
							</li>
							<li>
								<a href="pages_messages.html">
									<i class="clip-bubble-4"></i>
									&nbsp;My Messages (3)
								</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="{{ base_url() }}auth/logout">
									<i class="clip-exit"></i>
									&nbsp;Log Out
								</a>
							</li>
						</ul>
					</li>
					<!-- end: USER DROPDOWN -->
				</ul>
				<!-- end: TOP NAVIGATION MENU -->
			</div>
			<!-- start: HORIZONTAL MENU -->
			<div class="horizontal-menu navbar-collapse collapse">
				{{ $menu }}
			</div>
			<!-- end: HORIZONTAL MENU -->
		</div>
		<!-- end: TOP NAVIGATION CONTAINER -->
	</div>
	<!-- end: HEADER -->


	<!-- start: MAIN CONTAINER -->
	<div class="main-container">
		<!-- start: PAGE -->
		<div class="main-content">
			<div class="container" id="main_page">
				{{ $dashboard }}
			</div>
		</div>
		<!-- end: PAGE -->
	</div>
	<!-- end: MAIN CONTAINER -->


	<!-- start: FOOTER -->
	<div class="footer clearfix">
		<div class="footer-inner">
			2016 &copy; Daihatsu.
		</div>
		<div class="footer-items">
			<span class="go-top"><i class="clip-chevron-up"></i></span>
		</div>
	</div>	

	<div id="base_url" style="display:none">{{ base_url() }}</div>

	{{ $js }}

	<script>
		jQuery(document).ready(function() {
			Main.init();
			PagesUserProfile.init();
		});

		var settUpload = {
	        filebrowserBrowseUrl         : '/assets/js/kcfinder/browse.php?opener=ckeditor&type=files',
	        filebrowserImageBrowseUrl    : '/assets/js/kcfinder/browse.php?opener=ckeditor&type=images',
	        filebrowserFlashBrowseUrl    : '/assets/js/kcfinder/browse.php?opener=ckeditor&type=flash',
	        height						 : 500
	    }

	    var basic =  [
						{ items: [ 'Source', 'Sourcedialog', 'CreateDiv' ] },
						{ items: [ 'Undo', 'Redo', 'Cut', 'Copy', 'PasteFromWord', 'PasteText' ] },
						{ items: [ 'Bold', 'Italic', 'BulletedList', 'NumberedList', 'Blockquote', 'SpecialChar', 'RemoveFormat' ] },
						{ items: [ 'Format', 'Outdent', 'Indent','Font','FontSize'] },
						{ items: [ 'Find', 'Replace', '-', '-', 'Scayt' ] },
						{ items: [ 'Link', 'Unlink' ] },
						{ items: [ 'Maximize' ] },
					]
	</script>
</body>
<!-- end: BODY -->
</html>