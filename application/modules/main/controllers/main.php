<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_management
 *
 * @author Luthfi Aziz Nugraha
 */

class main extends MX_Controller {
    private $_module = "";

    //-------------------------------------------------------------------------------------------------------------------------------
    // CONSTRUCT
    //-------------------------------------------------------------------------------------------------------------------------------
    public function __construct() {
        parent::__construct();

        // GET SETTING APP
        $this->load->module("config/app_setting");
        $this->load->module("config/app_menu");
        $this->load->module("auth/app_auth");

        $this->_module = "main/main";

        $this->app_auth->checking_user_session();

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    public function index(){
        $setting = $this->app_setting;
        $setting->set_plugin(array('css-main','js-main'));

        $menu = $this->app_menu;

        $data = array(
                    "js"            => $setting->get_js(),
                    "css"           => $setting->get_css(),
                    "menu"          => $menu->get_top($this->SESSION["m_role_id"]),
                    "dashboard"     => $this->getDashboard(),
                    "namapengguna"  => $this->SESSION["m_user_nama"]
                );

        $this->blade->render('main/index',$data);
    }

    public function getDashboard($data = array()){
        $view = $this->blade->render('dashboard/index',$data,true);
        
        return $view;
    }
}