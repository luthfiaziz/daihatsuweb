<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class app_auth_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    private $_table1 = "m_user";
    private $_table2 = "m_role";
    private $_table3 = "m_otoritas_menu";

    private function _key($key) {
        if (!is_array($key)) {
            $key = array('user_id' => $key);
        }
        return $key;
    }

    // untuk mendapatkan data
    public function data($key = '') {
        $this->db->from($this->_table1 . " a");
        $this->db->join($this->_table2 . " b","b.m_role_id = a.m_role_id");
        $this->db->join($this->_table3 . " c","c.m_role_id = b.m_role_id");
        $this->db->limit(1);
        
        if (!empty($key) || is_array($key))
            $this->db->where($this->_key($key));
        return $this->db;
        // mereturn objek db, jd bisa di get,set dll
    }

    // create data baru
    public function save_as_new($data) {

        $this->db->trans_begin(); //transaksi = ketika semua proses sudah dilakukan di commit

        // $this->db->set_id($this->_table1, 'crud_id', 'date_prefix', 8); //set id handle auto increment -table -field

        $this->db->insert($this->_table1, $data);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    // untuk update data
    public function save($data, $key) {
        $this->db->trans_begin();
        $this->db->update($this->_table1, $data, $this->_key($key));
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function delete($key) {
        $this->db->trans_begin();

        $this->db->delete($this->_table1, $this->_key($key));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function register_session($data){
        if(empty($data)){
            return FALSE;
        }else{  
            $this->session->set_userdata($data);
        }

        return TRUE;
    }
}

/* End of file tm_gelar.php */
/* Location: ./application/modules/crud/models/crud.php */