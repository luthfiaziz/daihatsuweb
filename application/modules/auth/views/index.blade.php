<!DOCTYPE html>
<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.4 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<head>
	<title>CPANEL</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta content="" name="description" />
	<meta content="" name="author" />
	{{ $css }}
	<link rel="stylesheet" href="assets/css/print.css" type="text/css" media="print"/>
</head>
<body class="login example1">
	<div class="main-login col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
		<div class="logo">DAIHATSU<i class="clip-clip"></i>CPANEL
		</div>
		<div class="box-login">
			<h3>Sign in to your account</h3>
			<p>
				Please enter your name and password to log in.
			</p>
			<form class="form-login" action="{{ base_url() }}auth/confirm" method="POST">
				<div class="errorHandler alert alert-danger no-display">
					<i class="fa fa-remove-sign"></i> You have some form errors. Please check below.
				</div>
				<fieldset>
					<div class="form-group">
						<span class="input-icon">
							<input type="text" class="form-control" name="rkl_username" placeholder="Username">
							<i class="fa fa-user"></i> </span>
						<!-- To mark the incorrectly filled input, you must add the class "error" to the input -->
						<!-- example: <input type="text" class="login error" name="login" value="Username" /> -->
					</div>
					<div class="form-group form-actions">
						<span class="input-icon">
							<input type="password" class="form-control password" name="rkl_password" placeholder="Password">
							<i class="fa fa-lock"></i>
							<a class="forgot" href="javascript:void(0)">
								I forgot my password
							</a> </span>
					</div>
					<div class="form-actions">
						<button type="submit" class="btn btn-bricky pull-right">
							Login <i class="fa fa-arrow-circle-right"></i>
						</button>
					</div>
					<div class="new-account">
						Don't have an account yet?
						<a href="javascript:void(0)" class="register">
							Create an account
						</a>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="copyright">
			2016 &copy; Daihatsu.
		</div>
	</div>

	<br/>
	<?php 
		if($this->session->flashdata('status') != '' || $this->session->flashdata('status') != null){
	?>
	<div class="error_box autohidden">
		<div class="error_message">
			<div class="error_title"><u>ERROR</u></div>
		<?php 
			echo $this->session->flashdata('status');
		?>
		</div>
	</div>
	<?php
		}
	?>

	{{ $js }}

	<script>
		jQuery(document).ready(function() {
			Main.init();
			Login.init();
		});

		$(document).ready(function(){
			$("#rkl_username").select();

			setTimeout(function() {
				$(".autohidden").fadeOut();
			}, 1500);
		});
	</script>
</body>
</html>