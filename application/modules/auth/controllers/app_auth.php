<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_management
 *
 * @author Luthfi Aziz Nugraha
 */

class app_auth extends MX_Controller {
 	public function __construct() {
        parent::__construct();
        
        $this->_module = 'auth/app_auth';

        $this->load->module("config/app_setting");
        $this->load->model("auth/app_auth_model");
        
    }

    public function index(){
        $setting = $this->app_setting;

        $setting->set_plugin(array('css-main','js-main'));

        $data["js" ] = $setting->get_js();
        $data["css"] = $setting->get_css();

        $this->blade->render('auth/index',$data);
    }

    public function login_auth(){
        $this->form_validation->set_rules('rkl_username', 'Username', 'required');
        $this->form_validation->set_rules('rkl_password', 'Password', 'required');

        $location       = "auth";

        $data["username"] = $this->input->post("rkl_username");;  
        $data["password"] = $this->input->post("rkl_password");; 

        if($this->form_validation->run()){
        	$result = $this->login_process(trim($data["username"]), trim($data["password"]));

        	if($result == TRUE){
        		$data["status"] = "<p>Login Berhasil</p>";
        		$location       = "/main";
        	}else{
        		$data["status"] = "<p>Login Gagal, <u><b>Username</b></u> / <u><b>Password</b></u> yang anda masukan salah</p>";
        	}
        }else{
            $data["status"] = validation_errors();
        }
	    
    	$this->session->set_flashdata($data);
	    redirect(base_url($location));
    }

    private function login_process($usr = '', $pass = ''){
    	$key = array(
    			"a.m_user_username" => trim($usr),
    			"a.m_user_password" => md5($pass)
    		);

    	$data_forsession = $this->set_data_session($this->app_auth_model->data($key)->get());

        $data_count      = $this->app_auth_model->data($key)->get();
        
    	if($data_count->num_rows() > 0){
    		$this->app_auth_model->register_session($data_forsession);

    		return TRUE;
    	}else{
    		return FALSE;
    	}
    }

    private function set_data_session($param = array()){
        $data   = $param->result();
        $result = array();

        foreach ($data as $key => $value) {
            // SET SESSION NAME
            $key_id = APPKEY;

            $result[$key_id]["m_user_id"]               = $value->m_user_id;
            $result[$key_id]["m_user_nama"]             = $value->m_user_nama;
            $result[$key_id]["m_user_username"]         = $value->m_user_username;
            $result[$key_id]["m_user_password"]         = $value->m_user_password;
            $result[$key_id]["m_user_jk"]               = $value->m_user_jk;
            $result[$key_id]["m_user_tgl_lhr"]          = $value->m_user_tgl_lhr;
            $result[$key_id]["m_user_alamat"]           = $value->m_user_alamat;
            $result[$key_id]["m_user_email"]            = $value->m_user_email;
            $result[$key_id]["m_user_tlp"]              = $value->m_user_tlp;
            $result[$key_id]["m_user_wrong_pass"]       = $value->m_user_wrong_pass;
            $result[$key_id]["m_user_status"]           = $value->m_user_status;
            $result[$key_id]["m_user_created_date"]     = $value->m_user_created_date;
            $result[$key_id]["m_user_modified_date"]    = $value->m_user_modified_date;
            $result[$key_id]["m_user_created_by"]       = $value->m_user_created_by;
            $result[$key_id]["m_user_modified_by"]      = $value->m_user_modified_by;
            $result[$key_id]["m_role_id"]               = $value->m_role_id;
            $result[$key_id]["m_role_nama"]             = $value->m_role_nama;
            $result[$key_id]["m_role_ket"]              = $value->m_role_ket;
            $result[$key_id]["m_role_created_date"]     = $value->m_role_created_date;
            $result[$key_id]["m_role_modified_date"]    = $value->m_role_modified_date;
            $result[$key_id]["m_role_created_by"]       = $value->m_role_created_by;
            $result[$key_id]["m_role_modified_by"]      = $value->m_role_modified_by;

            $menu_id = $value->m_menu_id;

            $result[$key_id]["m_otoritas_menu"][$menu_id]["m_role_view"]             = $value->m_role_view;
            $result[$key_id]["m_otoritas_menu"][$menu_id]["m_role_add"]              = $value->m_role_add;
            $result[$key_id]["m_otoritas_menu"][$menu_id]["m_role_edit"]             = $value->m_role_edit;
            $result[$key_id]["m_otoritas_menu"][$menu_id]["m_role_delete"]           = $value->m_role_delete;
            $result[$key_id]["m_otoritas_menu"][$menu_id]["m_role_export"]           = $value->m_role_export;
            $result[$key_id]["m_otoritas_menu"][$menu_id]["m_role_import"]           = $value->m_role_import;
            $result[$key_id]["m_otoritas_menu"][$menu_id]["m_menu_id"]               = $value->m_menu_id;
        }

        $result_final = $result;

        return $result_final;
    }

    public function checking_user_session(){
        $GET_SESSION = $this->session->userdata(APPKEY);
        
        if(empty($GET_SESSION["m_user_username"]) || empty($GET_SESSION["m_user_password"])){
            redirect(base_url('auth'));
        }
    }

    public function sessionCEK(){
        $GET_SESSION = $this->session->userdata(APPKEY);
        $data = array(false);

        if(empty($GET_SESSION["m_user_username"]) || empty($GET_SESSION["m_user_password"])){
            $data = array(true);
        }

        echo json_encode($data);
    }

    public function logout(){
        $this->session->sess_destroy();

        redirect(base_url() . 'main');
    }
}