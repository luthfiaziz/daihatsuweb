<?php

if (!defined("BASEPATH")) {
    exit("No direct script access allowed");
}

/**
 * @module CRUD
 * @author  Warman Suganda
 * @created at 15 Februari 2014
 * @modified at 15 Februari 2014
 */
class crud extends MX_Controller {

    private $_title = 'Belajar CRUD';
    private $_module = 'crud/crud';
    private $_limit = 1; // limit 1 sebagai contoh saja, silahkan di set 10 saja

    public function __construct() {
        parent::__construct();

        // Protection
        hprotection::login();

        /* Load Global Model */
        $this->load->model('crud_model');
    }

    public function index() {
        // Load Modules
        $this->load->module("template/asset");

        // Memanggil plugin JS Crud
        $this->asset->set_plugin(array('crud'));

        $data['button_group'] = array(
            anchor(null, '<i class="icon-plus"></i> Tambah Data', array('class' => 'btn yellow', 'id' => 'button-add', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->_module . '/add')))
        );

        $data['page_title'] = '<i class="icon-laptop"></i> ' . $this->_title;
        $data['page_content'] = $this->_module . '/main';
        $data['data_sources'] = base_url($this->_module . '/load');

        echo Modules::run("template/admin", $data);
    }

    public function add($id = '') {
        $page_title = 'Tambah Data';
        $data['id'] = $id;
        if ($id != '') {
            $page_title = 'Edit Data';
            $loker = $this->crud_model->data($id);
            $data['default'] = $loker->get()->row();
        }

        $data['parent_options'] = $this->crud_model->options();

        $data['page_title'] = '<i class="icon-laptop"></i> ' . $page_title;
        $data['form_action'] = base_url($this->_module . '/proses');

        $this->load->view($this->_module . '/form', $data);
    }

    public function edit($id) {
        $this->add($id);
    }

    public function load($page = 1) {
        $data_table = $this->crud_model->data_table($this->_module, $this->_limit, $page);

        $this->load->library("ltable");
        $table = new stdClass();
        $table->id = 'daftar_absensi';
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->align = array('id' => 'center', 'aksi' => 'center');
        $table->page = $page;
        $table->limit = $this->_limit;
        $table->jumlah_kolom = 3;
        $table->header[] = array(
            "ID", 1, 1,
            "Keterangan", 1, 1,
            "Aksi", 1, 1
        );
        $table->total = $data_table['total'];
        $table->content = $data_table['rows'];
        $data = $this->ltable->generate($table, 'js', true);
        echo $data;
    }

    public function proses() {
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
        if ($this->form_validation->run($this)) {
            $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');
            $id = $this->input->post('id');

            $karyawan = array();
            $karyawan['crud_keterangan'] = $this->input->post('keterangan');

            if ($id == '') {
                if ($this->crud_model->save_as_new($karyawan)) {
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', '#content_table');
                }
            } else {

                if ($this->crud_model->save($karyawan, $id)) {

                    $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', '#content_table');
                }
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors(), '');
        }
        echo json_encode($message, true);
    }

    public function delete($id) {
        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        if ($this->crud_model->delete($id)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '#content_table');
        }
        echo json_encode($message);
    }

}

/* End of file crud.php */
/* Location: ./application/modules/crud/controllers/crud.php */
