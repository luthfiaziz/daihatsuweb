<?php

/**
 * @module CRUD
 * @author  Warman Suganda
 * @created at 15 Februari 2014
 * @modified at 15 Februari 2014
 */
class crud_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_table1 = "crud";

    private function _key($key) {
        if (!is_array($key)) {
            $key = array('crud_id' => $key);
        }
        return $key;
    }

    // untuk mendapatkan data
    public function data($key = '') {
        $this->db->from($this->_table1);
        if (!empty($key) || is_array($key))
            $this->db->where_condition($this->_key($key));
        return $this->db;
        // mereturn objek db, jd bisa di get,set dll
    }

    // create data baru
    public function save_as_new($data) {

        $this->db->trans_begin(); //transaksi = ketika semua proses sudah dilakukan di commit

        $this->db->set_id($this->_table1, 'crud_id', 'date_prefix', 8); //set id handle auto increment -table -field

        $this->db->insert($this->_table1, $data);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    // untuk update data
    public function save($data, $key) {
        $this->db->trans_begin();
        $this->db->update($this->_table1, $data, $this->_key($key));
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function delete($key) {
        $this->db->trans_begin();

        $this->db->delete($this->_table1, $this->_key($key));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    // untuk menampilkan data ke table 
    public function data_table($module = '', $limit = 20, $offset = 1) {

        // data filtering
        $filter = array();
        $kata_kunci = $this->input->post('kata_kunci');

        if (!empty($kata_kunci))
            $filter[$this->_table1 . ".crud_keterangan LIKE '%{$kata_kunci}%' "] = NULL;

        $total = $this->data($filter)->count_all_results();
        $this->db->limit($limit, $offset - 1);
        $record = $this->data($filter)->get();

        $rows = array();
        foreach ($record->result() as $row) {
            $id = $row->crud_id;
            $aksi = anchor(null, '<i class="icon-edit"></i>', array('class' => 'btn transparant', 'id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($module . '/edit/' . $id)));
            $aksi .= anchor(null, '<i class="icon-trash"></i>', array('class' => 'btn transparant', 'id' => 'button-delete-' . $id, 'onclick' => 'delete_row(this.id)', 'data-source' => base_url($module . '/delete/' . $id)));
            $rows[$id] = array(
                'id' => $id,
                'nama' => $row->crud_keterangan,
                'aksi' => $aksi
            );
        }

        return array('total' => $total, 'rows' => $rows);
    }

    public function options($default = '--Pilih Parent--') {
        $option = array();
        $list = $this->data()->get();

        if (!empty($default))
            $option[''] = $default;

        foreach ($list->result() as $row) {
            $option[$row->crud_id] = $row->crud_keterangan;
        }

        return $option;
    }

}

/* End of file tm_gelar.php */
/* Location: ./application/modules/crud/models/crud.php */