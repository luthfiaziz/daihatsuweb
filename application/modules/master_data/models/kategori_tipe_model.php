<?php

class kategori_tipe_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    private $_table1 = "m_kategori_tipe";
    
    private function _key($key) {
        if (!is_array($key)) {
            $key = array('m_kategori_tipe_id' => $key);
        }
        return $key;
    }

    public function data($key = '') {
        $this->db->from($this->_table1);

        if (!empty($key) || is_array($key))
            $this->db->where_condition($this->_key($key));

        return $this->db;
    }

    public function save_as_new($data) {
        $this->db->trans_begin();

        $this->db->set_id($this->_table1, 'm_kategori_tipe_id', 'date_prefix', 9);
        $this->db->insert($this->_table1, $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function save($data, $key) {
        $this->db->trans_begin();

        $this->db->update($this->_table1, $data, $this->_key($key));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function delete($key) {
        $this->db->trans_begin();

        $this->db->delete($this->_table1, $this->_key($key));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function reset_password($key) {
        $this->db->trans_begin();

        $this->db->update($this->_table1, $data, $this->_key($key));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function data_table($module = '', $limit = 20, $offset = 1) {
        // data filtering
        $filter = array();
        $kata_kunci = $this->input->post('kata_kunci');

        if (!empty($kata_kunci)) {
            $filter[$this->_table1 . ".m_kategori_tipe_name LIKE '%{$kata_kunci[0]}%'"] = NULL;
        }
        
        $total = $this->data($filter)->count_all_results();
        //$this->db->limit($limit, $offset - 1);
        $this->db->limit($limit, ($offset * $limit) - $limit);
        $record = $this->data($filter)->get();

        $rows = array();
        foreach ($record->result() as $row) {
            $id = $row->m_kategori_tipe_id;
            $aksi  = anchor(null, '<i class="fa fa-edit"></i>', array('class' => 'btn btn-xs btn-primary', 'id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($module . '/edit/' . $id)));
            $aksi .= "&nbsp;";
            $aksi .= anchor(null, '<i class="fa fa-trash-o"></i>', array('class' => 'btn btn-xs btn-bricky', 'id' => 'button-delete-' . $id, 'onclick' => 'delete_row(this.id)', 'data-source' => base_url($module . '/delete/' . $id)));

            $rows[$id] = array(
                                'm_kategori_tipe_name'      => $row->m_kategori_tipe_name,
                                'created_at'                => $row->created_at,
                                'updated_at'                => $row->updated_at,
                                'author'                    => $row->author,
                                'aksi'                      => $aksi
                            );
        }

        return array('total' => $total, 'rows' => $rows);
    }

}

/* End of file user_model.php */
/* Location: ./application/modules/meeting_management/models/user_model.php */ 