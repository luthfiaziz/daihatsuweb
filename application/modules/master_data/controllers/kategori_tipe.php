<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class kategori_tipe extends MX_Controller {

    private $_title     = 'kategori_tipe';
    private $_module    = 'master_data/kategori_tipe';
    private $_limit     = 10;

    public function __construct() {
        parent::__construct();

        /* Load Global Model */
        $this->load->model('kategori_tipe_model');

        // Protection
        // hprotection::login();
    }

    public function index() {
        $data['button_group'] = array(
            anchor(null, '<i class="icon-plus"></i> Tambah Data', array('class' => 'btn yellow', 'id' => 'button-add', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->_module . '/add')))
        );

        $data['print_group'] = array(
            anchor(null, '<i class="icon-plus"></i>', array('id' => 'button-print-pdf', 'class' => 'btn outline pull-right', 'rel' => 'tooltip', 'title' => 'Cetak PDF', 'onclick' => 'do_print(this.id, \'#ffilter\')', 'data-source' => $this->_module . '/pdf')),
            anchor(null, '<i class="icon-plus"></i>', array('id' => 'button-print-excel', 'class' => 'btn outline pull-right', 'rel' => 'tooltip', 'title' => 'Cetak Excel', 'onclick' => 'do_print(this.id, \'#ffilter\')', 'data-source' => $this->_module . '/excel'))
        );

        $data['page_title']         = '<i class="icon-laptop"></i> ' . $this->_title;
        $data['page_content']       = $this->_module . '/main';
        $data['data_source']        = base_url($this->_module . '/load');

        $view = $this->blade->render('master_data/kategori_tipe/index',$data,true);
        
        echo $view;
    }

    public function add($id = '') {
        $page_title = 'Tambah Data';
        $data['id'] = $id;

        if ($id != '') {
            $page_title         = 'Edit Data';
            $kategori_tipe           = $this->kategori_tipe_model->data($id);
            $data['default']    = $kategori_tipe->get()->row();
        }

        $data['page_title']     = '<i class="icon-laptop"></i> ' . $page_title;
        $data['form_action']    = base_url($this->_module . '/proses');

        $this->blade->render($this->_module . '/form',$data);
    }

    public function edit($id) {
        $this->add($id);
    }

    public function drildown() {
        $this->load->view('kategori_tipe/ganti_password');
    }

    public function load($page = 1) {
        $data_table = $this->kategori_tipe_model->data_table($this->_module, $this->_limit, $page);

        $this->load->library("ltable");

        $table = new stdClass();

        $table->id              = 'kategori_tipe_id';
        $table->style           = "table table-striped table-bordered table-hover datatable dataTable";
        $table->align           = array( 
                                            'm_kategori_tipe_name'      => 'left',
                                            'created_at'                => 'left',
                                            'updated_at'                => 'left',
                                            'author'                => 'left',
                                            'aksi'              => 'center');
        $table->page            = $page;
        $table->limit           = $this->_limit;
        $table->jumlah_kolom    = 8;
        $table->header[]        = array(
                                        "Kategori Tipe Name", 1, 1,
                                        "Created At", 1, 1,
                                        "Updated At", 1, 1,
                                        "Author", 1, 1,
                                        "Aksi", 1, 1
                                    );

        $table->total           = $data_table['total'];
        $table->content         = $data_table['rows'];

        $data = $this->ltable->generate($table, 'js', true);

        echo $data;
    }

    public function proses() {

        $this->form_validation->set_rules('m_kategori_tipe_name', 'Name', 'trim|required|max_length[30]');
        $this->load->library('encrypt');

        if ($this->form_validation->run($this)) {
            $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');
            $id = $this->input->post('id');

            $kategori_tipe = array();

            if ($id == '') {

                $kategori_tipe['m_kategori_tipe_name'] = $this->input->post('m_kategori_tipe_name');

                if ($this->kategori_tipe_model->save_as_new($kategori_tipe)) {
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', '#grid_kategori_tipe');
                }
            } else {

                $kategori_tipe['m_kategori_tipe_name']        = $this->input->post('m_kategori_tipe_name');

                if ($this->kategori_tipe_model->save($kategori_tipe, $id)) {
                    $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', '#grid_kategori_tipe');
                }
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors(), '');
        }

        echo json_encode($message, true);
    }

    public function delete($id) {
        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        if ($this->kategori_tipe_model->delete($id)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '#grid_kategori_tipe');
        }

        echo json_encode($message);
    }

    public function reset_password($id) {
        $message = array(false, 'Proses gagal', 'Proses reset password gagal.', '');

        $kategori_tipe = $this->kategori_tipe_model->data($id)->get();
        if ($kategori_tipe->num_rows() > 0) {
            $datakategori_tipe = $kategori_tipe->row();

            $data = array();
            $data['m_kategori_tipe_password'] = md5($datakategori_tipe->m_kategori_tipe_username);

            if ($this->kategori_tipe_model->save($data, $id)) {
                $message = array(true, 'Proses Berhasil', 'Proses reset password berhasil.', '#grid_kategori_tipe');
            }
        }
        echo json_encode($message);
    }

    public function ganti_password() {
        // Load Modules
        $this->load->module("template/asset");

        // Memanggil plugin JS Crud
        $this->asset->set_plugin(array('crud'));

        $page_title             = 'Ubah Password';
        $data['page_title']     = '<i class="icon-laptop"></i> ' . $this->_title;
        $data['form_action']    = base_url($this->_module . '/proses_password');
        $data['page_content']   = $this->_module . '/ganti_password';

        echo Modules::run("template/admin", $data);
    }

    public function proses_password() {
        $this->form_validation->set_rules('password_lama', 'Password Lama', 'trim|required|min_length[5]|max_length[30]|callback_password_check_db');
        $this->form_validation->set_rules('password_baru', 'Password Baru', 'trim|required|min_length[5]|max_length[30]|matches[konf_password]|callback_password_check[password_lama]');
        $this->form_validation->set_rules('konf_password', 'Konfirmasi Password Baru', 'trim|required|min_length[5]|max_length[30]|');
        $this->form_validation->set_message('matches', 'Kedua Password Baru tidak cocok.');

        if ($this->form_validation->run($this)) {
            $message        = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

            $passBaru       = md5($this->input->post('password_baru'));
            $id             = $this->session->kategori_tipedata('kategori_tipe_id');
           
            $data_kategori_tipe  = array();
            $data_kategori_tipe['m_kategori_tipe_password'] = $passBaru;
            $data_kategori_tipe['kategori_tipe_last_password_update'] = date('Y-m-d');
           
            if ($this->kategori_tipe_model->save($data_kategori_tipe, $id)) {
                $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', '');
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors(), '');
        }

        echo json_encode($message, true);
    }

    public function password_check($password_lama, $password_baru) {
        $passLama = md5($password_lama);
        $passBaru = md5($password_baru);

        if ($passLama == $passBaru) {
            $this->form_validation->set_message('password_check', '%s dan Password Lama sama.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function password_check_db($password) {
        $pass       = md5($password);
        $id         = $this->session->kategori_tipedata('kategori_tipe_id');
        $kategori_tipe   = $this->kategori_tipe_model->data($id)->get();

        if ($kategori_tipe->num_rows() > 0) {
            $datakategori_tipe = $kategori_tipe->row();
            if ($datakategori_tipe->m_kategori_tipe_password == $pass) {
                return TRUE;
            } else {
                $this->form_validation->set_message('password_check_db', '%s salah.');
                return FALSE;
            }
        } else {
            $this->form_validation->set_message('password_check_db', 'Data kategori_tipe Tidak ditemukan.');
        }
    }

    public function pdf() {
        $data_table = $this->kategori_tipe_model->data_table($this->_module, $this->_limit, 1);

        $this->load->library("ltable");
        $this->load->library("lpdf");

        $table = new stdClass();
        $table->align = array(
                                'id'                => 'center', 
                                'aksi'              => 'center',
                             );
        $table->jumlah_kolom = 8;
        $table->header[] = array(
            "ID", 1, 1,
            "Nama", 1, 1,
            "kategori_tipename", 1, 1,
            "Role", 1, 1,
            "Last Login", 1, 1,
            "Status", 1, 1,
            "Status Password", 1, 1,
            "Aksi", 1, 1
        );

        $table->total   = $data_table['total'];
        $table->content = $data_table['rows'];
        $data           = $this->ltable->generate($table, 'html', true);

        $this->lpdf->judul('ABSENSI PEGAWAI');
        $this->lpdf->html($data);
        $this->lpdf->cetak('A4-L');
    }

}

/* End of file kategori_tipe.php */
/* Location: ./application/modules/meeting_management/controllers/kategori_tipe.php */
