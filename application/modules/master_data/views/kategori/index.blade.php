<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <div class="page-header">
            <h1 class="head-right">Page | <small>Master Kategori Tipe</small></h1>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div id="table_menu">
                        <!-- end: PAGE HEADER -->
                    <!-- start: PAGE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- start: DYNAMIC TABLE PANEL -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i>
                                    DATA :: Master Kategori Tipe
                                    <div class="panel-tools">
                                        <a class="btn btn-xs btn-link panel-refresh" href="#"> <i class="fa fa-refresh"></i> </a>
                                    </div>
                                </div>
                                <div class="panel-body">

                                    <div class="span12" style="margin-bottom:10px">
                                            <div class="btn btn-primary" id="button-add" onclick="load_form_modal(this.id)" data-source="{{ base_url() }}master_data/kategori_tipe/add"><i class="fa fa-plus"></i> Tambah Data</div>
                                    </div>

                                    <div id="grid_kategori_tipe" data-source="{{ $data_source }}"></div>
                                </div>
                            </div>
                            <!-- end: DYNAMIC TABLE PANEL -->
                            <div class="modal_scrollable">
                                <div id="form-content" class="modal fade modal-overflow"></div>
                            </div>
                        </div>
                    </div>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<script type="text/javascript">
    jQuery(function($) {

        load_table('#grid_kategori_tipe', 1, '#ffilter');
        
        $('.onchange').change(function(){
            load_table('#grid_kategori_tipe', 1);
        });

        $('#button-filter').click(function() {
            load_table('#grid_kategori_tipe', 1, '#ffilter');
        });
    });
</script>