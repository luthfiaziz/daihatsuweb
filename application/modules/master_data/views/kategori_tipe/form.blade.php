<div class="container">
        <div class="page-header">
            <h1 class="head-left"><i class="fa fa-th-large teal"></i> Form <small>| {{ (isset($page_title)) ? $page_title : 'Untitle'; }}</small></h1>
        </div>
</div>
<div class="modal-body">
        <?php
            $hidden_form = array('id' => !empty($id) ? $id : '');
        ?>

        {{ form_open_multipart($form_action, array('id' => 'finput', 'class' => 'form-horizontal'), $hidden_form); }}
        <div class="control-group">
            <label for="nama" class="control-label"><b>Kategori Tipe Name <span class="symbol required"></span></b></label>
            <div class="controls">
                {{ form_input('m_kategori_tipe_name', !empty($default->m_kategori_tipe_name) ? $default->m_kategori_tipe_name : '', 'class="form-control" maxlength="100"'); }}
            </div>
        </div>
</div>
<div class="modal-footer">
    <div class="form-actions">
        <div id="button-save" class="btn btn-default"  onclick="simpan_data(this.id, '#finput', '#button-back')"><i class="icon-save"></i> Simpan</div>
        <div id="button-back" class="btn btn-bricky" onclick="close_form_modal(this.id)"><i class="icon-circle-arrow-left"></i> Tutup</div>
    </div>
    {{ form_close(); }}
</div>

<script type="text/javascript">
    $(function() {
        select2_icon('m_role_id');
        select2_icon('sico');
    });
</script>