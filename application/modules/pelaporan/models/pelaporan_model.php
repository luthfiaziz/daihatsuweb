<?php

class pelaporan_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    private $_table1 = "param_dummy";
    private $_table2 = "huji_dummy";
    
    private function _key($key) {
        if (!is_array($key)) {
            $key = array('a.param_dummy_id' => $key);
        }
        return $key;
    }

    public function data($key = '') {
        $this->db->select("a.*,b.*,a.param_dummy_id as id");
        $this->db->from($this->_table1 . " a");
        $this->db->join($this->_table2 . " b", "b.param_dummy_id = a.param_dummy_id", "left");

        if (!empty($key) || is_array($key))
            $this->db->where_condition($this->_key($key));

        return $this->db;
    }

    public function save_as_new($data) {
        $this->db->trans_begin();

        $this->db->insert($this->_table2, $data);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function save($data, $key) {
        $this->db->trans_begin();

        $this->db->update($this->_table1, $data, $this->_key($key));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function delete($key) {
        $this->db->trans_begin();

        $this->db->delete($this->_table1, $this->_key($key));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function reset_password($key) {
        $this->db->trans_begin();

        $this->db->update($this->_table1, $data, $this->_key($key));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function data_table($module = '', $limit = 20, $offset = 1) {
        // data filtering
        $filter = array();
        $kata_kunci = $this->input->post('kata_kunci');

        if (!empty($kata_kunci)) {
            $filter[$this->_table1 . ".m_pelaporan_name LIKE '%{$kata_kunci[0]}%'"] = NULL;
        }
        
        $filter = array("a.m_user_id" => $this->SESSION["m_user_id"]);

        $total = $this->data($filter)->count_all_results();

        //$this->db->limit($limit, $offset - 1);
        // $this->db->limit($limit, ($offset * $limit) - $limit);
        $record = $this->data($filter)->get();
        
        $rows = array();
        foreach ($record->result() as $row) {
            $id    = $row->id;
            $aksi  = "Status: Proses";
            if(empty($row->huji_dummy_val))
                $aksi  = anchor(null, '<i class="fa fa-plus-square"></i>', array('class' => 'btn btn-xs btn-primary', 'id' => 'button-edit-' . $id, 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($module . '/edit/' . $id)));

            $rows[$id] = array(
                                'param_dummy_name'          => $row->param_dummy_name,
                                'param_dummy_val'           => $row->param_dummy_val,
                                'huji_dummy_val'            => $row->huji_dummy_val,
                                'aksi'                      => $aksi
                            );
        }

        return array('total' => $total, 'rows' => $rows);
    }

}

/* End of file user_model.php */
/* Location: ./application/modules/meeting_management/models/user_model.php */ 