<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class pelaporan extends MX_Controller {

    private $_title     = 'pelaporan';
    private $_module    = 'pelaporan/pelaporan';
    private $_limit     = 10;

    public function __construct() {
        parent::__construct();

        /* Load Global Model */
        $this->load->model('pelaporan_model');

        // Protection
        // hprotection::login();

        // GET SESSION DATA
        $this->SESSION  = $this->session->userdata(APPKEY);
    }

    public function index() {
        $data['button_group'] = array(
            anchor(null, '<i class="icon-plus"></i> Tambah Data', array('class' => 'btn yellow', 'id' => 'button-add', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->_module . '/add')))
        );

        $data['print_group'] = array(
            anchor(null, '<i class="icon-plus"></i>', array('id' => 'button-print-pdf', 'class' => 'btn outline pull-right', 'rel' => 'tooltip', 'title' => 'Cetak PDF', 'onclick' => 'do_print(this.id, \'#ffilter\')', 'data-source' => $this->_module . '/pdf')),
            anchor(null, '<i class="icon-plus"></i>', array('id' => 'button-print-excel', 'class' => 'btn outline pull-right', 'rel' => 'tooltip', 'title' => 'Cetak Excel', 'onclick' => 'do_print(this.id, \'#ffilter\')', 'data-source' => $this->_module . '/excel'))
        );

        $data['page_title']         = '<i class="icon-laptop"></i> ' . $this->_title;
        $data['page_content']       = $this->_module . '/main';
        $data['data_source']        = base_url($this->_module . '/load');

        $view = $this->blade->render('pelaporan/pelaporan/index',$data,true);
        
        echo $view;
    }

    public function add($id = '') {
        $page_title = 'Tambah Data';
        $data['id'] = $id;

        if ($id != '') {
            $page_title         = 'Edit Data';

            $key = array("a.param_dummy_id"=>$id, "a.m_user_id"=>$this->SESSION['m_user_id']);

            $pelaporan           = $this->pelaporan_model->data($key);
            $data['default']    = $pelaporan->get()->row();
        }

        $data['page_title']     = '<i class="icon-laptop"></i> ' . $page_title;
        $data['form_action']    = base_url($this->_module . '/proses');

        $this->blade->render($this->_module . '/form',$data);
    }

    public function edit($id) {
        $this->add($id);
    }

    public function drildown() {
        $this->load->view('pelaporan/ganti_password');
    }

    public function load($page = 1) {
        $data_table = $this->pelaporan_model->data_table($this->_module, $this->_limit, $page);

        $this->load->library("ltable");

        $table = new stdClass();

        $table->id              = 'pelaporan_id';
        $table->style           = "table table-striped table-bordered table-hover datatable dataTable";
        $table->align           = array( 
                                            'Parameter'            => 'left',
                                            'param_dummy_val'      => 'center',
                                            'huji_dummy_val'       => 'center',
                                            'aksi'                 => 'center'
                                            );

      $table->width_colom     = array(    
                                    'param_dummy_name'    => '50%',
                                    'param_dummy_val'     => '10%',
                                    'huji_dummy_val'       => '10%',
                                    'aksi'                  => '30%',
                                );
        $table->page            = $page;
        $table->limit           = $this->_limit;
        $table->jumlah_kolom    = 8;
        $table->header[]        = array(
                                        "Parameter", 1, 1,
                                        "Nilai 1", 1, 1,
                                        "Nilai 2", 1, 1,
                                        "Aksi", 1, 1
                                    );

        $table->total           = $data_table['total'];
        $table->content         = $data_table['rows'];

        $data = $this->ltable->generate($table, 'js', true);

        echo $data;
    }

    public function proses() {

        $this->form_validation->set_rules('huji_dummy_val', 'Nilai 2', 'trim|required');
        $this->load->library('encrypt');

        if ($this->form_validation->run($this)) {
            $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');
            $id = $this->input->post('id');

            $pelaporan = array();

            if ($id == '') {

                $pelaporan['huji_dummy_val'] = $this->input->post('huji_dummy_val');

                if ($this->pelaporan_model->save_as_new($pelaporan)) {
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', '#grid_pelaporan');
                }
            } else {

                $pelaporan['huji_dummy_val']        = $this->input->post('huji_dummy_val');
                $pelaporan['param_dummy_id']        = $this->input->post('id');

                if ($this->pelaporan_model->save_as_new($pelaporan)) {
                    $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', '#grid_pelaporan');
                }
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors(), '');
        }

        echo json_encode($message, true);
    }

    public function delete($id) {
        $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');

        if ($this->pelaporan_model->delete($id)) {
            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '#grid_pelaporan');
        }

        echo json_encode($message);
    }

    public function reset_password($id) {
        $message = array(false, 'Proses gagal', 'Proses reset password gagal.', '');

        $pelaporan = $this->pelaporan_model->data($id)->get();
        if ($pelaporan->num_rows() > 0) {
            $datapelaporan = $pelaporan->row();

            $data = array();
            $data['m_pelaporan_password'] = md5($datapelaporan->m_pelaporan_username);

            if ($this->pelaporan_model->save($data, $id)) {
                $message = array(true, 'Proses Berhasil', 'Proses reset password berhasil.', '#grid_pelaporan');
            }
        }
        echo json_encode($message);
    }

    public function ganti_password() {
        // Load Modules
        $this->load->module("template/asset");

        // Memanggil plugin JS Crud
        $this->asset->set_plugin(array('crud'));

        $page_title             = 'Ubah Password';
        $data['page_title']     = '<i class="icon-laptop"></i> ' . $this->_title;
        $data['form_action']    = base_url($this->_module . '/proses_password');
        $data['page_content']   = $this->_module . '/ganti_password';

        echo Modules::run("template/admin", $data);
    }

    public function proses_password() {
        $this->form_validation->set_rules('password_lama', 'Password Lama', 'trim|required|min_length[5]|max_length[30]|callback_password_check_db');
        $this->form_validation->set_rules('password_baru', 'Password Baru', 'trim|required|min_length[5]|max_length[30]|matches[konf_password]|callback_password_check[password_lama]');
        $this->form_validation->set_rules('konf_password', 'Konfirmasi Password Baru', 'trim|required|min_length[5]|max_length[30]|');
        $this->form_validation->set_message('matches', 'Kedua Password Baru tidak cocok.');

        if ($this->form_validation->run($this)) {
            $message        = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

            $passBaru       = md5($this->input->post('password_baru'));
            $id             = $this->session->pelaporandata('pelaporan_id');
           
            $data_pelaporan  = array();
            $data_pelaporan['m_pelaporan_password'] = $passBaru;
            $data_pelaporan['pelaporan_last_password_update'] = date('Y-m-d');
           
            if ($this->pelaporan_model->save($data_pelaporan, $id)) {
                $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', '');
            }
        } else {
            $message = array(false, 'Proses gagal', validation_errors(), '');
        }

        echo json_encode($message, true);
    }

    public function password_check($password_lama, $password_baru) {
        $passLama = md5($password_lama);
        $passBaru = md5($password_baru);

        if ($passLama == $passBaru) {
            $this->form_validation->set_message('password_check', '%s dan Password Lama sama.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function password_check_db($password) {
        $pass       = md5($password);
        $id         = $this->session->pelaporandata('pelaporan_id');
        $pelaporan   = $this->pelaporan_model->data($id)->get();

        if ($pelaporan->num_rows() > 0) {
            $datapelaporan = $pelaporan->row();
            if ($datapelaporan->m_pelaporan_password == $pass) {
                return TRUE;
            } else {
                $this->form_validation->set_message('password_check_db', '%s salah.');
                return FALSE;
            }
        } else {
            $this->form_validation->set_message('password_check_db', 'Data pelaporan Tidak ditemukan.');
        }
    }

    public function pdf() {
        $data_table = $this->pelaporan_model->data_table($this->_module, $this->_limit, 1);

        $this->load->library("ltable");
        $this->load->library("lpdf");

        $table = new stdClass();
        $table->align = array(
                                'id'                => 'center', 
                                'aksi'              => 'center',
                             );
        $table->jumlah_kolom = 8;
        $table->header[] = array(
            "ID", 1, 1,
            "Nama", 1, 1,
            "pelaporanname", 1, 1,
            "Role", 1, 1,
            "Last Login", 1, 1,
            "Status", 1, 1,
            "Status Password", 1, 1,
            "Aksi", 1, 1
        );

        $table->total   = $data_table['total'];
        $table->content = $data_table['rows'];
        $data           = $this->ltable->generate($table, 'html', true);

        $this->lpdf->judul('ABSENSI PEGAWAI');
        $this->lpdf->html($data);
        $this->lpdf->cetak('A4-L');
    }

}

/* End of file pelaporan.php */
/* Location: ./application/modules/meeting_management/controllers/pelaporan.php */
