<?php

class auto_table{
	private $id;
	private $header;
	private $content;
	private $total;
	private $style;
	private $page;
	private $offset;
	private $limit;
    private $align;
	private $jumlahKolom;
    private $maxPage;
    private $pagination = false;

    public function __construct(){
        $this->CI = &get_instance();
    }

    public function config($TableAttr){

    	if(!isset($TableAttr->id))
    		$TableAttr->id 		= '';

    	if(!isset($TableAttr->header))
    		$TableAttr->header 	= array();

    	if(!isset($TableAttr->content))
    		$TableAttr->content = array();

    	if(!isset($TableAttr->total))
    		$TableAttr->total 	= 0;

    	if(!isset($TableAttr->style))
    		$TableAttr->style 	= '';

    	if(!isset($TableAttr->page))
    		$TableAttr->page 	= 0;

    	if(!isset($TableAttr->limit))
    		$TableAttr->limit 	= 10;

    	if(!isset($TableAttr->jumlahKolom))
    		$TableAttr->jumlahKolom 	= 0;

        if(!isset($TableAttr->align))
            $TableAttr->align     = array();

        if(!isset($TableAttr->pagination))
            $TableAttr->pagination     = false;

        if(!isset($TableAttr->maxPage))
            $TableAttr->maxPage     = 0;

    	$this->id 		     = $TableAttr->id;
    	$this->header 	     = $TableAttr->header;
    	$this->content 	     = $TableAttr->content;
    	$this->total 	     = $TableAttr->total;
    	$this->style 	     = $TableAttr->style;
    	$this->page 	     = $TableAttr->page;
    	$this->limit 	     = $TableAttr->limit;
        $this->offset 	     = ($TableAttr->page * $TableAttr->limit) - $TableAttr->limit;
        $this->align         = $TableAttr->align;
        $this->pagination    = $TableAttr->pagination;
        $this->maxPage       = ceil($this->total/$this->limit);
    }

    public function generate($config){
        $this->config($config);

        return $this->create_table();
    }

    public function create_table(){

        $content    = $this->content;

        $style      = "class='" . $this->style . "'";

        $table_id   = "id='" . $this->id . "'";

        $html    = "<table $table_id $style>";

        // ------------------------------------------------------------------------------------
        // HEAD
        // ------------------------------------------------------------------------------------
            $html   .= "<thead>";

            foreach ($this->header as $k1 => $v1) {
                $html .= "<tr>";
                foreach ($v1 as $k2 => $v2) {
                    $html .= "<th style='text-align:center !important; padding:10px;'>" . strtoupper($v1[$k2]) . "</th>";
                }
                $html .= "</tr>";
            }

            $html   .= "</thead>";
        // ------------------------------------------------------------------------------------


        // ------------------------------------------------------------------------------------
        // BODY
        // ------------------------------------------------------------------------------------
            $html   .= "<tbody>";

            foreach ($content as $k1 => $v1) {
                $html .= "<tr>";
                foreach ($v1 as $k2 => $v2) {
                    $html .= "<td align='" . $this->align[$k2] . "'>" . $v1[$k2] . "</td>";
                }
                $html .= "</tr>";
            }

            $html   .= "</tbody>";
        // ------------------------------------------------------------------------------------

        $html   .= "</table>";


        if($this->pagination == true){
            $total_page = 0;

            $html .= "<ul class='pagination'>";
            $html .= "<li class='next '><a class='number_of_page_btn' nilai_arah='previous'> <i class='fa fa-angle-double-left fa-1x'></i></a></li>";
            $html .= "<li class='control_page'><input type='text' name='number_of_page' class='number_of_page form-control' value='{$this->page}'/>of {$this->maxPage} page</li>";
            $html .= "<li class='max_page'><input type='text' name='max_page' value='{$this->maxPage}' style='display:none'/></li>";
            $html .= "<li class='prev '><a class='number_of_page_btn' nilai_arah='next'> <i class='fa fa-angle-double-right fa-1x'></i></a> </li>";
            $html .= "</ul>";
        }

        return $html;
    }
}   